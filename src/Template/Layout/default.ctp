<?php $cakeDescription = 'LNHS'; ?>
<!DOCTYPE html>
<html>
	<head>
		<?= $this->Html->charset() ?>
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<title>
			<?= $cakeDescription ?>:
			<?= $this->fetch('title') ?>
		</title>
		<?= $this->Html->meta('icon') ?>

		<?= $this->fetch('meta') ?>
		<?= $this->fetch('css') ?>
		<?= $this->fetch('script') ?>

		<?= $this->Html->css([
			'https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css', //bootstrap
			'https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css', //font awesome
			'https://maxcdn.bootstrapcdn.com/bootswatch/3.3.7/cerulean/bootstrap.min.css', //color ui

			// 'bootstrap.min.css', //bootstrap
			// 'https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css', //font awesome
			'nprogress/nprogress', //NProgress
			'bootstrap-daterangepicker/daterangepicker', //daterangepicker
			'custom.min', //custom theme style,
			//datatables
			'datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min',
			'datatables.net-bs/css/dataTables.bootstrap.min',
			'datatables.net-buttons-bs/css/buttons.bootstrap.min',
			'datatables.net-responsive-bs/css/responsive.bootstrap.min',
			'datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min',
			'smart_wizard_vertical',
			'datepicker'
		]);?>

		<?= $this->Html->script([
			'jquery/dist/jquery.min', // Jquery
			'datatables.net/js/jquery.dataTables.min',
			'datatables.net-bs/js/dataTables.bootstrap.min',
			'datatables.net-buttons/js/dataTables.buttons.min',
			'datatables.net-buttons-bs/js/buttons.bootstrap.min',
			'datatables.net-buttons/js/buttons.flash.min',
			'datatables.net-buttons/js/buttons.html5.min',
			'datatables.net-buttons/js/buttons.print.min',
			'datatables.net-responsive/js/dataTables.responsive.min',
			'datatables.net-fixedheader/js/dataTables.fixedHeader.min',
			'jQuery-Smart-Wizard/js/jquery.smartWizard',
			'bootstrap-datepicker',
			'jquery.maskedinput.js'
		]);?>
	</head>
	<body class="nav-md">
		<div class="container body">
			<div class="main_container">

				<div class="col-md-3 left_col">
					<?php echo $this->element('meta/sidebar') ?>
				</div>

				<!-- top navigation -->
				<div class="top_nav">
					<div class="nav_menu">
					<!--navmenu-->
						<?php echo $this->element('meta/nav_menu') ?>
					</div>
				</div>
				<!-- /top navigation -->

				<!-- page content -->
				<div class="right_col" role="main">
					<?= $this->Flash->render() ?>
					<?= $this->fetch('content') ?>
				</div>
				<!-- /page content -->

				<!-- footer content -->
				<footer>
					<?php echo $this->element('meta/footer') ?>
				</footer>
				<!-- /footer content -->
			</div>
		</div>

		<?= $this->Html->script([
			'fastclick/lib/fastclick', //fastclick
			'nprogress', //nprogress
			'bootstrap/dist/js/bootstrap.min', //bootstrap
			'DateJS/build/date', //Date js
			//'bootstrap-daterangepicker/daterangepicker', //datepicker
			'moment/min/moment.min', // moment
			'custom.min',
		]);?>
		<script type="text/javascript">
			<?php foreach ($usersphoto as $uphoto ) { ?>
				var photo = 'default.png';
				if('<?php echo $uphoto["photo"] ?>' !== "") {
					var photo = '<?php echo $uphoto["photo"] ?>';
				}
				$(".profilephoto").attr('src','<?php echo $this->request->webroot; ?>img/'+photo);
			<?php } ?>
		</script>
	</body>
</html>
