<?= $this->Form->create($users, [ 'class' => 'form form-horizontal form-label-left', 'url' => '/users/teachersavepds/'.$users['id'] ]) ?>
<table class="table table-bordered" id="civilserviceTable" style="margin-top: 10px;">
	<thead></thead>
	<tbody>
		<tr>
			<td colspan="7" style="background-color: #8094A8; color: white"><i> IV. CIVIL SERVICE ELIGIBILITY</i></td>
		</tr>
		<tr>
			<td style="background-color: #EAEAEA;">
				<center>27. CAREER SERVICE/ RA 1080 (BOARD/ BAR) UNDER<br> 
				SPECIAL LAWS/ CES/ CSEE<br>                                                    
				BARANGAY ELIGIBILITY / DRIVER'S LICENSE	
				</center>	
			</td>
			<td style="background-color: #EAEAEA;"><center>Rating<br><small>(Write in full)</small></td>
			<td style="background-color: #EAEAEA;"><center> DATE OF EXAMINATION/<br>CONFERMENT</center></td>
			<td style="background-color: #EAEAEA;">
				<center>PLACE OF EXAMINATION/CONFERMENT
			</td>
			<td colspan="2" style="background-color: #EAEAEA;">
				<center>LICENSE<small>(If applicable)</small><br> 
				<span class="pull-left">NUMBER</span>
				<span class="pull-right"> Date of Validity</span>
			</td>
		</tr>
		<?php
			$rows = 6; // define number of rows
			$cols = 5;// define number of columns
			for($tr=0;$tr<=$rows;$tr++){
			    echo "<tr>";
			        for($td=0;$td<=$cols;$td++){ 
		?>
		<td><input type='text' class="form-control civil<?php echo $td; ?> csecolumn_<?php echo $td.'_'.$tr; ?>" data-id="civil<?php echo $td; ?>"> </td>
		<?php 
			}
			    echo "</tr>";
			}
		?>
		<tr>
			<td colspan="6" style="color: red"><center><small><i>(Continue on separate sheet if necessary)</i></small></center></td>
		</tr>
	</tbody>
</table>
<table class="table table-bordered" id="workexperienceTable" style="margin-top: -20px;">
	<thead></thead>
	<tbody>
		<tr>
			<td colspan="8" style="background-color: #8094A8; color: white"><i> V. WORK EXPERIENCE</i></td>
		</tr>
		<tr>
			<td colspan="8" style="background-color: #8094A8; color: white"><i> ( Include private employment.  Start from your recent work) Description of duties should be indicated in the attached Work Experience sheet. )</i></td>
		</tr>
		<tr>
			<td colspan="2" style="background-color: #EAEAEA;">
				<center>28. INCLUSIVE DATES<br>                                                    
				(mm/dd/yyyy)<br>
				<span class="pull-left">FROM</span>
				<span class="pull-right">TO</span>
				</center>	
			</td>
			<td style="background-color: #EAEAEA;"><center>POSITION TITLE<br><small>(Write in full/Do not abbreviate)</small></td>
			<td style="background-color: #EAEAEA;"><center> DEPARTMENT/AGENCY/OFFICE/COMPANY<br><small>(Write in full/Do not abbreviate)</small></center></td>
			<td style="background-color: #EAEAEA;">
				<center>MONTHLY SALARY</center>
			</td>
			<td style="background-color: #EAEAEA;">
				<center>SALARY/JOB/PAY GRADE<br><small>(if applicable)&<br>STEP (Format "00-0")/INCREMENT</small>
			</td>
			<td style="background-color: #EAEAEA;">
				<center>STATUS OF<br> APPOINTMENT</small>
			</td>
			<td style="background-color: #EAEAEA;">
				<center>GOV'T SERVICE<br>(Y/N)</small>
			</td>
		</tr>
		<?php
			$rows = 28; // define number of rows
			$cols = 7;// define number of columns
			for($tr=0;$tr<=$rows;$tr++){
			    echo "<tr>";
			        for($td=0;$td<=$cols;$td++){ 
		?>
		<td><input type='text' class="form-control work<?php echo $td; ?> workcolumn_<?php echo $td.'_'.$tr; ?>" data-id="work<?php echo $td; ?>"> </td>
		<?php 
			}
			    echo "</tr>";
			}
		?>
		<tr>
			<td colspan="8" style="color: red"><center><small><i>(Continue on separate sheet if necessary)</i></small></center></td>
		</tr>
	</tbody>
</table>
<?php $arinput = [
	'cse_career','cse_rating','cse_exam_date','cse_exam_place','cse_license_no','cse_license_expiry',
	'we_inclusive_from','we_inclusive_to','we_position','we_department_agency','we_monthly_salary',
	'we_salary_job','we_status_of_appointment','we_govt_service'
]; ?>
<?php for($i=0; $i<count($arinput); $i++) {?>
	<?= $this->Form->control($arinput[$i], ['label' => false, 'type' => 'hidden']); ?>
<?php } ?>
<?= $this->Form->button(__('Save'),['class'=>'btn btn-primary pull-right','id' => 'saveUser']); ?>
<button type="button" class="btn btn-success" onclick="window.print()"><i class="fa fa-print"></i> Print</button>
<?= $this->Form->end() ?>