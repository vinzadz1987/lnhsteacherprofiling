
<?= $this->Form->create($users, [ 'class' => 'form form-horizontal form-label-left', 'url' => '/users/teachersavepds/'.$users['id'] ]) ?>
	<table class="table table-bordered" id="c1">
		<thead>
			<tr>
				<th colspan="4">
				<i><b>CS FORM NO. 212<br>
				Revised 2017</b></i>
				<center style="font-size: 28px">PERSONAL DATA SHEET</center>
				<br><br>
				<i><small><b>WARNING: Any misrepresentation made in the Personal Data Sheet and the Work Experience Sheet shall cause the filing of administrative/criminal case/s against the person concerned.</b></small></i><br><br>
				<i>
					<small>
						<b>READ THE ATTACHED GUIDE TO FILLING OUT THE PERSONAL DATA SHEET (PDS) BEFORE ACCOMPLISHING THE PDS FORM.</b>
					</small>
				</i>
			</th>
		</tr>
		<tr>
			<th colspan="3"><i>
					<small>
						Print legibly. Tick appropriate box es (  ) and use separate if necessary. Indicat N/A if not applicable. DO NOT ABBREVIATE
					</small>
				</i>
			</th>
			<th>
				<small> <input type="text" value="1. CS ID No. (Do not fill up, For CSC use only)" class="form-control col-md-7 col-xs-12 input-sm" disabled="disabled"></small>
			</th>
		</tr>
		<tr>
			<th colspan="4" style="background-color: #8094A8; color: white"><i>I. PERSONAL INFORMATION</i></th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td style="background-color: #EAEAEA;">2. SURNAME</td>
			<td colspan="3"><?= $this->Form->control('lastname', ['label' => false, 'class' => 'form-control col-md-7 col-xs-12 input-sm']); ?></td>
		</tr>
		<tr>
			<td style="background-color: #EAEAEA;">FIRSTNAME</td>
			<td colspan="2"><?= $this->Form->control('firstname', ['label' => false, 'class' => 'form-control col-md-7 col-xs-12 input-sm']); ?></td>
			<td><small>NAME EXTENSION (JR., SR)</small><br> <?= $this->Form->control('name_extension', ['label' => false, 'class' => 'form-control col-md-7 col-xs-12 input-sm namextens']); ?></td>
		</tr>
		<tr>
			<td style="background-color: #EAEAEA;">MIDDLE NAME</td>
			<td colspan="3"><?= $this->Form->control('middlename', ['label' => false, 'class' => 'form-control col-md-7 col-xs-12 input-sm']); ?></td>
		</tr>
		<tr>
			<td style="background-color: #EAEAEA;">3: DATE OF BIRTH<br><small>(mm/dd/yyyy)</small></td>
			<td><?= $this->Form->control('dateofbirth', ['label' => false, 'class' => 'form-control col-md-7 col-xs-12 input-sm', 'type' => 'text']); ?></td>
			<td  style="background-color: #EAEAEA; ">16. CITIZENSHIP</td>
			<td ><?= $this->Form->control('citizenship', ['label' => false, 'class' => 'form-control col-md-7 col-xs-12 input-sm', 'type' => 'text']); ?></td>
		</tr>
		<tr>
			<td style="background-color: #EAEAEA; ">4: PLACE OF BIRTH</td>
			<td><?= $this->Form->control('placeofbirth', ['label' => false, 'class' => 'form-control col-md-7 col-xs-12 input-sm', 'type' => 'text']); ?></td>
			<td style="background-color: #EAEAEA; "><small>If holder of dual citizenship</small></td>
			<td>Pls. indicate country:</td>
		</tr>
		<tr>
			<td style="background-color: #EAEAEA;">5: SEX</td>
			<td>
				<?= $this->Form->input('sex', [
						'type' => 'select',
						'class' => 'form-control input-sm',
						'label'=> false,
						'options' => [
							'' => 'Select Gender',
							'Male' => 'Male',
							'Female' => 'Female'
						]
					]);
				?>
			</td>
			<td style="background-color: #EAEAEA; "><small>pleas indicate the details</small></td>
			<td><?= $this->Form->control('other_country', ['label' => false, 'class' => 'form-control col-md-7 col-xs-12 input-sm', 'type' => 'text']); ?></td>
		</tr>
		<tr>
			<td style="background-color: #EAEAEA;">6: CIVIL STATUS</td>
			<td>
				<?= $this->Form->input('civilstatus', [
						'type' => 'select',
						'class' => 'form-control input-sm',
						'label'=> false,
						'options' => [
							'' => 'Select Civil Status',
							'Single' => 'Single',
							'Married ' => 'Married ',
							'Divorced ' => 'Divorced',
							'Separated ' => 'Separated ',
							'Widowed' => 'Widowed'
						]
					]);
				?>
			</td>
			<td style="background-color: #EAEAEA; ">17: RESIDENTIAL ADDRESS</td>
			<td>
				<?= $this->Form->control('residence_address_block', ['label' => 'House/Block/Lot No.', 'class' => 'form-control col-md-4 col-xs-12 input-sm']); ?>
				<?= $this->Form->control('residence_address_street', ['label' => 'Street', 'class' => 'form-control col-md-4 col-xs-12 input-sm']); ?>
				<?= $this->Form->control('residence_address_subdivision', ['label' => 'Subdivision/Village', 'class' => 'form-control col-md-4 col-xs-12 input-sm']); ?>
				<?= $this->Form->control('residence_address_brgy', ['label' => 'Barangay', 'class' => 'form-control col-md-4 col-xs-12 input-sm']); ?>
			</td>
		</tr>
		<tr>
			<td style="background-color: #EAEAEA;">7: HEIGHT<small>(m)</small></td>
			<td><?= $this->Form->control('height', ['label' => false, 'class' => 'form-control col-md-7 col-xs-12 input-sm']); ?></td>
			<td style="background-color: #EAEAEA;"></td>
			<td>
				<?= $this->Form->control('residence_address_city', ['label' => 'City/Municipality', 'class' => 'form-control col-md-4 col-xs-12 input-sm']); ?>
				<?= $this->Form->control('residence_address_province', ['label' => 'Province', 'class' => 'form-control col-md-4 col-xs-12 input-sm']); ?>
			</td>
		</tr>
		<tr>
			<td style="background-color: #EAEAEA;">8: WEIGHT<small>(kg)</small></td>
			<td><?= $this->Form->control('weight', ['label' => false, 'class' => 'form-control col-md-7 col-xs-12 input-sm']); ?></td>
			<td style="background-color: #EAEAEA;">ZIP CODE</td>
			<td><?= $this->Form->control('residence_address_zip', ['label' => false, 'class' => 'form-control col-md-4 col-xs-12 input-sm']); ?></td>
		</tr>
		<tr>
			<td style="background-color: #EAEAEA;">9: BLOOD TYPE</td>
			<td>
				<?= $this->Form->input('bloodtype', [
				'type' => 'select',
				'class' => 'form-control input-sm',
				'label'=> false,
				'options' => [
					'' => 'Select Blood type',
					'O-' => 'O-',
					'O+' => 'O+',
					'A-' => 'A-',
					'A+' => 'A+',
					'B-' => 'B-',
					'B+' => 'B+',
					'AB-' => 'AB-',
					'AB+' => 'AB+'
				]
			]);
		?>
			</td>
			<td style="background-color: #EAEAEA;"> 18. PERMANENT ADDRESS</td>
			<td>
				<?= $this->Form->control('permanent_address_block', ['label' => 'House/Block/Lot No.', 'class' => 'form-control col-md-4 col-xs-12 input-sm']); ?>
				<?= $this->Form->control('permanent_address_street', ['label' => 'Street', 'class' => 'form-control col-md-4 col-xs-12 input-sm']); ?>
			</td>
		</tr>
		<tr>
			<td style="background-color: #EAEAEA;">9: GSIS ID NO. </td>
			<td><?= $this->Form->control('gsisidno', ['label' => false, 'class' => 'form-control col-md-7 col-xs-12 input-sm']); ?></td>
			<td style="background-color: #EAEAEA;"></td>
			<td>
				<?= $this->Form->control('permanent_address_subdivision', ['label' => 'Subdivision/Village', 'class' => 'form-control col-md-4 col-xs-12 input-sm']); ?>
				<?= $this->Form->control('permanent_address_brgy', ['label' => 'Barangay', 'class' => 'form-control col-md-4 col-xs-12 input-sm']); ?>
			</td>
		</tr>
		<tr>
			<td style="background-color: #EAEAEA; ">11. PAG-IBIG ID NO.</td>
			<td><?= $this->Form->control('pagibigidno', ['label' => false, 'class' => 'form-control col-md-7 col-xs-12 input-sm']); ?></td>
			<td style="background-color: #EAEAEA;"></td>
			<td>
				<?= $this->Form->control('permanent_address_city', ['label' => 'City/Municipality', 'class' => 'form-control col-md-4 col-xs-12 input-sm']); ?>
				<?= $this->Form->control('permanent_address_province', ['label' => 'Province', 'class' => 'form-control col-md-4 col-xs-12 input-sm']); ?>
			</td>
		</tr>
		<tr>
			<td style="background-color: #EAEAEA;">11. PHILHEALTH NO..</td>
			<td><?= $this->Form->control('philhealthno', ['label' => false, 'class' => 'form-control col-md-7 col-xs-12 input-sm']); ?></td>
			<td style="background-color: #EAEAEA;">ZIP CODE</td>
			<td>
				<?= $this->Form->control('permanent_address_zip', ['label' => false, 'class' => 'form-control col-md-4 col-xs-12 input-sm']); ?>
			</td>
		</tr>
		<tr>
			<td style="background-color: #EAEAEA;">13. SSS No.</td>
			<td><?= $this->Form->control('sssno', ['label' => false, 'class' => 'form-control col-md-7 col-xs-12 input-sm']); ?></td>
			<td style="background-color: #EAEAEA;">19. TELEPHONE NO.</td>
			<td><?= $this->Form->control('telephoneno', ['label' => false, 'class' => 'form-control col-md-4 col-xs-12 input-sm']); ?></td>
		</tr>
		<tr>
			<td style="background-color: #EAEAEA;">14. TIN NO.</td>
			<td><?= $this->Form->control('tinno', ['label' => false, 'class' => 'form-control col-md-7 col-xs-12 input-sm']); ?></td>
			<td style="background-color: #EAEAEA;">20. MOBILE NO.</td>
			<td><?= $this->Form->control('mobileno', ['label' => false, 'class' => 'form-control col-md-4 col-xs-12 input-sm']); ?></td>
		</tr>
		<tr>
			<td style="background-color: #EAEAEA;">15. AGENCY EMPLOYEE NO.</td>
			<td><?= $this->Form->control('agencyemployeeno', ['label' => false, 'class' => 'form-control col-md-7 col-xs-12']); ?></td>
			<td style="background-color: #EAEAEA;">E-MAIL ADDRESS <small>(if any)</small></td>
			<td><?= $this->Form->control('email', ['label' => false, 'class' => 'form-control col-md-4 col-xs-12 input-sm']); ?></td>
		</tr>
		<tr>
			<td colspan="4" style="background-color: #8094A8; color: white"><i> II. FAMILY BACKGROUND</i></td>
		</tr>
		<tr>
			<td style="background-color: #EAEAEA;">22. SPOUSE'S SURNAME</td>
			<td>
				<?= $this->Form->control('spousesurname', ['label' => false, 'class' => 'form-control col-md-4 col-xs-12 input-sm']); ?>
				<?= $this->Form->control('agencyemployeeno', ['label' => '<small>NAME EXTENSION (JR., SR)</small>', 'class' => 'form-control col-md-4 col-xs-12','escape' => false]); ?>
			</td>
			<td style="background-color: #EAEAEA;">23. NAME of CHILDREN <br><small>(Write full name and list all)</small></td>
			<td style="background-color: #EAEAEA;">DATE OF BIRTH <small>(mm\dd\yyyy)</small></td>
		</tr>
		<tr>
			<td style="background-color: #EAEAEA;">FIRST NAME</td>
			<td><?= $this->Form->control('spousefirstname', ['label' => false, 'class' => 'form-control col-md-7 col-xs-12 input-sm']); ?></td>
			<td><input type="text" name="childname" class="form-control col-md-7 col-xs-12 input-sm childname0"></td>
			<td><input type="text" name="childdob" class="form-control col-md-7 col-xs-12 input-sm childdob0"></td>
		</tr>
		<tr>
			<td style="background-color: #EAEAEA;">MIDDLENAME</td>
			<td><?= $this->Form->control('spousemiddlename', ['label' => false, 'class' => 'form-control col-md-7 col-xs-12 input-sm']); ?></td>
			<td><input type="text" name="childname" class="form-control col-md-7 col-xs-12 input-sm childname1"></td>
			<td><input type="text" name="childdob" class="form-control col-md-7 col-xs-12 input-sm childdob1"></td>
		</tr>
		<tr>
			<td style="background-color: #EAEAEA;">OCCUPATION</td>
			<td><?= $this->Form->control('occupation', ['label' => false, 'class' => 'form-control col-md-7 col-xs-12 input-sm']); ?></td>
			<td><input type="text" name="childname" class="form-control col-md-7 col-xs-12 input-sm childname2"></td>
			<td><input type="text" name="childdob" class="form-control col-md-7 col-xs-12 input-sm childdob2"></td>
		</tr>
		<tr>
			<td style="background-color: #EAEAEA;">EMPLOYEER\BUSINESS NAME</td>
			<td><?= $this->Form->control('employerbusinessname', ['label' => false, 'class' => 'form-control col-md-7 col-xs-12 input-sm']); ?></td>
			<td><input type="text" name="childname" class="form-control col-md-7 col-xs-12 input-sm childname3"></td>
			<td><input type="text" name="childdob" class="form-control col-md-7 col-xs-12 input-sm childdob3"></td>
		</tr>
		<tr>
			<td style="background-color: #EAEAEA;">BUSINESS ADDRESS</td>
			<td><?= $this->Form->control('businessaddress', ['label' => false, 'class' => 'form-control col-md-7 col-xs-12 input-sm']); ?></td>
			<td><input type="text" name="childname" class="form-control col-md-7 col-xs-12 input-sm  childname4"></td>
			<td><input type="text" name="childdob" class="form-control col-md-7 col-xs-12  input-sm childdob4"></td>
		</tr>
		<tr>
			<td style="background-color: #EAEAEA;">TELEPHONE NO.</td>
			<td><?= $this->Form->control('spouse_telno', ['label' => false, 'class' => 'form-control col-md-7 col-xs-12 input-sm']); ?></td>
			<td><input type="text" name="childname" class="form-control col-md-7 col-xs-12 input-sm  childname5"></td>
			<td><input type="text" name="childdob" class="form-control col-md-7 col-xs-12  input-sm childdob5"></td>
		</tr>
		<tr>
			<td style="background-color: #EAEAEA;">24. FATHER'S SURNAME</td>
			<td><?= $this->Form->control('father_surname', ['label' => false, 'class' => 'form-control col-md-7 col-xs-12 input-sm']); ?></td>
			<td><input type="text" name="childname" class="form-control col-md-7 col-xs-12 input-sm  childname6"></td>
			<td><input type="text" name="childdob" class="form-control col-md-7 col-xs-12 input-sm childdob6"></td>
		</tr>
		<tr>
			<td style="background-color: #EAEAEA;">FIRSTNAME</td>
			<td>
				<?= $this->Form->control('fatherfirstname', ['label' => false, 'class' => 'form-control col-md-4 col-xs-12 input-sm']); ?>
				<?= $this->Form->control('fathernameextension', ['label' => '<small>NAME EXTENSION (JR., SR)</small>', 'class' => 'form-control col-md-4 col-xs-12 input-sm','escape' => false]); ?>
			</td>
			<td>
				<input type="text" name="childname" class="form-control col-md-7 col-xs-12 input-sm  childname7">
				<input type="text" name="childname" class="form-control col-md-7 col-xs-12 input-sm childname8" style="margin-top: 30px;">
			</td>
			<td>
				<input type="text" name="childdob" class="form-control col-md-7 col-xs-12 input-sm childdob7">
				<input type="text" name="childdob" class="form-control col-md-7 col-xs-12 input-sm childdob8" style="margin-top: 30px;">
			</td>
		</tr>
		<tr>
			<td style="background-color: #EAEAEA;">MIDDLE NAME</td>
			<td><?= $this->Form->control('fathermiddlename', ['label' => false, 'class' => 'form-control col-md-7 col-xs-12 input-sm']); ?></td>
			<td><input type="text" name="childname" class="form-control col-md-7 col-xs-12 input-sm  childname9"></td>
			<td><input type="text" name="childdob" class="form-control col-md-7 col-xs-12 input-sm childdob9"></td>
		</tr>
		<tr>
			<td style="background-color: #EAEAEA;">25. MOTHER'S MAIDEN NAME</td>
			<td><?= $this->Form->control('mother_maidenname', ['label' => false, 'class' => 'form-control col-md-7 col-xs-12 input-sm']); ?></td>
			<td><input type="text" name="childname" class="form-control col-md-7 col-xs-12 input-sm childname10"></td>
			<td><input type="text" name="childdob" class="form-control col-md-7 col-xs-12 childdob11"></td>
		</tr>
		<tr>
			<td style="background-color: #EAEAEA;">SURNAME</td>
			<td><?= $this->Form->control('mothersurname', ['label' => false, 'class' => 'form-control col-md-7 col-xs-12 input-sm']); ?></td>
			<td><input type="text" name="childname" class="form-control col-md-7 col-xs-12 input-sm  childname11"></td>
			<td><input type="text" name="childdob" class="form-control col-md-7 col-xs-12 input-sm childdob11"></td>
		</tr>
		<tr>
			<td style="background-color: #EAEAEA;">FIRSTNAME</td>
			<td><?= $this->Form->control('motherfirstname', ['label' => false, 'class' => 'form-control col-md-7 col-xs-12 input-sm']); ?></td>
			<td><input type="text" name="childname" class="form-control col-md-7 col-xs-12 input-sm  childname12"></td>
			<td>
				<input type="text" name="childdob" class="form-control col-md-7 col-xs-12 input-sm childdob12">
			</td>
		</tr>
		<tr>
			<td style="background-color: #EAEAEA;">MIDDLE NAME</td>
			<td><?= $this->Form->control('mothermiddlename', ['label' => false, 'class' => 'form-control col-md-7 col-xs-12 input-sm']); ?>
				<?= $this->Form->control('nameofchildrens', ['label' => false, 'type' => 'hidden']); ?>
				<?= $this->Form->control('childrendateofbirth', ['label' => false, 'type' => 'hidden']); ?>
			</td>
			<td colspan="2" style="color: red"><center><small><i>(Continue on separate sheet if necessary)</i></small></center></td>
		</tr>
	</tbody>
</table>
<table class="table table-bordered" id="educationTable" style="margin-top: -20px;">
	<thead></thead>
	<tbody>
		<tr>
			<td colspan="8" style="background-color: #8094A8; color: white"><i> III. EDUCATION BACKGROUND</i></td>
		</tr>
		<tr>
			<td style="background-color: #EAEAEA;">26. LEVEL</td>
			<td style="background-color: #EAEAEA;"><center>NAME OF SCHOOL <br>(Write in full)</td>
			<td style="background-color: #EAEAEA;"><center> EDUCATION/DEGREE/COURSE<br> (Write in full)</center></td>
			<td colspan="2" style="background-color: #EAEAEA;">
				<center>PERIOD OF ATTENDANCE<br><br>
				<span class="pull-left">FROM</span>
				<span class="pull-right" style="margin-right: 150px">TO</span>
			</td>
			<td style="background-color: #EAEAEA;"><center>HIGHEST LEVEL/<br>UNITS EARNED <br><small>(If not graduated)</small></td>
			<td style="background-color: #EAEAEA;"><center>YEAR <br>GRADUATED</td>
			<td style="background-color: #EAEAEA;"><center>SCHOLARSHIP/<BR>ACADEMIC<br>/HONORS RECEIVED</td>
		</tr>
		<?php $rows = 4; $cols = 7; ?>
		<?php $array_educ = ['ELEMENTARY','SECONDARY','VOCATIONAL/<br>TRADE COURSE','COLLEGE','GRADUATE STUDIES']; ?>
		<?php for($tr=0;$tr<=$rows;$tr++){ ?>
			<tr>
				<?php for($td=0;$td<=$cols;$td++){ ?>
					<?php if($td == 0) { ?>
						<td style="background-color: #EAEAEA"><?php echo $array_educ[$tr]; ?></td>
					<?php } else { ?>
						<td><input type='text' class="form-control educ<?php echo $td; ?> educcolumn_<?php echo $td.'_'.$tr; ?>" data-id="educ<?php echo $td; ?>"> </td>
					<?php } ?>
				<?php } ?>
			</tr>
		<?php } ?> 
		<tr>
			<td colspan="8" style="color: red"><center><small><i>(Continue on separate sheet if necessary)</i></small></center></td>
		</tr>
		<tr>
			<td style="background-color: #EAEAEA;"><b><i>SIGNATURE</i></b></td>
			<td colspan="3"></td>
			<td style="background-color: #EAEAEA;"><b><i>DATE</i></b></td>
			<td colspan="4"></td>
		</tr>
		<tr>
			<td colspan="8" style="color: red"><small class="pull-right"><i>CS FORM 212 (Revised 2017), Page 1 of 4	</i></small></td>
		</tr>
	</tbody>
</table>
	<?php $arinput = ['education_school_name','education_degree_course','education_attendance_from','education_attendance_to','education_highest_level','education_year_graduated','education_scholars'];
		for($i=0; $i<=6; $i++) {
	?>
		<?= $this->Form->control($arinput[$i], ['label' => false, 'type' => 'hidden']); ?>
	<?php } ?>
	<?= $this->Form->button(__('Save'),['class'=>'btn btn-primary pull-right','id' => 'saveUser']); ?>
	<button type="button" class="btn btn-success" onclick="window.print()"><i class="fa fa-print"></i> Print</button>
	<?= $this->Form->end() ?>