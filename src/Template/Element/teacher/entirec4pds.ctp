<?= $this->Form->create($users, [ 'class' => 'form form-horizontal form-label-left', 'url' => '/users/teachersavepds/'.$users['id'] ]) ?>
<table class="table table-bordered" id="voluntaryTable" style="margin-top: 10px;">
	<thead></thead>
	<tbody>
		<tr>
			<td style="background-color: #EAEAEA;">
				34.	Are you related by consanguinity or affinity to the appointing or recommending authority, or to<br>the			
				chief of bureau or office or to the person who has immediate supervision over you in the Office, <br>			
				Bureau or Department where you will be apppointed,	<br>		
					<br>
				a. within the third degree?			
					<br>
				b. within the fourth degree (for Local Government Unit - Career Employees)?			
			</td>
			<td>
				<div style="margin-top: 55px;">
					<span><input type="checkbox"> YES</span>
					<span><input type="checkbox"> NO</span><br>
					<span><input type="checkbox"> YES</span>
					<span><input type="checkbox"> NO</span><br>
					If Yes, give details
					<br><br>
					_________________________________
				</div>
			</td>
		</tr>
		<tr>
			<td style="background-color: #EAEAEA;">
				35.		a. Have you ever been found guilty of any administrative offense?			

				<br><br><br><br><br><br>
				b. Have you been criminally charged before any court?	
			</td>
			<td>
				<div>
					<span><input type="checkbox"> YES</span>
					<span><input type="checkbox"> NO</span><br>

					If Yes, give details
					<br><br>
					_________________________________<br>
					<span><input type="checkbox"> YES</span>
					<span><input type="checkbox"> NO</span><br>
					If Yes, give details
					<br><br>
					_________________________________
				</div>
			</td>
		</tr>
		<tr>
			<td style="background-color: #EAEAEA;">
				36.		Have you ever been convicted of any crime or violation of any law, decree, ordinance or regulation by any court or tribunal?		
			</td>
			<td>
				<div>
					<span><input type="checkbox"> YES</span>
					<span><input type="checkbox"> NO</span><br>

					If Yes, give details
					<br><br>
					_________________________________
				</div>
			</td>
		</tr>
		<tr>
			<td style="background-color: #EAEAEA;">
				37.		Have you ever been separated from the service in any of the following modes: resignation, <br>retirement, dropped from the rolls, dismissal, termination, end of term, finished contract or<br> phased out (abolition) in the public or private sector?		
			</td>
			<td>
				<div>
					<span><input type="checkbox"> YES</span>
					<span><input type="checkbox"> NO</span><br>

					If Yes, give details
					<br><br>
					_________________________________
				</div>
			</td>
		</tr>
		<tr>
			<td style="background-color: #EAEAEA;">
			38.		a. Have you ever been a candidate in a national or local election held within the last year<br> (except Barangay election)?			
			<br><br>

			b. Have you resigned from the government service during the three (3)-month period before<br> the last election to promote/actively campaign for a national or local candidate?			
	
			</td>
			<td>
				<div>
					<span><input type="checkbox"> YES</span>
					<span><input type="checkbox"> NO</span><br>

					If Yes, give details
					<br><br>
					_________________________________ <br><br>
					<span><input type="checkbox"> YES</span>
					<span><input type="checkbox"> NO</span><br>

					If Yes, give details
					<br><br>
					_________________________________
				</div>
			</td>
		</tr>
		<tr>
			<td style="background-color: #EAEAEA;">
				39.		Have you acquired the status of an immigrant or permanent resident of another country?
			</td>
			<td>
				<div>
					<span><input type="checkbox"> YES</span>
					<span><input type="checkbox"> NO</span><br>

					If Yes, give details (country)
					<br><br>
					_________________________________
				</div>
			</td>
		</tr>
		<tr>
			<td style="background-color: #EAEAEA;">
				40.		Pursuant to: (a) Indigenous People's Act (RA 8371); (b) Magna Carta for Disabled Persons<br> (RA 7277); and (c) Solo Parents Welfare Act of 2000 (RA 8972), please answer the following<br> items:			
				<br>
				a. 		Are you a member of any indigenous group?			
				<br><br><br><br>
				b. 		Are you a person with disability?			
				<br><br><br><br>
				c. 		Are you a solo parent?		

			</td>
			<td>
				<div style="margin-top: 55px;">
					<span><input type="checkbox"> YES</span>
					<span><input type="checkbox"> NO</span><br>

					If Yes, please specify
					<br><br>
					_________________________________<br>
					<span><input type="checkbox"> YES</span>
					<span><input type="checkbox"> NO</span><br>
					If Yes, please specify ID no
					<br><br>
					_________________________________
					<br>
					<span><input type="checkbox"> YES</span>
					<span><input type="checkbox"> NO</span><br>
					If Yes, please specify ID no
					<br><br>
					_________________________________
				</div>
			</td>
		</tr>
		<tr>
			<td>
				<div class="x_title"  style="background-color: #EAEAEA;">
					<h2><small>41. REFERENCES (Person not related by consanguinity or affinity to applicant /appointee)</small></h2>
					<div class="clearfix"></div>
				</div>
				<div class="x_content">

					<table class="table table-bordered">
						<thead>
							<tr>
								<th>NAME</th>
								<th>ADDRESS</th>
								<th>TEL NO</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<th></th>
								<td></td>
								<td></td>
							</tr>
							<tr>
								<th></th>
								<td></td>
								<td></td>
							</tr>
							<tr>
								<th></th>
								<td></td>
								<td></td>
							</tr>
						</tbody>
					</table>
				</div>

					<div class="clearfix"></div>
					<div class="x_title"  style="background-color: #EAEAEA;">
					<h2><small>42.	I declare under oath that I have personally accomplished this Personal Data Sheet which is a true, correct<br> and complete statement pursuant to the provisions of pertinent laws, rules and regulations of the Republic of the<br> Philippines. I authorize the agency head/authorized representative to verify/validate the contents stated herein. <br>         I  agree that any misrepresentation made in this document and its attachments shall cause the filing of<br> administrative/criminal case/s against me.					
					</small></h2>
					<div class="clearfix"></div>
					</div>

			</td>
			<td>
				<div class="table-bordered" style="width: 180px; margin-left: 40px;">
				<p>
					<center>
						ID picture taken within <br>
						the last  6 months<br>
						3.5 cm. X 4.5 cm<br>
						(passport size)<br>

						With full and handwritten<br>
						name tag and signature over<br>
						printed name<br>

						Computer generated <br>
						or photocopied picture <br>
						is not acceptable<br>
					</center>
				</p>
				</div>
				<span style="margin-left: 100px;">PHOTO</span>
			</td>

		</tr>
		<tr>
			<td colspan="2">
				<div class="col-md-10 col-sm-10 col-xs-12">
					<table>
						<tr>
							<td>
								<table class="table table-bordered col-md-4 col-sm-4">
									<thead>
										<tr>
											<th>Government Issued ID (i.e.Passport, GSIS, SSS, PRC, Driver's License, etc.) <br>
											 PLEASE INDICATE ID Number and Date of Issuance		</th>
										</tr>
									</thead>
									<tbody>
										<tr>
											<td>Government Issued ID:</td>
										</tr>
										<tr>
											<td>ID/License/Passport No.: </td>
										</tr>
										<tr>
											<td>Date/Place of Issuance:	</td>
										</tr>
									</tbody>
								</table>
							</td>
							<td style="width: 110px"></td>
							<td>
								<table class="table table-bordered col-md-4 col-sm-4" style="width: 300px">
									<thead>
										<tr style="height: 58px;">
											<td></td>
										</tr>
									</thead>
									<tbody>
										<tr>
											<td></td>
										</tr>
										<tr>
											<td>Signature (Sign inside the box)</td>
										</tr>
										<tr>
											<td></td>
										</tr>
										<tr>
											<td>Date Accomplished</td>
										</tr>
									</tbody>
								</table>
							</td>
							<td>
								<div class="table-bordered" style="width: 180px; margin-left: 84%; height: 165px;">
								<center><br><br><br><br><br><br><br><span style="background-color:#EAEAEA">Right Thumbmark</span></center>
								</div>
							</td>
						</tr>
					</table>
				</div>
			</td>
		</tr>
		<tr>
			<td colspan="2">
			SUBSCRIBED AND SWORN to before me this ___________________________, affiant exhibiting his/her validly issued government ID as indicated above.												
			<center>
				<div class="table-bordered" style="width: 400px; height: 165px;">
					<center><br><br><br><br><br><br><br>
					<span style="background-color:#EAEAEA; padding: 10px">Person Administering Oath</span></center>
				</div>
			</center>
			</td>
		</tr>
		<tr>
			<td colspan="2">
			<span class="pull-right" style="color: red;"><i>CS FORM 212 (Revised 2017),  Page 4 of 4</i></span>	
			</td>
		</tr>
	</tbody>
</table>

<button type="button" class="btn btn-success pull-right" onclick="window.print()"><i class="fa fa-print"></i> Print</button>
<?= $this->Form->end() ?>