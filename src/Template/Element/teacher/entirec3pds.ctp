<?= $this->Form->create($users, [ 'class' => 'form form-horizontal form-label-left', 'url' => '/users/teachersavepds/'.$users['id'] ]) ?>
<table class="table table-bordered" id="voluntaryTable" style="margin-top: 10px;">
	<thead></thead>
	<tbody>
		<tr>
			<td colspan="7" style="background-color: #8094A8; color: white"><i> VI. VOLUNTARY WORK OR INVOLVEMENT IN CIVIC / NON-GOVERNMENT / PEOPLE / VOLUNTARY ORGANIZATION/S										</i></td>
		</tr>
		<tr>
			<td style="background-color: #EAEAEA;">
				<center>29. NAME AND ADDRESS OF ORGANIZATION <br> <small>(Write in full)</small></center>	
			</td>
			<td colspan="2" style="background-color: #EAEAEA;">
				<center>INCLUSIVE DATE <br><small>mm/dd/yy</small><br> 
				<span class="pull-left">FROM</span>
				<span class="pull-right">TO</span>
			</td>
			<td style="background-color: #EAEAEA;"><center>NUMBER OF HOURS</center></td>
			<td style="background-color: #EAEAEA;">
				<center>POSITION/ NATURE OF WORK</center>
			</td>
		</tr>
		<?php
			$rows = 6; // define number of rows
			$cols = 4;// define number of columns
			for($tr=0;$tr<=$rows;$tr++){
			    echo "<tr>";
			        for($td=0;$td<=$cols;$td++){ 
		?>
		<td><input type='text' class="form-control voluntary<?php echo $td; ?> voluntarycolumn_<?php echo $td.'_'.$tr; ?>" data-id="voluntary<?php echo $td; ?>"> </td>
		<?php 
			}
			    echo "</tr>";
			}
		?>
		<tr>
			<td colspan="6" style="color: red"><center><small><i>(Continue on separate sheet if necessary)</i></small></center></td>
		</tr>
	</tbody>
</table>
<table class="table table-bordered" id="learningTable" style="margin-top: -20px;">
	<thead></thead>
	<tbody>
		<tr>
			<td colspan="8" style="background-color: #8094A8; color: white"><i> VII.  LEARNING AND DEVELOPMENT (L&D) INTERVENTIONS/TRAINING PROGRAMS ATTENDED										</i></td>
		</tr>
		<tr>
			<td colspan="8" style="background-color: #8094A8; color: white"><small><i>(Start from the most recent L&D/training program and include only the relevant L&D/training taken for the last five (5) years for Division Chief/Executive/Managerial positions)	</small></tr>
		<tr>
			<td style="background-color: #EAEAEA;">
				<center>30. TITLE OF LEARNING AND DEVELOPMENT INTERVENTIONS/TRAINING PROGRAMS <small> (Write in full)</small></center>	
			</td>
			<td colspan="2" style="background-color: #EAEAEA;">
				<center>INCLUSIVE DATE OF ATTENDANCE<br><small>mm/dd/yy</small><br> 
				<span class="pull-left">FROM</span>
				<span class="pull-right">TO</span>
			</td>
			<td style="background-color: #EAEAEA;"><center>NUMBER OF HOURS</center></td>
			<td style="background-color: #EAEAEA;">
				<center>Type of ID <br>( Managerial/Supervisory/Technical/etc)</center>
			</td>
			<td style="background-color: #EAEAEA;width:230px">
				<center> CONDUCTED/ SPONSORED BY <br><small>(Write in full) </small></center>
			</td>
		</tr>
		<?php	
			$rows = 20; // define number of rows
			$cols = 5;// define number of columns
			for($tr=0;$tr<=$rows;$tr++){
			    echo "<tr>";
			        for($td=0;$td<=$cols;$td++){ 
		?>
		<td><input type='text' class="form-control learning<?php echo $td; ?> learningcolumn_<?php echo $td.'_'.$tr; ?>" data-id="learning<?php echo $td; ?>"> </td>
		<?php 
			}
			    echo "</tr>";
			}
		?>
		<tr>
			<td colspan="8" style="color: red"><center><small><i>(Continue on separate sheet if necessary)</i></small></center></td>
		</tr>
	</tbody>
</table>
<?php $arinput = [
	'voluntary_name_org','voluntary_inclusive_from','voluntary_inclusive_to','voluntary_hrs_no','voluntary_position',
	'learning_title','learning_inclusive_from','learning_inclusive_to',
	'learning_hrs_no','learning_id_type','learning_conducted'
]; ?>
<?php for($i=0; $i<count($arinput); $i++) {?>
	<?= $this->Form->control($arinput[$i], ['label' => false, 'type' => 'hidden']); ?>
<?php } ?>
<?php
	$array_replace = array('[',']','"');
	$splitpapers = explode(',', str_replace($array_replace, "", $users['papers']));
	$papers = '';
	foreach ($splitpapers as $value) {
		if(in_array('PDS', $splitpapers)) {
			$saln = '';
		} else {
			$saln = 'PDS';
		}
		$papers .= $value.',';
	}
	$papers = json_encode($papers.$saln);
	if(sizeof($splitpapers) == 3 ) {
		$status = 1;
	} else if(sizeof($splitpapers) == 2) {
		$status = 1;
	} else {
		$status = 0;
	}
?>
<?= $this->Form->control('status', ['label' => false, 'type' => 'hidden','value' => $status]); ?>
<?= $this->Form->control('papers', ['label' => false, 'type' => 'hidden','value' => $papers]); ?>
<?= $this->Form->button(__('Save'),['class'=>'btn btn-primary pull-right','id' => 'saveUser']); ?>
<button type="button" class="btn btn-success" onclick="window.print()"><i class="fa fa-print"></i> Print</button>
<?= $this->Form->end() ?>