<?php
  $session = $this->request->session();
  $user_data = $session->read('Auth.User');
  if(!empty($user_data)) {
?>
<div class="left_col scroll-view">
<div class="navbar nav_title" style="border: 0;">
  <h2><a href="<?php echo BASE_URL; ?>users" class="site_title"><img src="<?php echo $this->request->webroot; ?>img/lucsoonlogo.jpg" alt="..." class="img-circle profile_img" style="    width: 38px;
    margin-left: 0;
    margin-top: 0;"> <span>LNHS <small> Teacher Profiling</small></span></a></h2>
</div>

<div class="clearfix"></div>

<!-- menu profile quick info -->
<div class="profile clearfix">
  <div class="profile_pic">
    <img src="<?php echo $this->request->webroot; ?>img/default.png" alt="..." class="img-circle profile_img profilephoto">
  </div>
  <div class="profile_info">
    <span>Welcome,</span>
    <h2><?= ucfirst($user_data['firstname']).' '.ucfirst($user_data['lastname']);?></h2>
  </div>
</div>
<!-- /menu profile quick info -->

<br />

<!-- sidebar menu -->
<div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
	<div class="menu_section">
		<h3>General</h3>
		<ul class="nav side-menu">
			<?php if ($user_data['usertype'] == 2) { ?>
				<li><a href="<?= BASE_URL ?>users/adminmasterlist"><i class="fa fa-user"></i> Masterlist </a></li>
				<li><a href="<?= BASE_URL ?>users/adminclearedfaculty"><i class="fa fa-user "></i> Cleared Faculty </a></li>
				<li><a href="<?= BASE_URL ?>users/admindepartment"><i class="fa fa-dashboard "></i> Department </a></li>
				<li><a href="<?= BASE_URL ?>users/adminsetdeadline"><i class="fa fa-calendar "></i> Deadline </a></li>
				<li><a href="<?= BASE_URL ?>users/adminleavelist"><i class="fa fa-calendar "></i> Leave </a></li>
				<li><a href="<?= BASE_URL ?>users/admintrainingseminars"><i class="fa fa-user "></i> Trainings & Seminars </a></li>
			<?php  } if ($user_data['usertype'] == 1) { ?>
				<li><a href="<?= BASE_URL ?>users/teacherfacultypds"><i class="fa fa-user" style="color: #FFC300;"></i> Personal Data Sheet</a></li>
				<li><a href="<?= BASE_URL ?>users/teachertrainingseminar"><i class="fa fa-user" style="color: #DAF7A6"></i>Trainings and Seminars </a></li>
				<li><a href="<?= BASE_URL ?>users/teacherservicerecords"><i class="fa fa-file" style="color: #2471A3"></i> Service Records </a></li>
				<li><a href="<?= BASE_URL ?>users/teacherleavelist"><i class="fa fa-calendar "></i> Leave </a></li>
				<li><a href="<?= BASE_URL ?>users/teachersaln"><i class="fa fa-money" style="color: #D4AC0D"></i> S A L N  </a></li>
			<?php } ?>
		</ul>
	</div>
</div>
<!-- /sidebar menu -->
</div>
<?php } ?>
