<?= $this->Html->css([
	'chosen', //bootstrap
]);?>
<!-- Modal -->
<div class="modal fade" id="newMessage" role="dialog">
	<div class="modal-dialog">

		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">New Message</h4>
			</div>
			<div class="modal-body">
				<p>
					<div class="ui grid">
						<div class="twelve wide column centered">
							<form id="messageForm" class="ui form">
								<div class="field">
									<label>Recepient*</label>
									<div class="ui input icon">
									<select multiple="" class="chosen-select" id="teachers" data-placeholder="Choose a Recepient...">
											<?php foreach ($forSelectUsers as $value) {
												echo '<option value="'.$value['userid'].'">'.ucfirst($value['firstname']).' '.ucfirst($value['lastname']).'</option>';
											} ?>
										</select>
									</div>
								</div>
								<div class="field">
									<label>Title*</label>
									<div class="ui input icon">
									<input type="text" class="form-control" id="title" />
									</div>
								</div>
								<div class="field">
									<label>Message*</label>
									<div class="ui input icon">
									<textarea rows="5" class="form-control" id="messageContent"></textarea>
									</div>
								</div>
								<?php
									$session = $this->request->session();
									$user_data = $session->read('Auth.User');
								?>
								<input type="hidden" id="userid" value="<?php echo $user_data['userid']; ?>">
							</form>
						</div>
					</div>
				</p>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-primary btn_ok_mdl">Send</button>
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			</div>
		</div>

	</div>
</div>
<?= $this->Html->script(['chosen.jquery.min']);?>
<script type="text/javascript">
	$(function () {

		$('#newmessage').click( function() {
			$("#messageContent").val("");
			$('#title').val("");
			$("#teachers").val('').trigger("chosen:updated");
		});

		$('.ui-dialog').css('overflow','visible');
		$('.ui-dialog-content').css('overflow','visible');
		$('.chosen-select').chosen({allow_single_deselect:true});

		//resize the chosen on window resize
		$(window).on('resize.chosen', function() {
		var w = $('.chosen-select').parent().width();
			$('.chosen-select').next().css({'width':560});
		}).trigger('resize.chosen');

		$(".btn_ok_mdl").click(function(){
			var teachers = [];
			$.each($("#teachers option:selected"), function(){
				teachers.push($(this).val());
			});
			sendMessage(teachers.join(','));
		});

	});

	function sendMessage(teachers) {

		var messages = $("#messageContent").val();
		var title = $('#title').val();
		var msg = '';

		if(teachers == "" || title == "" || messages == "") { 
			alert('Required field is empty.'); 
			return false;
		} 
		
		$.ajax({
			url:'sendMessage',
			type:'post',
			data:{  sender: $("#userid").val(), recipient: teachers, message:messages, title: title },
			success: function(data) {
				location.reload();
			}
		});

	}
</script>