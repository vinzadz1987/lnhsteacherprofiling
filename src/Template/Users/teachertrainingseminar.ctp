<div class="">
	<div class="page-title">
		<div class="title_left">
			<h3> Teacher Board </h3>
		</div>
	</div>
	<div class="clearfix"></div>
	<div class="row">
		<div class="col-md-12 col-sm-12 col-xs-12">
			<div class="x_panel">
				<div class="x_title">
					<h2>Trainings and Seminar</h2>
					<div class="clearfix"></div>
				</div>
				<div class="x_content">
					<?php foreach($training_seminar as $seminar) { ?>
						
						<div class="panel-group">
							<div class="panel panel-primary">
								<div class="panel-heading"><b><i><?php echo ($seminar['type']==1) ? "Seminar" : "Trainings"; ?> : </i></b><?php echo ucfirst($seminar['name']) ?> <span class="pull-right"><?php echo $seminar['date'] ?></span></div>
								<div class="panel-body">
									<p><?php echo ucfirst($seminar['description']) ?></p>
									<p><b>Venue:</b> <?php echo $seminar['venue'] ?></p>
								</div>
							</div>
						</div>
					<?php } ?>
				</div>
			</div>
		</div>
	</div>
</div>
<?= $this->Html->script(['teachertraining']);?>
<script>
	function canceljoin(id) {
		console.log($(".project_detail").find("#joiner"+id).val());
	}
</script>
