<div class="">
	<div class="page-title">
		<div class="title_left">
			<h3> Teacher Board </h3>
		</div>
	</div>
	<style type="text/css">
		.name, .birth {
		  border: 0;
		  outline: 0;
		  background: transparent;
		  border-bottom: 1px solid #c4acac;
		  width: 497px;
		  text-transform: uppercase;
		  text-align: center;
		}
	</style>
	<div class="clearfix"></div>
	<div class="row">
		<div class="col-md-12 col-sm-12 col-xs-12">
			<div class="x_panel">
				<div class="x_title">
					<h2>Service Record</h2>
					<div class="clearfix"></div>
				</div>
				<div class="x_content">
					<table class="table">
						<tr>
							<td style="border: 0px"><?php  echo $this->Html->image('lucsoonlogo.jpg', ['alt'=>'personal data sheet', 'style' => 'width: 120px; float: right;']); ?></td>
							<td style="text-align: center; font-size: 18px;border: 0px">
								Republic of the Philippines<br>Department of Education <br> Region VIII, Eastern Visayas <br> <b>DIVISION OF BILIRAN</b> <br> Naval, Biliran</td>
							<td style="border: 0px"><?php  echo $this->Html->image('educationlogo.png', ['alt'=>'personal data sheet', 'style' => 'width: 120px;']); ?></td>
						</tr>
					</table>
					<center>
						<table class="table">
							<tr>
								<td style="border: 0px">NAME  </td>
								<td style="border: 0px">: <input type="text" name="" class="name" value="<?= $users['lastname']; ?>, <?= $users['firstname']; ?> <?= $users['middlename']; ?>"><span><br>(Last Name) (Given Name) (Middle Name)</span></td>
								<td style="border: 0px">( If married woman, give maiden name )</td>
							</tr>
							<tr>
								<td style="border: 0px">BIRTH  </td>
								<td style="border: 0px">: <input type="text" name="" class="birth" value="<?php $time = strtotime($users['dateofbirth']); echo date('F d, Y',$time); ?>, <?= $users['placeofbirth'];?>"><br>(Date) (Place)</span></td>
								<td style="border: 0px">( Data herein should be check from birth or baptismal certificate or some other <br> reliable documents. )</td>
							</tr>
						</table>
						<p>This is to certify that the above named employee actually rendered services in this Office as shown by the  "SERVICE RECORD" below each line of which is supported by appointments and other papers actually issue and approved by the authorities concerned.</p>
					</center>
					<table class="table table-bordered" id="servicerecords">
						<thead>
							<tr>
								<th colspan="2" style="text-align: center;">SERVICES <br>
									<small>(Inclusive Dates)<br></small>
									<span class="pull-left">FROM</span>
									<span class="pull-right">TO</span>
								</th>
								<th style="text-align: center;">Designation</th>
								<th style="text-align: center;">Record of Status</th>
								<th style="text-align: center;">Salary</th>
								<th style="text-align: center;">BRANCH</th>
								<th style="text-align: center;">L/V <br>W/O<br> PAY</th>
								<th style="text-align: center;">SEP'N <br>Date/Cause</th>
							</tr>
						</thead>
						<tbody>
						<?= $this->Form->create($users) ?>
						</tr>
							<?php	
								$rows = 30; // define number of rows
								$cols=7;// define number of columns
								for($tr=0;$tr<=$rows;$tr++){
									echo "<tr>";
										for($td=0;$td<=$cols;$td++){ 
							?>
							<td><input type='text' class="form-control servicerecord<?php echo $td; ?> servicerecordcolumn_<?php echo $td.'_'.$tr; ?>" data-id="servicerecord<?php echo $td; ?>"> </td>
							<?php 
								}
									echo "</tr>";
								}
							?>
						<tr>
						</tbody>
					</table>
					<p>Issued in compliance with Executive Order No.  54 dated August 10, 1954 and accordance with the Circular No. 58 dated August 10, 1954 of the system.</p>
					<p><?= $this->Form->control('servicerecordsdate', ['label' => false, 'type' => 'text','class' => "name"]); ?><br> <span style="margin-left: 207px;">(Date)</span></p>
					<p style="margin-left: 81%;">CERTIFIED CORRECT: <br><br> <span style="text-decoration: underline; font-weight: bold;"> NAME LASTNAME </span><br> <i> Administrative Officer V </i></p>
					<?php $arinput = [
						'servicerecordsfrom','servicerecordsto','servicerecordsdesignation','servicerecordsstatus','servicerecordssalary','servicerecordsbranch',
						'servicerecordslvpay','servicerecordsseparation'
					]; ?>
					<?php for($i=0; $i<count($arinput); $i++) {?>
						<?= $this->Form->control($arinput[$i], ['label' => false, 'type' => 'hidden']); ?>
					<?php } ?>
					<?php
						$array_replace = array('[',']','"');
						$splitpapers = explode(',', str_replace($array_replace, "", $users['papers']));
						$papers = '';
						foreach ($splitpapers as $value) {
							if(in_array('SERVICE RECORDS', $splitpapers)) {
								$saln = '';
							} else {
								$saln = 'SERVICE RECORDS';
							}
							$papers .= $value.',';
						}
						$papers = json_encode($papers.$saln);
						if(sizeof($splitpapers) == 3 ) {
							$status = 1;
						} else if(sizeof($splitpapers) == 2) {
							$status = 1;
						} else {
							$status = 0;
						}
					?>
					<?= $this->Form->control('status', ['label' => false, 'type' => 'hidden','value' => $status]); ?>
					<?= $this->Form->control('papers', ['label' => false, 'type' => 'hidden','value' => $papers]); ?>
					<?= $this->Form->button(__('Save'),['class'=>'btn btn-primary pull-right','id' => 'saveUser']); ?>
					<button type="button" class="btn btn-success" onclick="window.print()"><i class="fa fa-print"></i> Print</button>
					<?= $this->Form->end() ?>
				</div>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
	$(function() {
		$("#servicerecordsdate").datepicker({
			autoclose: true,
			todayHighlight: true
		});
		$(".servicerecord0 , .servicerecord1 ").mask('99/99/9999');
		var arrayTable = ['8_servicerecord'];
		for(x=0;x<arrayTable.length;x++) {
			for(i=0;i<arrayTable[x].split('_')[0];i++) {
				$("."+arrayTable[x].split('_')[1]+i).keyup(function(){
					getdata($(this).data('id'));
				});
			}
		}
		distribute_data();
	});
	function distribute_data() {
		var eData = ['servicerecordsfrom','servicerecordsto','servicerecordsdesignation','servicerecordsstatus','servicerecordssalary',
		'servicerecordsbranch','servicerecordslvpay','servicerecordsseparation'];
	for(y=0; y<eData.length;y++) {
		var pEData = $("#"+eData[y]).val().split(',');
		$.each(pEData, function(i) {
			$("#servicerecords").find('td input.servicerecordcolumn_'+y+'_'+i).each(function() {
				$(this).val(pEData[i]);
			});
		});
	}
}
	function getdata(rowname) {
	var iData = new Array();
	$("."+rowname).each(function() {
		iData.push($(this).val());
	});
	var input = "";
	switch (rowname) {
		case 'servicerecord0':
			input = "servicerecordsfrom";
		break;
		case 'servicerecord1':
			input = "servicerecordsto";
		break;
		case 'servicerecord2':
			input = "servicerecordsdesignation";
		break;
		case 'servicerecord3':
			input = "servicerecordsstatus";
		break;
		case 'servicerecord4':
			input = "servicerecordssalary";
		break;
		case 'servicerecord5':
			input = "servicerecordsbranch";
		break;
		case 'servicerecord6':
			input = "servicerecordslvpay";
		break;
		case 'servicerecord7':
			input = "servicerecordsseparation";
		break;
	}
	$("#"+input).val(iData);
}
</script>