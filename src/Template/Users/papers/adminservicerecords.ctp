<div class="">
	<div class="page-title">
		<div class="title_left">
			<h3> Teacher Board </h3>
		</div>
	</div>
	<style type="text/css">
		.name, .birth {
		  border: 0;
		  outline: 0;
		  background: transparent;
		  border-bottom: 1px solid #c4acac;
		  width: 497px;
		  text-transform: uppercase;
		}
	</style>
	<div class="clearfix"></div>
	<div class="row">
		<div class="col-md-12 col-sm-12 col-xs-12">
			<div class="x_panel">
				<div class="x_title">
					<h2>Service Record</h2>
					<div class="clearfix"></div>
				</div>
				<div class="x_content">
					<table class="table">
						<tr>
							<td style="border: 0px"><?php  echo $this->Html->image('lucsoonlogo.jpg', ['alt'=>'personal data sheet', 'style' => 'width: 120px; float: right;']); ?></td>
							<td style="text-align: center; font-size: 18px;border: 0px">
								Republic of the Philippines<br>Department of Education <br> Region VIII, Eastern Visayas <br> <b>DIVISION OF BILIRAN</b> <br> Naval, Biliran</td>
							<td style="border: 0px"><?php  echo $this->Html->image('educationlogo.png', ['alt'=>'personal data sheet', 'style' => 'width: 120px;']); ?></td>
						</tr>
					</table>
					<center>
						<table class="table">
							<tr>
								<td style="border: 0px">NAME  </td>
								<td style="border: 0px">: <input type="text" name="" class="name" value="<?= $users['lastname']; ?>, <?= $users['firstname']; ?> <?= $users['middlename']; ?>" disabled="disabled"><span><br>(Last Name) (Given Name) (Middle Name)</span></td>
								<td style="border: 0px">( If married woman, give maiden name )</td>
							</tr>
							<tr>
								<td style="border: 0px">BIRTH  </td>
								<td style="border: 0px">: <input type="text" name="" class="birth" value="<?php $time = strtotime($users['dateofbirth']); echo date('F d, Y',$time); ?>, <?= $users['placeofbirth'];?>" disabled="disabled"><br>(Date) (Place)</span></td>
								<td style="border: 0px">( Data herein should be check from birth or baptismal certificate or some other <br> reliable documents. )</td>
							</tr>
						</table>
						<p>This is to certify that the above named employee actually rendered services in this Office as shown by the  "SERVICE RECORD" below each line of which is supported by appointments and other papers actually issue and approved by the authorities concerned.</p>
					</center>
					<table class="table table-bordered">
						<thead>
							<tr>
								<th style="text-align: center;">SERVICES <br><small>(Inclusive Dates)<br> From To</small></th>
								<th style="text-align: center;">Designation</th>
								<th style="text-align: center;">Record of Status</th>
								<th style="text-align: center;">Salary</th>
								<th style="text-align: center;">BRANCH</th>
								<th style="text-align: center;">L/V <br>W/O<br> PAY</th>
								<th style="text-align: center;">SEP'N <br>Date/Cause</th>
							</tr>
						</thead>
						<tbody>
						<?php for ($i = 1 ; $i <= 30; $i++ ){ ?>
						<tr>
							<th>
								<div class="form-row">
									<div class="col-md-12">
									<input type="text" class="form-control pull-right datefrom" name="service_records_dates_from[]" style="width: 100px"> 
									<input type="text" class="form-control pull-left dateto" name="service_records_dates_to[]" style="width: 100px">
									</div>
								</div>
							</th>
							<td><input type="text" class="form-control" name="service_records_designation[]"></td>
							<td><input type="text" class="form-control" name="service_records_status[]"></td>
							<td><input type="text" class="form-control" name="service_records_salary[]"></td>
							<td><input type="text" class="form-control" name="service_records_branch[]"></td>
							<td><input type="text" class="form-control" name="service_records_lwpay[]"></td>
							<td><input type="text" class="form-control" name="service_records_sepdate[]"></td>
						</tr>
						<?php } ?>
						</tbody>
					</table>
					<p>Issued in compliance with Executive Order No.  54 dated August 10, 1954 and accordance with the Circular No. 58 dated August 10, 1954 of the system.</p>
					<p><input type="text" name="" class="name"><br> <span style="margin-left: 207px;">(Date)</span></p>
					<p style="margin-left: 81%;">CERTIFIED CORRECT: <br><br> <span style="text-decoration: underline; font-weight: bold;"> NAME LASTNAME </span><br> <i> Administrative Officer V </i></p>
					<button class="btn btn-default" onclick="window.print();"><i class="fa fa-print"></i> Print</button>
				</div>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
	$(function() {
		$(".datefrom, .dateto").datepicker({
			autoclose: true,
			todayHighlight: false
		});
	});
</script>