<div class="">
	<div class="page-title">
		<div class="title_left">
			<h3> Teacher Profile </h3>
		</div>
	</div>
	<div class="clearfix"></div>
	<div class="row">
		<div class="col-md-12 col-sm-12 col-xs-12">
			<div class="x_panel">
				<div class="x_title" style="padding: 16px 8px 1px; border-bottom: 0px">
					<h2>PERSONAL DATA SHEETS (PDS) <small>CS FORM NO. 212 <i>Revised 2017</i></small></h2>
					<div class="clearfix"></div>
				</div>
				<div class="x_content">
				<!-- Smart Wizard -->
                    <div id="wizard_verticle" class="form_wizard wizard_verticle">
                      <ul class="list-unstyled wizard_steps">
						<li>
							<a href="#step-1">
								<span class="step_no">1</span>
							</a>
						</li>
						<li>
							<a href="#step-2">
								<span class="step_no">2</span>
							</a>
						</li>
						<li>
							<a href="#step-3">
								<span class="step_no">3</span>
							</a>
						</li>
						<li>
							<a href="#step-4">
								<span class="step_no">4</span>
							</a>
						</li>
                      </ul>

                      <div id="step-1"><BR>
                        <h2 class="StepTitle" style="height: 31px;">C1</h2>
                        <?= $this->Form->create($users, [ 'class' => 'form form-horizontal form-label-left', 'url' => '/users/teachersavepds/'.$users['id'] ]) ?>
						<table class="table table-bordered" id="c1">
							<thead>
								<tr>
									<th colspan="4">
										<i><b>CS FORM NO. 212<br>
										Revised 2017</b></i>
										<center style="font-size: 28px">PERSONAL DATA SHEET</center>
										<br><br>
										<i><small><b>WARNING: Any misrepresentation made in the Personal Data Sheet and the Work Experience Sheet shall cause the filing of administrative/criminal case/s against the person concerned.</b></small></i><br><br>
										<i>
											<small>
												<b>READ THE ATTACHED GUIDE TO FILLING OUT THE PERSONAL DATA SHEET (PDS) BEFORE ACCOMPLISHING THE PDS FORM.</b>
											</small>
										</i>
									</th>
								</tr>
								<tr>
									<th colspan="3"><i>
											<small>
												Print legibly. Tick appropriate box es (  ) and use separate if necessary. Indicat N/A if not applicable. DO NOT ABBREVIATE
											</small>
										</i>
									</th>
									<th>
										<small> <input type="text" value="1. CS ID No. (Do not fill up, For CSC use only)" class="form-control col-md-7 col-xs-12 input-sm" disabled="disabled"></small>
									</th>
								</tr>
								<tr>
									<th colspan="4" style="background-color: #8094A8; color: white"><i>I. PERSONAL INFORMATION</i></th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td style="background-color: #EAEAEA;">2. SURNAME</td>
									<td colspan="3"><?= $this->Form->control('lastname', ['label' => false, 'class' => 'form-control col-md-7 col-xs-12 input-sm']); ?></td>
								</tr>
								<tr>
									<td style="background-color: #EAEAEA;">FIRSTNAME</td>
									<td colspan="2"><?= $this->Form->control('firstname', ['label' => false, 'class' => 'form-control col-md-7 col-xs-12 input-sm']); ?></td>
									<td><small>NAME EXTENSION (JR., SR)</small><br> <?= $this->Form->control('name_extension', ['label' => false, 'class' => 'form-control col-md-7 col-xs-12 input-sm namextens']); ?></td>
								</tr>
								<tr>
									<td style="background-color: #EAEAEA;">MIDDLE NAME</td>
									<td colspan="3"><?= $this->Form->control('middlename', ['label' => false, 'class' => 'form-control col-md-7 col-xs-12 input-sm']); ?></td>
								</tr>
								<tr>
									<td style="background-color: #EAEAEA;">3: DATE OF BIRTH<br><small>(mm/dd/yyyy)</small></td>
									<td><?= $this->Form->control('dateofbirth', ['label' => false, 'class' => 'form-control col-md-7 col-xs-12 input-sm', 'type' => 'text']); ?></td>
									<td  style="background-color: #EAEAEA; ">16. CITIZENSHIP</td>
									<td ><?= $this->Form->control('citizenship', ['label' => false, 'class' => 'form-control col-md-7 col-xs-12 input-sm', 'type' => 'text']); ?></td>
								</tr>
								<tr>
									<td style="background-color: #EAEAEA; ">4: PLACE OF BIRTH</td>
									<td><?= $this->Form->control('placeofbirth', ['label' => false, 'class' => 'form-control col-md-7 col-xs-12 input-sm', 'type' => 'text']); ?></td>
									<td style="background-color: #EAEAEA; "><small>If holder of dual citizenship</small></td>
									<td>Pls. indicate country:</td>
								</tr>
								<tr>
									<td style="background-color: #EAEAEA;">5: SEX</td>
									<td>
										<?= $this->Form->input('sex', [
												'type' => 'select',
												'class' => 'form-control input-sm',
												'label'=> false,
												'options' => [
													'' => 'Select Gender',
													'Male' => 'Male',
													'Female' => 'Female'
												]
											]);
										?>
									</td>
									<td style="background-color: #EAEAEA; "><small>pleas indicate the details</small></td>
									<td><?= $this->Form->control('other_country', ['label' => false, 'class' => 'form-control col-md-7 col-xs-12 input-sm', 'type' => 'text']); ?></td>
								</tr>
								<tr>
									<td style="background-color: #EAEAEA;">6: CIVIL STATUS</td>
									<td>
										<?= $this->Form->input('civilstatus', [
												'type' => 'select',
												'class' => 'form-control input-sm',
												'label'=> false,
												'options' => [
													'' => 'Select Civil Status',
													'Single' => 'Single',
													'Married ' => 'Married ',
													'Divorced ' => 'Divorced',
													'Separated ' => 'Separated ',
													'Widowed' => 'Widowed'
												]
											]);
										?>
									</td>
									<td style="background-color: #EAEAEA; ">17: RESIDENTIAL ADDRESS</td>
									<td>
										<?= $this->Form->control('residence_address_block', ['label' => 'House/Block/Lot No.', 'class' => 'form-control col-md-4 col-xs-12 input-sm']); ?>
										<?= $this->Form->control('residence_address_street', ['label' => 'Street', 'class' => 'form-control col-md-4 col-xs-12 input-sm']); ?>
										<?= $this->Form->control('residence_address_subdivision', ['label' => 'Subdivision/Village', 'class' => 'form-control col-md-4 col-xs-12 input-sm']); ?>
										<?= $this->Form->control('residence_address_brgy', ['label' => 'Barangay', 'class' => 'form-control col-md-4 col-xs-12 input-sm']); ?>
									</td>
								</tr>
								<tr>
									<td style="background-color: #EAEAEA;">7: HEIGHT<small>(m)</small></td>
									<td><?= $this->Form->control('height', ['label' => false, 'class' => 'form-control col-md-7 col-xs-12 input-sm']); ?></td>
									<td style="background-color: #EAEAEA;"></td>
									<td>
										<?= $this->Form->control('residence_address_city', ['label' => 'City/Municipality', 'class' => 'form-control col-md-4 col-xs-12 input-sm']); ?>
										<?= $this->Form->control('residence_address_province', ['label' => 'Province', 'class' => 'form-control col-md-4 col-xs-12 input-sm']); ?>
									</td>
								</tr>
								<tr>
									<td style="background-color: #EAEAEA;">8: WEIGHT<small>(kg)</small></td>
									<td><?= $this->Form->control('weight', ['label' => false, 'class' => 'form-control col-md-7 col-xs-12 input-sm']); ?></td>
									<td style="background-color: #EAEAEA;">ZIP CODE</td>
									<td><?= $this->Form->control('residence_address_zip', ['label' => false, 'class' => 'form-control col-md-4 col-xs-12 input-sm']); ?></td>
								</tr>
								<tr>
									<td style="background-color: #EAEAEA;">9: BLOOD TYPE</td>
									<td>
										<?= $this->Form->input('bloodtype', [
										'type' => 'select',
										'class' => 'form-control input-sm',
										'label'=> false,
										'options' => [
											'' => 'Select Blood type',
											'O-' => 'O-',
											'O+' => 'O+',
											'A-' => 'A-',
											'A+' => 'A+',
											'B-' => 'B-',
											'B+' => 'B+',
											'AB-' => 'AB-',
											'AB+' => 'AB+'
										]
									]);
								?>
									</td>
									<td style="background-color: #EAEAEA;"> 18. PERMANENT ADDRESS</td>
									<td>
										<?= $this->Form->control('permanent_address_block', ['label' => 'House/Block/Lot No.', 'class' => 'form-control col-md-4 col-xs-12 input-sm']); ?>
										<?= $this->Form->control('permanent_address_street', ['label' => 'Street', 'class' => 'form-control col-md-4 col-xs-12 input-sm']); ?>
									</td>
								</tr>
								<tr>
									<td style="background-color: #EAEAEA;">9: GSIS ID NO. </td>
									<td><?= $this->Form->control('gsisidno', ['label' => false, 'class' => 'form-control col-md-7 col-xs-12 input-sm']); ?></td>
									<td style="background-color: #EAEAEA;"></td>
									<td>
										<?= $this->Form->control('permanent_address_subdivision', ['label' => 'Subdivision/Village', 'class' => 'form-control col-md-4 col-xs-12 input-sm']); ?>
										<?= $this->Form->control('permanent_address_brgy', ['label' => 'Barangay', 'class' => 'form-control col-md-4 col-xs-12 input-sm']); ?>
									</td>
								</tr>
								<tr>
									<td style="background-color: #EAEAEA; ">11. PAG-IBIG ID NO.</td>
									<td><?= $this->Form->control('pagibigidno', ['label' => false, 'class' => 'form-control col-md-7 col-xs-12 input-sm']); ?></td>
									<td style="background-color: #EAEAEA;"></td>
									<td>
										<?= $this->Form->control('permanent_address_city', ['label' => 'City/Municipality', 'class' => 'form-control col-md-4 col-xs-12 input-sm']); ?>
										<?= $this->Form->control('permanent_address_province', ['label' => 'Province', 'class' => 'form-control col-md-4 col-xs-12 input-sm']); ?>
									</td>
								</tr>
								<tr>
									<td style="background-color: #EAEAEA;">11. PHILHEALTH NO..</td>
									<td><?= $this->Form->control('philhealthno', ['label' => false, 'class' => 'form-control col-md-7 col-xs-12 input-sm']); ?></td>
									<td style="background-color: #EAEAEA;">ZIP CODE</td>
									<td>
										<?= $this->Form->control('permanent_address_zip', ['label' => false, 'class' => 'form-control col-md-4 col-xs-12 input-sm']); ?>
									</td>
								</tr>
								<tr>
									<td style="background-color: #EAEAEA;">13. SSS No.</td>
									<td><?= $this->Form->control('sssno', ['label' => false, 'class' => 'form-control col-md-7 col-xs-12 input-sm']); ?></td>
									<td style="background-color: #EAEAEA;">19. TELEPHONE NO.</td>
									<td><?= $this->Form->control('telephoneno', ['label' => false, 'class' => 'form-control col-md-4 col-xs-12 input-sm']); ?></td>
								</tr>
								<tr>
									<td style="background-color: #EAEAEA;">14. TIN NO.</td>
									<td><?= $this->Form->control('tinno', ['label' => false, 'class' => 'form-control col-md-7 col-xs-12 input-sm']); ?></td>
									<td style="background-color: #EAEAEA;">20. MOBILE NO.</td>
									<td><?= $this->Form->control('mobileno', ['label' => false, 'class' => 'form-control col-md-4 col-xs-12 input-sm']); ?></td>
								</tr>
								<tr>
									<td style="background-color: #EAEAEA;">15. AGENCY EMPLOYEE NO.</td>
									<td><?= $this->Form->control('agencyemployeeno', ['label' => false, 'class' => 'form-control col-md-7 col-xs-12']); ?></td>
									<td style="background-color: #EAEAEA;">E-MAIL ADDRESS <small>(if any)</small></td>
									<td><?= $this->Form->control('email', ['label' => false, 'class' => 'form-control col-md-4 col-xs-12 input-sm']); ?></td>
								</tr>
								<tr>
									<td colspan="4" style="background-color: #8094A8; color: white"><i> II. FAMILY BACKGROUND</i></td>
								</tr>
								<tr>
									<td style="background-color: #EAEAEA;">22. SPOUSE'S SURNAME</td>
									<td>
										<?= $this->Form->control('spousesurname', ['label' => false, 'class' => 'form-control col-md-4 col-xs-12 input-sm']); ?>
										<?= $this->Form->control('agencyemployeeno', ['label' => '<small>NAME EXTENSION (JR., SR)</small>', 'class' => 'form-control col-md-4 col-xs-12','escape' => false]); ?>
									</td>
									<td style="background-color: #EAEAEA;">23. NAME of CHILDREN <br><small>(Write full name and list all)</small></td>
									<td style="background-color: #EAEAEA;">DATE OF BIRTH <small>(mm\dd\yyyy)</small></td>
								</tr>
								<tr>
									<td style="background-color: #EAEAEA;">FIRST NAME</td>
									<td><?= $this->Form->control('spousefirstname', ['label' => false, 'class' => 'form-control col-md-7 col-xs-12 input-sm']); ?></td>
									<td><input type="text" name="childname" class="form-control col-md-7 col-xs-12 input-sm childname0"></td>
									<td><input type="text" name="childdob" class="form-control col-md-7 col-xs-12 input-sm childdob0"></td>
								</tr>
								<tr>
									<td style="background-color: #EAEAEA;">MIDDLENAME</td>
									<td><?= $this->Form->control('spousemiddlename', ['label' => false, 'class' => 'form-control col-md-7 col-xs-12 input-sm']); ?></td>
									<td><input type="text" name="childname" class="form-control col-md-7 col-xs-12 input-sm childname1"></td>
									<td><input type="text" name="childdob" class="form-control col-md-7 col-xs-12 input-sm childdob1"></td>
								</tr>
								<tr>
									<td style="background-color: #EAEAEA;">OCCUPATION</td>
									<td><?= $this->Form->control('occupation', ['label' => false, 'class' => 'form-control col-md-7 col-xs-12 input-sm']); ?></td>
									<td><input type="text" name="childname" class="form-control col-md-7 col-xs-12 input-sm childname2"></td>
									<td><input type="text" name="childdob" class="form-control col-md-7 col-xs-12 input-sm childdob2"></td>
								</tr>
								<tr>
									<td style="background-color: #EAEAEA;">EMPLOYEER\BUSINESS NAME</td>
									<td><?= $this->Form->control('employerbusinessname', ['label' => false, 'class' => 'form-control col-md-7 col-xs-12 input-sm']); ?></td>
									<td><input type="text" name="childname" class="form-control col-md-7 col-xs-12 input-sm childname3"></td>
									<td><input type="text" name="childdob" class="form-control col-md-7 col-xs-12 input-sm childdob3"></td>
								</tr>
								<tr>
									<td style="background-color: #EAEAEA;">BUSINESS ADDRESS</td>
									<td><?= $this->Form->control('businessaddress', ['label' => false, 'class' => 'form-control col-md-7 col-xs-12 input-sm']); ?></td>
									<td><input type="text" name="childname" class="form-control col-md-7 col-xs-12 input-sm  childname4"></td>
									<td><input type="text" name="childdob" class="form-control col-md-7 col-xs-12  input-sm childdob4"></td>
								</tr>
								<tr>
									<td style="background-color: #EAEAEA;">TELEPHONE NO.</td>
									<td><?= $this->Form->control('spouse_telno', ['label' => false, 'class' => 'form-control col-md-7 col-xs-12 input-sm']); ?></td>
									<td><input type="text" name="childname" class="form-control col-md-7 col-xs-12 input-sm  childname5"></td>
									<td><input type="text" name="childdob" class="form-control col-md-7 col-xs-12  input-sm childdob5"></td>
								</tr>
								<tr>
									<td style="background-color: #EAEAEA;">24. FATHER'S SURNAME</td>
									<td><?= $this->Form->control('father_surname', ['label' => false, 'class' => 'form-control col-md-7 col-xs-12 input-sm']); ?></td>
									<td><input type="text" name="childname" class="form-control col-md-7 col-xs-12 input-sm  childname6"></td>
									<td><input type="text" name="childdob" class="form-control col-md-7 col-xs-12 input-sm childdob6"></td>
								</tr>
								<tr>
									<td style="background-color: #EAEAEA;">FIRSTNAME</td>
									<td>
										<?= $this->Form->control('fatherfirstname', ['label' => false, 'class' => 'form-control col-md-4 col-xs-12 input-sm']); ?>
										<?= $this->Form->control('fathernameextension', ['label' => '<small>NAME EXTENSION (JR., SR)</small>', 'class' => 'form-control col-md-4 col-xs-12 input-sm','escape' => false]); ?>
									</td>
									<td>
										<input type="text" name="childname" class="form-control col-md-7 col-xs-12 input-sm  childname7">
										<input type="text" name="childname" class="form-control col-md-7 col-xs-12 input-sm childname8" style="margin-top: 30px;">
									</td>
									<td>
										<input type="text" name="childdob" class="form-control col-md-7 col-xs-12 input-sm childdob7">
										<input type="text" name="childdob" class="form-control col-md-7 col-xs-12 input-sm childdob8" style="margin-top: 30px;">
									</td>
								</tr>
								<tr>
									<td style="background-color: #EAEAEA;">MIDDLE NAME</td>
									<td><?= $this->Form->control('fathermiddlename', ['label' => false, 'class' => 'form-control col-md-7 col-xs-12 input-sm']); ?></td>
									<td><input type="text" name="childname" class="form-control col-md-7 col-xs-12 input-sm  childname9"></td>
									<td><input type="text" name="childdob" class="form-control col-md-7 col-xs-12 input-sm childdob9"></td>
								</tr>
								<tr>
									<td style="background-color: #EAEAEA;">25. MOTHER'S MAIDEN NAME</td>
									<td><?= $this->Form->control('mother_maidenname', ['label' => false, 'class' => 'form-control col-md-7 col-xs-12 input-sm']); ?></td>
									<td><input type="text" name="childname" class="form-control col-md-7 col-xs-12 input-sm childname10"></td>
									<td><input type="text" name="childdob" class="form-control col-md-7 col-xs-12 childdob11"></td>
								</tr>
								<tr>
									<td style="background-color: #EAEAEA;">SURNAME</td>
									<td><?= $this->Form->control('mothersurname', ['label' => false, 'class' => 'form-control col-md-7 col-xs-12 input-sm']); ?></td>
									<td><input type="text" name="childname" class="form-control col-md-7 col-xs-12 input-sm  childname11"></td>
									<td><input type="text" name="childdob" class="form-control col-md-7 col-xs-12 input-sm childdob11"></td>
								</tr>
								<tr>
									<td style="background-color: #EAEAEA;">FIRSTNAME</td>
									<td><?= $this->Form->control('motherfirstname', ['label' => false, 'class' => 'form-control col-md-7 col-xs-12 input-sm']); ?></td>
									<td><input type="text" name="childname" class="form-control col-md-7 col-xs-12 input-sm  childname12"></td>
									<td>
										<input type="text" name="childdob" class="form-control col-md-7 col-xs-12 input-sm childdob12">
									</td>
								</tr>
								<tr>
									<td style="background-color: #EAEAEA;">MIDDLE NAME</td>
									<td><?= $this->Form->control('mothermiddlename', ['label' => false, 'class' => 'form-control col-md-7 col-xs-12 input-sm']); ?>
										<?= $this->Form->control('nameofchildrens', ['label' => false, 'type' => 'hidden']); ?>
										<?= $this->Form->control('childrendateofbirth', ['label' => false, 'type' => 'hidden']); ?>
									</td>
									<td colspan="2" style="color: red"><center><small><i>(Continue on separate sheet if necessary)</i></small></center></td>
								</tr>
							</tbody>
						</table>
						<table class="table table-bordered" style="margin-top: -20px;">
							<thead></thead>
							<tbody>
								<tr>
									<td colspan="7" style="background-color: #8094A8; color: white"><i> III. EDUCATION BACKGROUND</i></td>
								</tr>
								<tr>
									<td style="background-color: #EAEAEA;">26. LEVEL</td>
									<td style="background-color: #EAEAEA;"><center>(Write in full)</td>
									<td style="background-color: #EAEAEA;"><center> EDUCATION/DEGREE/COURSE<br> (Write in full)</center></td>
									<td style="background-color: #EAEAEA;">
										<center>PERIOD OF ATTENDANCE<br><br>
										FROM
										&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
										&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
										TO
									</td>
									<td style="background-color: #EAEAEA;"><center>HIGHEST LEVEL/<br>UNITS EARNED <br><small>(If not graduated)</small></td>
									<td style="background-color: #EAEAEA;"><center>YEAR <br>GRADUATED</td>
									<td style="background-color: #EAEAEA;"><center>SCHOLARSHIP/<BR>ACADEMIC<br>/HONORS RECEIVED</td>
								</tr>
								<tr>
									<td style="background-color: #EAEAEA;">ELEMENTARY</td>
									<td><input type="text" class="form-control col-md-7 col-xs-12 input-sm"></td>
									<td><input type="text" class="form-control col-md-7 col-xs-12 input-sm childdob"></td>
									<td style="width: 220px;">
										<input type="text" class="form-control col-md-3 col-xs-12 input-sm pull-left" style="width: 100px;">
										<input type="text" class="form-control col-md-3 col-xs-12 input-sm pull-right" style="width: 100px;">
									</td>
									<td><input type="text" class="form-control col-md-7 col-xs-12 input-sm"></td>
									<td><input type="text" class="form-control col-md-7 col-xs-12 input-sm childdob"></td>
									<td><input type="text" class="form-control col-md-7 col-xs-12 input-sm"></td>
								</tr>
								<tr>
									<td style="background-color: #EAEAEA;">SECONDARY</td>
									<td><input type="text" class="form-control col-md-7 col-xs-12 input-sm"></td>
									<td><input type="text" class="form-control col-md-7 col-xs-12 input-sm"></td>
									<td>
										<input type="text" class="form-control col-md-3 col-xs-12 input-sm pull-left childdob" style="width: 100px;">
										<input type="text" class="form-control col-md-3 col-xs-12 input-sm pull-right childdob" style="width: 100px;">
									</td>
									<td><input type="text" class="form-control col-md-7 col-xs-12 input-sm"></td>
									<td><input type="text" class="form-control col-md-7 col-xs-12 input-sm childdob"></td>
									<td><input type="text" class="form-control col-md-7 col-xs-12 input-sm"></td>
								</tr>
								<tr>
									<td style="background-color: #EAEAEA;">VOCATIONAL/<br>TRADE COURSE</td>
									<td><input type="text" class="form-control col-md-7 col-xs-12 input-sm"></td>
									<td><input type="text" class="form-control col-md-7 col-xs-12 input-sm"></td>
									<td>
										<input type="text" class="form-control col-md-3 col-xs-12 input-sm pull-left childdob" style="width: 100px;">
										<input type="text" class="form-control col-md-3 col-xs-12 input-sm pull-right childdob" style="width: 100px;">
									</td>
									<td><input type="text" class="form-control col-md-7 col-xs-12 input-sm"></td>
									<td><input type="text" class="form-control col-md-7 col-xs-12 input-sm childdob"></td>
									<td><input type="text" class="form-control col-md-7 col-xs-12 input-sm"></td>
								</tr>
								<tr>
									<td style="background-color: #EAEAEA;">COLLEGE</td>
									<td><input type="text" class="form-control col-md-7 col-xs-12 input-sm"></td>
									<td><input type="text" name="[]" class="form-control col-md-7 col-xs-12 input-sm"></td>
									<td>
										<input type="text" class="form-control col-md-3 col-xs-12 input-sm pull-left childdob" style="width: 100px;">
										<input type="text" name="[]" class="form-control col-md-3 col-xs-12 input-sm pull-right childdob" style="width: 100px;">
									</td>
									<td><input type="text" class="form-control col-md-7 col-xs-12 input-sm"></td>
									<td><input type="text" class="form-control col-md-7 col-xs-12 input-sm childdob"></td>
									<td><input type="text" class="form-control col-md-7 col-xs-12 input-sm"></td>
								</tr>
								<tr>
									<td style="background-color: #EAEAEA;">GRADUATE STUDIES</td>
									<td><input type="text" class="form-control col-md-7 col-xs-12 input-sm"></td>
									<td><input type="text" class="form-control col-md-7 col-xs-12 input-sm"></td>
									<td>
										<input type="text" class="form-control col-md-3 col-xs-12 input-sm pull-left childdob" style="width: 100px;">
										<input type="text" class="form-control col-md-3 col-xs-12 input-sm pull-right childdob" style="width: 100px;">
									</td>
									<td><input type="text" class="form-control col-md-7 col-xs-12 input-sm"></td>
									<td><input type="text" class="form-control col-md-7 col-xs-12 input-sm childdob"></td>
									<td><input type="text" class="form-control col-md-7 col-xs-12 input-sm"></td>
								</tr>
							</tbody>
						</table>
							<?= $this->Form->button(__('Save'),['class'=>'btn btn-primary pull-right','id' => 'saveUser']); ?>
    						<button type="button" class="btn btn-success" onclick="window.print()"><i class="fa fa-print"> Print</i></button>
    						<button type="button" class="btn btn-success" style="margin-bottom: -748px;margin-left: -133px;">Print</button>
							<?= $this->Form->end() ?>
						</div>
			<div id="step-2" style="height: 1500px;">
				<h2 class="StepTitle">C2</h2>
				test
			</div>
            <div id="step-3" style="height: 1500px;">
				<h2 class="StepTitle">C3</h2>
				test
            </div>
            <div id="step-4" style="height: 1500px;">
				<h2 class="StepTitle">C4</h2>
				test
            </div>
          </div>
                    <!-- End SmartWizard Content -->
				</div>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
	$(function () {
		$('.stepContainer').attr('style','height: 1500px;');
		$("#dateofbirth").datepicker({
			autoclose: true,
			todayHighlight: false
		});
		$("input[name=childdob]").datepicker({
			autoclose: true,
			todayHighlight: false
		});
		$("#sssno").mask("9999-9999999-9");
		$("#pagibigidno").mask("9999-9999-9999");
		$("#tinno").mask("999-999-999-999");
		$("#philhealthno").mask("99-999999999-9");
		$("input[name=childname],input[name=childdob]").on('keyup', function() {
			getChildName();
			$("#nameofchildrens").val(getChildName);
		});

		jQuery('input[name=childdob]').on('change', function () {
			 getChildBod();
 			$("#childrendateofbirth").val(getChildBod);
		 });

		var dataChildName = $("#nameofchildrens").val().split(',');
		$.each(dataChildName, function(i) {
			$("#c1").find('td input.childname'+i).each(function() {
				$(this).val(dataChildName[i]);
			});
		});

		var dataChildBod = $("#childrendateofbirth").val().split(',');
		$.each(dataChildBod, function(i) {
			$("#c1").find('td input.childdob'+i).each(function() {
				$(this).val(dataChildBod[i]);
			});
		});

    });
	function getChildName() {
		var arrayChildName = new Array();
		$('input[name=childname]').each(function() {
			arrayChildName.push($(this).val());
		});
		return arrayChildName;
	}
	function getChildBod() {
		var arraychilddob =  new Array();
		$('input[name=childdob]').each(function() {
			arraychilddob.push($(this).val());
		});
		return arraychilddob;
	}
</script>
