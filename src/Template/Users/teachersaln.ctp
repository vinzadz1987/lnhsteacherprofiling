<style type="text/css">
		.underline {
		  border: 0;
		  outline: 0;
		  background: transparent;
		  border-bottom: 1px solid #c4acac;
		  width: 300px;
		  text-transform: uppercase;
		}
</style>
<div class="">
	<div class="page-title">
		<div class="title_left">
			<h3> Teacher Board </h3>
		</div>
	</div>
	<div class="clearfix"></div>
	<div class="row">
		<div class="col-md-12 col-sm-12 col-xs-12">
			<div class="x_panel">
				<div class="x_title">
					<h2>Statement of Assets, Liabilities and Net Worth <small>SALN</small></h2>
					<div class="clearfix"></div>
				</div>
				<div class="x_content">
					<?= $this->Form->create($teachersaln, [ 'class' => 'form form-horizontal form-label-left']) ?>
					<div class="col-md-12 col-sm-12 col-xs-12" style="border: 1px solid gray; padding: 5px;">
						<p>
						<span class="pull-right">
						<small>
							Revised as of January 2015<br>
							Per CSC Resolution No. 1500088<br>
							Promulgated on January 23, 2015<br>
						</small>
						</span>
						</p>
						<br><br><br>
						<center>	
							<p><h2><b>SWORN STATEMENT OF ASSETS, LIABILITIES AND NET WORTH </b></h2> </p>
							<p>As of <?= $this->Form->control('assetsdate', ['label' => false, 'type' => 'text','class' => "underline"]); ?><br>
							(Required by R.A. 6713)<br>
							<p>
								<small><i><b>Note:</b> Husband and wife who are both public officials and employees may file the required statements jointly or separately.</i></small>
							</p>
							<table  class="table table-bordered">
								<tr>
									<td><input type="radio" name="filing[]" > Joint Filing </td>
									<td><input type="radio" name="filing[]" > Separate Filing </td>
									<td><input type="radio" name="filing[]" > Not Applicable </td>
								</tr>
							</table>
							<table  class="table">
								<tr>
									<td><b>DECLARANT: </td>
									<td><?= $this->Form->control('assetsdeclarant', ['label' => false, 'type' => 'text','class' => "underline",'value' => $teachersaln['firstname'].' '. $teachersaln['lastname']]); ?>
									</td>
									<td><b>POSITION: </td>
									<td><?= $this->Form->control('occupation', ['label' => false, 'type' => 'text','class' => "underline",'value' => 'Teacher'.' '.$teachersaln['designation']]); ?> </td>
								</tr>
								<tr>
									<td></td>
									<td>(Family Name) (First Name) (M.I)</td>
									<td><b>AGENCY/OFFICE: </td>
									<td><?= $this->Form->control('businessaddress', ['label' => false, 'type' => 'text','class' => "underline"]); ?> </td>
								</tr>
								<tr>
									<td><b>ADDRESS: </td>
									<td><?= $this->Form->control('assetsaddress', ['label' => false, 'type' => 'text','class' => "underline", 'value' => $teachersaln['residence_address_brgy'].' '.$teachersaln['residence_address_city'].' '.$teachersaln['residence_address_province']]); ?></td>
									<td><b>OFFICE ADDRESS: </td>
									<td><?= $this->Form->control('businessaddress', ['label' => false, 'type' => 'text','class' => "underline"]); ?> </td>
								</tr>
								<tr>
									<td></td>
									<td><?= $this->Form->control('assetsdeclarant', ['label' => false, 'type' => 'text','class' => "underline"]); ?></td>
									<td></td>
									<td><?= $this->Form->control('assetsposition', ['label' => false, 'type' => 'text','class' => "underline"]); ?> </td>
								</tr>
								<tr>
									<td><b>SPOUSE: </td>
									<td><?= $this->Form->control('assetsspouse', ['label' => false, 'type' => 'text','class' => "underline", 'value' => $teachersaln['spousefirstname'].' '.$teachersaln['spousesurname']]); ?></td>
									<td><b>POSITION: </td>
									<td><?= $this->Form->control('occupation', ['label' => false, 'type' => 'text','class' => "underline"]); ?> </td>
								</tr>
								<tr>
									<td></td>
									<td>(Family Name) (First Name) (M.I)</td>
									<td><b>AGENCY/OFFICE: </td>
									<td><?= $this->Form->control('assetsspouseposition', ['label' => false, 'type' => 'text','class' => "underline"]); ?> </td>
								</tr>
								<tr>
									<td></td>
									<td></td>
									<td><b>OFFICE ADDRESS: </td>
									<td><?= $this->Form->control('assetsspouseaddress1', ['label' => false, 'type' => 'text','class' => "underline"]); ?>
										<?= $this->Form->control('assetsspouseaddress2', ['label' => false, 'type' => 'text','class' => "underline"]); ?>
									</td>
								</tr>
							</table>
							<hr style="border: 1px solid gray">
							<p><h5 style="text-decoration: underline;"><b>UNMARRIED CHILDREN BELOW EIGHTEEN (18) YEARS OF AGE LIVING IN DECLARANT’S HOUSEHOLD</h5></b></p>
							<table class="table" id="declarantHouseholds">
								<tr>
									<th><b> NAME </th>
									<th><b>DATE OF BIRTH</th>
									<th><b>AGE </th>
								</tr>
								<tr>
									<?php	
										$rows = 4; 
										$cols = 2;
										for($tr=0;$tr<=$rows;$tr++){
										    echo "<tr>";
										        for($td=0;$td<=$cols;$td++){ 
									?>
										<td><input type='text' class="form-control saln<?php echo $td; ?> salncolumn_<?php echo $td.'_'.$tr; ?>" data-id="saln<?php echo $td; ?>"></td>
									<?php 
										}
										    echo "</tr>";
										}
									?>
								<tr>
							</table>
							<?= $this->Form->control('salnchildrensname', ['label' => false, 'type' => 'hidden']); ?>
							<?= $this->Form->control('salnchildrensdob', ['label' => false, 'type' => 'hidden']); ?>
							<?= $this->Form->control('salnchildrensage', ['label' => false, 'type' => 'hidden']); ?>
							<hr style="border: 1px solid gray">
							<p><h5 style="text-decoration: underline;"><b>ASSETS, LIABILITIES AND NETWORTH</h5></b></p>
							<p><i>Including those of the spouse and unmarried children below eighteen (18)<br>
							years of age living in declarant’s household)</i>
							</p>
							<p class="pull-left"><B> 1. ASSETS</p><br>
							<p class="pull-left"><B> a. Real Properties*</p><br><br>
							<table class="table table-bordered" id="assetslialnet">
								<tr>
									<th bgcolor="#EAEAEA" style="color: black; padding: 2px; text-align: center;"><b> <small>DESCRIPTION</b><br>
									(e.g. lot, house and<br>
									lot, condominium<br>
									and improvements </th>
									<th bgcolor="#EAEAEA" style="color: black; padding: 2px; text-align: center;"><b><small>KIND<br>
									(e.g. residential,<br>
									commercial, industrial,<br>
									agricultural and mixed<br>
									use)</th>
									<th bgcolor="#EAEAEA" style="color: black; padding: 2px; text-align: center;"><small><b>EXACT
									LOCATION </th>
									<th colspan="2"  bgcolor="#EAEAEA" style="color: black; padding: 2px; text-align: center;"><small>
									<span class="pull-left">ASSESSED<br>
									VALUE</span>
									<span class="pull-right">CURRENT FAIR<br>
									MARKET VALUE</span><br><br><br>
									(As found in the Tax Declaration of
									Real Property)</th>
									<th colspan="2"  bgcolor="#EAEAEA" style="color: black; padding: 2px; text-align: center;">
										<small>  
										ACQUISITION<br><br>
										<span class="pull-left">YEAR</span>
										<span class="pull-right">MODE</span>
									</th>
									<th  bgcolor="#EAEAEA" style="color: black; padding: 2px; text-align: center;"><small>ACQUISITION
									COST</th>
								</tr>
								<tr>
									<?php	
										$rows = 4; 
										$cols = 7;
										for($tr=0;$tr<=$rows;$tr++){
										    echo "<tr>";
										        for($td=0;$td<=$cols;$td++){ 
									?>
										<td><input type='text' class="form-control assetslialnet<?php echo $td; ?> assetslialnetcolumn_<?php echo $td.'_'.$tr; ?>" data-id="assetslialnet<?php echo $td; ?>"></td>
									<?php 
										}
										    echo "</tr>";
										}
									?>
								<tr>
								<tr>
									<td colspan="8"><span class="pull-right">Subtotal: <?= $this->Form->control('assetsspouseaddress1', ['label' => false, 'type' => 'text','class' => "underline"]); ?></span></td>
								</tr>
							</table>
							<?php $arinput = [
								'salnassetsdescription','salnassetskind','salnassetslocation','salnassetsassvalue','salnassetsmarketvalue',
								'salnassetsyear','salnassetsmode','salnassetscost'
							]; ?>
							<?php for($i=0; $i<count($arinput); $i++) {?>
								<?= $this->Form->control($arinput[$i], ['label' => false, 'type' => 'hidden']); ?>
							<?php } ?>
							<p class="pull-left" style="margin-left: 75px"><B> a. Personal Properties*</p><br><br>
							<table class="table table-bordered">
							<tr>
								<th bgcolor="#EAEAEA" style="color: black; padding: 2px; text-align: center;"><small><b> DESCRIPTION </th>
								<th bgcolor="#EAEAEA" style="color: black; padding: 2px; text-align: center;"><small><b>YEAR ACQUIRED</th>
								<th bgcolor="#EAEAEA" style="color: black; padding: 2px; text-align: center;"><small><b>ACQUISITION <br> COST/AMOUNT </th>
							</tr>
							<tr>
								<?php	
									$rows = 4; 
									$cols = 2;
									for($tr=0;$tr<=$rows;$tr++){
									    echo "<tr>";
									        for($td=0;$td<=$cols;$td++){ 
								?>
									<td><?= $this->Form->control('assetsspouseaddress1', ['label' => false, 'type' => 'text', 'class' => 'form-control']); ?></td>
								<?php 
									}
									    echo "</tr>";
									}
								?>
							<tr>
							<tr>
								<td colspan="3"><span class="pull-right">Subtotal: <?= $this->Form->control('assetsspouseaddress1', ['label' => false, 'type' => 'text','class' => "underline"]); ?></span></td>
							</tr>
							<tr>
								<td colspan="3"><span class="pull-right">TOTAL ASSETS (a+b): <?= $this->Form->control('assetsspouseaddress1', ['label' => false, 'type' => 'text','class' => "underline"]); ?></span></td>
							</tr>
							</table>
							<span class="pull-left"><i>* Additional sheet/s may be used, if necessary. </i></span><br><br>
							<p class="pull-left"><B> 2. LIABILITIES* </p><br><br>
							<table class="table table-bordered">
							<tr>
								<th bgcolor="#EAEAEA" style="color: black; padding: 2px; text-align: center;"><small><b> NATURE </th>
								<th bgcolor="#EAEAEA" style="color: black; padding: 2px; text-align: center;"><small><b>NAME OF CREDITORS</th>
								<th bgcolor="#EAEAEA" style="color: black; padding: 2px; text-align: center;"><small><b>OUSTANDING BALANCE</th>
							</tr>
							<tr>
								<?php	
									$rows = 4; 
									$cols = 2;
									for($tr=0;$tr<=$rows;$tr++){
									    echo "<tr>";
									        for($td=0;$td<=$cols;$td++){ 
								?>
									<td><input type='text' class="form-control liabilities<?php echo $td; ?> liabilitiescolumn_<?php echo $td.'_'.$tr; ?>" data-id="liabilities<?php echo $td; ?>"></td>
								<?php 
									}
									    echo "</tr>";
									}
								?>
							<tr>
							<tr>
								<td colspan="3"><span class="pull-right">TOTAL LIABILITIES: <?= $this->Form->control('assetsspouseaddress1', ['label' => false, 'type' => 'text','class' => "underline"]); ?></span></td>
							</tr>
							<tr>
								<td colspan="3"><span class="pull-right">NET WORTH : Total Assets less Total Liabilities = <?= $this->Form->control('assetsspouseaddress1', ['label' => false, 'type' => 'text','class' => "underline"]); ?></span></td>
							</tr>
							</table>
							<span class="pull-left"><i>* Additional sheet/s may be used, if necessary. </i></span><br>
							<P><h5 style="text-decoration: underline;"><b>BUSINESS INTERESTS AND FINANCIAL CONNECTIONS</h5></b> </p>
							<p><small><i>(of Declarant /Declarant’s spouse/ Unmarried Children Below Eighteen (18) years of Age Living in Declarant’s Household) <Br>
							<input type="checkbox" /> I/We do not have any business interest or financial connection.</i></small></P>
							<table class="table table-bordered">
							<tr>
								<th bgcolor="#EAEAEA" style="color: black; padding: 2px; text-align: center;"><small><b> NAME OF ENTITY/BUSINESS
								ENTERPRISE</th>
								<th bgcolor="#EAEAEA" style="color: black; padding: 2px; text-align: center;"><small><b>BUSINESS ADDRESS</th>
								<th bgcolor="#EAEAEA" style="color: black; padding: 2px; text-align: center;"><small><b>NATURE OF BUSINESS
								INTEREST &/OR FINANCIAL<br>
								CONNECTION<br>
								</th>
								<th bgcolor="#EAEAEA" style="color: black; padding: 2px; text-align: center;"><small>DATE OF ACQUISITION OF<br>
								INTEREST OR CONNECTION<br>
								</th>
							</tr>
							<tr>
								<?php	
									$rows = 4; 
									$cols = 3;
									for($tr=0;$tr<=$rows;$tr++){
									    echo "<tr>";
									        for($td=0;$td<=$cols;$td++){ 
								?>
									<td><input type='text' class="form-control businessinterest<?php echo $td; ?> businessinterestcolumn_<?php echo $td.'_'.$tr; ?>" data-id="businessinterest<?php echo $td; ?>"></td>
								<?php 
									}
									    echo "</tr>";
									}
								?>
							<tr>
							</table>
							<P><h5 style="text-decoration: underline;"><b>RELATIVES IN THE GOVERNMENT SERVICE
							</h5></b> </p>
							<p><small><i>(Within the Fourth Degree of Consanguinity or Affinity. Include also Bilas, Balae and Inso)<Br>
							<input type="checkbox" /> I/We do not have any business interest or financial connection.</i></small></P>
							<table class="table table-bordered">
							<tr>
								<th bgcolor="#EAEAEA" style="color: black; padding: 2px; text-align: center;"><small><b> NAME OF RELATIVE</th>
								<th bgcolor="#EAEAEA" style="color: black; padding: 2px; text-align: center;"><small><b>RELATIONSHIP</th>
								<th bgcolor="#EAEAEA" style="color: black; padding: 2px; text-align: center;"><small><b>POSITION</th>
								<th bgcolor="#EAEAEA" style="color: black; padding: 2px; text-align: center;"><small>NAME OF AGENCY/OFFICE AND ADDRESS</th>
							</tr>
							<tr>
								<?php	
									$rows = 4; 
									$cols = 3;
									for($tr=0;$tr<=$rows;$tr++){
									    echo "<tr>";
									        for($td=0;$td<=$cols;$td++){ 
								?>
									<td><?= $this->Form->control('assetsspouseaddress1', ['label' => false, 'type' => 'text', 'class' => 'form-control']); ?></td>
								<?php 
									}
									    echo "</tr>";
									}
								?>
							<tr>
							</table>
							<br>
							<p style="text-align: justify;">I hereby certify that these are true and correct statements of my assets, liabilities, net worth,
							business interests and financial connections, including those of my spouse and unmarried children below
							eighteen (18) years of age living in my household, and that to the best of my knowledge, the aboveenumerated
							are names of my relatives in the government within the fourth civil degree of consanguinity or
							affinity. </p>
							<p style="text-align: justify;">I hereby authorize the Ombudsman or his/her duly authorized representative to obtain and
							secure from all appropriate government agencies, including the Bureau of Internal Revenue such
							documents that may show my assets, liabilities, net worth, business interests and financial connections,
							to include those of my spouse and unmarried children below 18 years of age living with me in my
							household covering previous years to include the year I first assumed office in government.</p>
							<span class="pull-left"><?= $this->Form->control('assetsdatesign', ['label' => 'Date: ', 'type' => 'text','class' => "underline"]); ?></span><br>
							<table class="table">
								<tr>
									<td colspan="2">_______________________</td>
									<td colspan="2">_______________________</td>
								</tr>
								<tr>
									<td colspan="2">(Signature of Declarant)</td>
									<td colspan="2">(Signature of Co-Declarant/Spouse)</td>
								</tr>
								<tr>
									<td>Government Issued ID: </td>
									<td><?= $this->Form->control('assetsgovernissueid1', ['label' => false, 'type' => 'text','class' => "underline"]); ?></td>
									<td>Government Issued ID: </td>
									<td><?= $this->Form->control('assetsgovernissueid1', ['label' => false, 'type' => 'text','class' => "underline"]); ?></td>
								</tr>
								<tr>
									<td> ID No.: </td>
									<td><?= $this->Form->control('assetsidno1', ['label' => false, 'type' => 'text','class' => "underline"]); ?></td>
									<td> ID No.: </td>
									<td><?= $this->Form->control('assetsidno2', ['label' => false, 'type' => 'text','class' => "underline"]); ?></td>
								</tr>
								<tr>
									<td>Date Issued: </td>
									<td><?= $this->Form->control('assetsdateissued1', ['label' => false, 'type' => 'text','class' => "underline"]); ?></td>
									<td>Date Issued: </td>
									<td><?= $this->Form->control('assetsdateissued2', ['label' => false, 'type' => 'text','class' => "underline"]); ?></td>
								</tr>
							</table>
							<p style="text-align: justify;"><b>SUBSCRIBED AND SWORN </b> to before me this _____ day of ____, affiant exhibiting to me the above-stated
							government issued identification card.</p>
							<?php
								$array_replace = array('[',']','"');
	                			$splitpapers = explode(',', str_replace($array_replace, "", $teachersaln['papers']));
								$papers = '';
								foreach ($splitpapers as $value) {
									if(in_array('SALN', $splitpapers)) {
										$saln = '';
									} else {
										$saln = 'SALN';
									}
									$papers .= $value.',';
								}
								$papers = json_encode($papers.$saln);
								if(sizeof($splitpapers) == 3 ) {
									$status = 1;
								} else if(sizeof($splitpapers) == 2) {
									$status = 1;
								} else {
									$status = 0;
								}
							?>
							<?= $this->Form->control('status', ['label' => false, 'type' => 'hidden','value' => $status]); ?>
							<?= $this->Form->control('papers', ['label' => false, 'type' => 'hidden','value' => $papers]); ?>
							<p><?= $this->Form->control('assetsdateissued2', ['label' => false, 'type' => 'text','class' => "underline pull-right"]); ?><br><br><span class="pull-right" style="margin-right: 61px;">(Person Administering Oath)</span></p>
							<?= $this->Form->button(__('Save'),['class'=>'btn btn-primary pull-left','id' => 'saveUser']); ?>
							<button type="button" class="btn btn-success pull-left" onclick="window.print()"><i class="fa fa-print"></i> Print</button>
							<?= $this->Form->end() ?>
						</center>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<?= $this->Html->script(['teachersaln']);?>
