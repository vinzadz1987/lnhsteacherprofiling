<div style="height: 50px"></div>
<div class="modal-header">
	<?php 
      echo $this->Html->link(
        '<i class="fa fa-arrow-left" aria-hidden="true"></i> Back To List',
        [
          'controller'=>'users',
          'action'=>'adminmasterlist'
        ],
        ['escape' => false, 'class' => 'btn btn-success pull-right' ]
      );
    ?>
	</button>
	<h4 class="modal-title" id="myModalLabel">Add <?= $type; ?></h4>
</div>
<div class="modal-body">
	<div class="card card-register mx-auto mt-5">
		<div class="card-body">
			<?= $this->Form->create($adminmasterlistadd, [ 'class' => 'form' ]) ?>

			<div class="form-group">
				<div class="form-row">
					<div class="col-md-12">
						<?= $this->Form->control('userid', [ 'placeholder' => 'Enter Faculty ID', 'class' => 'form-control', 'label' => 'Faculty ID' ]); ?>
					</div>
				</div>
			</div>


			<div class="form-group">
				<div class="form-row">
					<div class="col-md-12">
						<?= $this->Form->control('firstname', [ 'class' => 'form-control', 'placeholder' => 'Enter Firstname' ]) ?>
					</div>
				</div>
			</div>

			<div class="form-group">
				<div class="form-row">
					<div class="col-md-12">
						<?= $this->Form->control('middlename', [ 'class' => 'form-control', 'placeholder' => 'Enter Middlename' ]) ?>
					</div>
				</div>
			</div>

			<div class="form-group">
				<div class="form-row">
					<div class="col-md-12">
						<?= $this->Form->control('lastname', [ 'class' => 'form-control', 'placeholder' => 'Enter Lastname' ]) ?>
					</div>
				</div>
			</div>

			<div class="form-group">
				<div class="form-row">
					<div class="col-md-12">
						<?= $this->Form->control('contact_no', [ 'class' => 'form-control', 'placeholder' => 'Enter Contact No', 'label' => 'Contact No' ]) ?>
					</div>
				</div>
			</div>

			<div class="form-group">
				<div class="form-row">
					<div class="col-md-12">
						<?= $this->Form->control('email', [ 'class' => 'form-control', 'placeholder' => 'Enter email' ]) ?>
					</div>
				</div>
			</div>

			<div class="form-group">
				<div class="form-row">
					<div class="col-md-6">
						<?= $this->Form->input('designation', [
							'type' => 'select',
							'class' => 'form-control',
							'options' => [ 
								'' => 'Select Designation',
								'Regular' => 'Regular',
								'Contractual' => 'Contractual'
							]
						]);
						?>
					</div>
					<div class="col-md-6">
						<?= $this->Form->input('department', [
							'type' => 'select',
							'class' => 'form-control',
							'options' => [ 
								'' => 'Select Department',
								'Junior High' => 'Junior High',
								'Senior High (GAS)' => 'Senior High (GAS) ',
								'Senior High (ABM)' => 'Senior High (ABM)',
								'Senior High (HUMSS)' => 'Senior High (HUMSS)',
								'Senior High (STEM)' => 'Senior High (STEM)',
								'Senior High (TLE)' => 'Senior High (TLE)'
							]
						]);
						?>
					</div>
				</div>
			</div>

			<div class="form-group">
				<div class="form-row">
					<div class="col-md-6">
						<?= $this->Form->control('password', [ 'class' => 'form-control', 'placeholder' => 'Enter password' ]) ?>
					</div>
					<div class="col-md-6">
						<?= $this->Form->control('password2',[ 'type' => 'password', 'label' => 'Confirm Password','class' => 'form-control', 'placeholder' => 'Enter confirm password', 'disabled' => 'disabled' ] ) ?>
						<span id="divCheckPasswordMatch"></span>
					</div>
					<div class="col-md-6">
						<?= $this->Form->control('usertype',[ 'type' => 'hidden', 'value' => $usertype ] ) ?>
						<?= $this->Form->control('created',[ 'type' => 'hidden', 'value' => date("Y-m-d H:i:s") ] ) ?>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="modal-footer">
	<?php 
      echo $this->Html->link(
        '<i class="fa fa-close" aria-hidden="true"></i> Close',
        [
          'controller'=>'users',
          'action'=>'adminmasterlist'
        ],
        ['escape' => false, 'class' => 'btn btn-danger' ]
      );
    ?>
	<?= $this->Form->button(__('Save'),['class'=>'btn btn-primary','id' => 'saveUser']); ?>
	<?= $this->Form->end() ?>
</div>
<script type="text/javascript">
	$(function() {
		$("#contact-no").mask("999999999999");
		$('#password').on('keyup',function() {
			$('#password2').prop('disabled',false); 
		});
		$("#password,#password2").keyup(checkPasswordMatch);
	});
	function checkPasswordMatch() {
		var password = $("#password").val();
		var confirmPassword = $("#password2").val();

		if (password != confirmPassword)
		$("#divCheckPasswordMatch").html("Passwords do not match!");
		else
		$("#divCheckPasswordMatch").html("Passwords match.");
	}
</script>
