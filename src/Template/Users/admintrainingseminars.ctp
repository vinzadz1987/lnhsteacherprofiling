<div class="">
	<div class="page-title">
		<div class="title_left">
			<h3> <i class="fa fa fa-tachometer blue"></i> Admin Board </h3>
		</div>
	</div>
	<div class="clearfix"></div>
	<div class="row">
		<div class="col-md-12 col-sm-12 col-xs-12">
			<div class="x_panel">
				<div class="x_title">
					<h2>Trainings and Seminars</h2>
					<div class="pull-right">
						<div class="input-group">
							<?php
								echo $this->Html->link(
									'<i class="fa fa-plus" aria-hidden="true"></i> Add ',
									[
										'controller'=>'users',
										'action'=>'admintrainingseminarsadd'
									],
									['escape' => false, 'class' => 'btn btn-primary' ]
								);
								?>
						</div>
					</div>
					<div class="clearfix"></div>
				</div>
				<div class="x_content">
					<table id="trainingTable" class="table table-striped table-bordered">
						<thead>
							<tr>
								<th>Title</th>
								<th>Description</th>
								<th>Venue</th>
								<!-- <th>Joins</th> -->
								<th>Type</th>
								<th>Date</th>
								<th style="width: 145px;">Action</th>
							</tr>
						</thead>
						<tbody>
							<?php foreach($training_seminar as $ts): ?>
							<tr>
								<td><?=$ts->name ?></td>
								<td><?=$ts->description; ?> </td>
								<td><?=$ts->venue; ?> </td>
								<!-- <td><?=h($ts->joiner)?></td> -->
								<td>
									<?php if($ts->type == 1) { ?>
										<span class="label label-info">Seminars</span>
									<?php } else if($ts->type == 2) { ?>
										<span class="label label-primary">Training</span>
									<?php } ?>
								</td>
								<td><?=h($ts->date)?></td>
								<td>
									<?php 
										echo $this->Html->link(
										'<i class="fa fa-pencil" aria-hidden="true"></i>',
										[
											'controller'=>'users',
											'action'=>'admintrainingseminarsedit',
											h($ts->id)
										],
										['escape' => false, 'class' => 'btn btn-success btn-xs ' ]
										);
									?>
									<?php 
										echo $this->Html->link(
										'<i class="fa fa-times" aria-hidden="true"></i>',
										[
											'controller'=>'users',
											'action'=>'admintrainingseminarsdelete',
											h($ts->id)
										],
										['confirm' => 'Are you sure you want to delete this?','escape' => false, 'class' => 'btn btn-danger btn-xs ' ]
										);
									?>
								</td>
							</tr>
							<?php endforeach; ?> 
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
	$('#trainingTable').dataTable( {
		'language': {
			'searchPlaceholder': 'Title, Description, Venue ....'
		}
	});
</script>