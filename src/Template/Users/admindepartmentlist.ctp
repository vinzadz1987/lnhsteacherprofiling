<div class="">
	<div class="page-title">
		<div class="title_left">
			<h3> <i class="fa fa fa-tachometer blue"></i> Admin Board </h3>
		</div>
		<div class="title_right">
			<div class="pull-right">
				<div class="input-group">
					<?php 
						echo $this->Html->link(
						'<i class="fa fa-arrow-left" aria-hidden="true"></i> Back To Department',
						[
							'controller'=>'users',
							'action'=>'admindepartment'
						],
						['escape' => false, 'class' => 'btn btn-success pull-right' ]
						);
					?>
				</div>
			</div>
		</div>
	</div>
<div class="clearfix"></div>
<div class="row">
  <div class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">
      <div class="x_title">
        <h2><?= $department ?></h2>
        <div class="clearfix"></div>
      </div>
       <div class="x_content">
        <table id="datatable-buttons" class="table table-striped table-bordered">
          <thead>
            <tr>
              <th>ID Number</th>
              <th>Name</th>
              <th>Email</th>
              <th>Contact No.</th>
              <th>Department</th>
              <th>Designation</th>
              <th>User Type</th>
              <th>Created</th>
              <th style="width: 90px;">Action</th>
            </tr>
          </thead>
          <tbody>
             <?php foreach($users as $user): ?>
              <tr>
                <td><?=h($user->userid)?></td>
                <td><?=h($user->lastname)?> <?=h($user->firstname)?></td>
                <td><?=h($user->email)?></td>
                <td><?=h($user->contact_no)?></td>
                <td><?=h($user->department)?></td>
                <td><?=h($user->designation)?></td>
                <td>
                  <?php echo ($user->usertype == 1)? 'Teacher' : 'Admin' ?>
                </td>
                <td><?=h($user->created)?></td>
                <td>
                  <?php 
                      echo $this->Html->link(
                        '<i class="fa fa-pencil" aria-hidden="true"></i>',
                        [
                          'controller'=>'users',
                          'action'=>'adminmasterlistedit',
                          h($user->id),
                          h($user->usertype)
                        ],
                        ['escape' => false, 'class' => 'btn btn-success btn-xs ' ]
                      );
                    ?>
                    <?php 
                      echo $this->Html->link(
                        '<i class="fa fa-eye" aria-hidden="true"></i>',
                        [
                          'controller'=>'users',
                          'action'=>'viewUsers',
                          h($user->id),
                          h($user->usertype)
                        ],
                        [ 'escape' => false, 'class' => 'btn btn-info btn-xs ' ]
                      );
                    ?>
                </td>
              </tr>
              <?php endforeach; ?> 
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>
</div>
