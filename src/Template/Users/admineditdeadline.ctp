<div style="height: 50px"></div>
<div class="modal-header">
	<?php 
		echo $this->Html->link(
		'<i class="fa fa-arrow-left" aria-hidden="true"></i> Back To List',
		[
			'controller'=>'users',
			'action'=>'adminsetdeadline'
		],
		['escape' => false, 'class' => 'btn btn-success pull-right' ]
		);
	?>
	<h4 class="modal-title" id="myModalLabel">Set Deadline</h4>
</div>
<div class="modal-body">
	<div class="card card-register mx-auto mt-5">
		<div class="card-body">
			<?= $this->Form->create($users, [ 'class' => 'form' ]) ?>
			<div class="x_panel">
				<div class="x_title">
					<h2><i><?= $users['userid'] ?></i> <?= ucfirst($users['firstname']) ?> <?= ucfirst($users['lastname']) ?>  <small><?= $users['department'] ?></small></h2>
					<ul class="nav navbar-right panel_toolbox"></ul>
					<div class="clearfix"></div>
				</div>
				<div class="x_content">
					<div class="form-group">
						<div class="form-row">
							<div class="col-md-4">
								<?= $this->Form->control('submitdate', [ 'class' => 'form-control', 'type' => 'text' ]) ?>
							</div>
						</div>
						<div class="form-row">
							<div class="col-md-4">
								<?= $this->Form->control('deadline', [ 'class' => 'form-control', 'type' => 'text' ]) ?>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="modal-footer">
	<?php 
      echo $this->Html->link(
        '<i class="fa fa-close" aria-hidden="true"></i> Close',
        [
          'controller'=>'users',
          'action'=>'adminsetdeadline'
        ],
        ['escape' => false, 'class' => 'btn btn-danger' ]
      );
    ?>
	<?= $this->Form->button(__('Save'),['class'=>'btn btn-primary','id' => 'saveUser']); ?>
	<?= $this->Form->end() ?>
</div>
<script type="text/javascript">
	$(function () {
		$("#deadline,#submitdate").datepicker({
			dateFormat: 'yy-mm-dd',
			autoclose: true,
			todayHighlight: false
		});
	});
</script>
