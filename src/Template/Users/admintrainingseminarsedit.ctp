<div class="modal-body">
	<div class="card card-register mx-auto mt-5">
		<div class="card-body">
			<?= $this->Form->create($adminedittrainingseminars, [ 'class' => 'form' ]) ?>
			<div class="x_panel">
				<div class="x_title">
					<h2>Add Traning and Seminars</h2>
					<?php 
						echo $this->Html->link(
						'<i class="fa fa-arrow-left" aria-hidden="true"></i> Back To List',
						[
							'controller'=>'users',
							'action'=>'admintrainingseminars'
						],
						['escape' => false, 'class' => 'btn btn-success pull-right' ]
						);
					?>
					<ul class="nav navbar-right panel_toolbox"></ul>
					<div class="clearfix"></div>
				</div>
				<div class="x_content">
					<div class="form-group">
						<div class="form-row">
							<div class="col-md-8">
								<?= $this->Form->control('name', [ 'class' => 'form-control', 'type' => 'text', 'required' => true ]) ?>
							</div>
						</div>
					</div>
					<div class="form-group">
						<div class="form-row">
							<div class="col-md-6">
								<?= $this->Form->input('type', [
									'type' => 'select',
									'class' => 'form-control',
									'required' => true,
									'options' => [ 
										'' => 'Select Type',
										1 => 'Seminar',
										2 => 'Traning'
									]
								]);
								?>
							</div>
						</div>
					</div>
					<div class="form-group">
						<div class="form-row">
							<div class="col-md-8">
								<?= $this->Form->control('description', [ 'class' => 'form-control', 'type' => 'textarea', 'required' => true ]) ?>
							</div>
						</div>
					</div>
					<div class="form-group">
						<div class="form-row">
							<div class="col-md-8">
								<?= $this->Form->control('venue', [ 'class' => 'form-control', 'required' => true ]) ?>
							</div>
						</div>
					</div>
					<div class="form-group">
						<div class="form-row">
							<div class="col-md-8">
								<?= $this->Form->control('joiner', [ 'class' => 'form-control', 'required' => true, 'label' => 'Attendees' ]) ?>
							</div>
						</div>
					</div>
					<div class="form-group">
						<div class="form-row">
							<div class="col-md-8">
								<?= $this->Form->control('date', [ 'class' => 'form-control', 'type' => 'text', 'required' => true ]) ?>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="modal-footer">
	<?php 
      echo $this->Html->link(
        '<i class="fa fa-close" aria-hidden="true"></i> Close',
        [
          'controller'=>'users',
          'action'=>'admintrainingseminars'
        ],
        ['escape' => false, 'class' => 'btn btn-danger' ]
      );
    ?>
	<?= $this->Form->button(__('Save'),['class'=>'btn btn-primary','id' => 'saveUser']); ?>
	<?= $this->Form->end() ?>
</div>
<script type="text/javascript">
	$(function () {
		$("#date").datepicker({
			dateFormat: 'yy-mm-dd',
			autoclose: true,
			todayHighlight: false
		});
	});
</script>
