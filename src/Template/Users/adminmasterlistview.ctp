<div style="height: 50px"></div>
<div class="modal-header">
	<?php 
      echo $this->Html->link(
        '<i class="fa fa-arrow-left" aria-hidden="true"></i> Back To List',
        [
          'controller'=>'users',
          'action'=>'adminmasterlist'
        ],
        ['escape' => false, 'class' => 'btn btn-info pull-right' ]
      );
    ?>
    <?php 
      echo $this->Html->link(
        '<i class="fa fa-pencil" aria-hidden="true"></i> Edit',
        [
          'controller'=>'users',
          'action'=>'adminmasterlistedit',
          $user->id,
          $user->usertype
        ],
        ['escape' => false, 'class' => 'btn btn-success pull-right' ]
      );
     ?>
	<h4 class="modal-title" id="myModalLabel">View <?= $type; ?></h4>
</div>
<div class="modal-body">
	<div class="card card-register mx-auto mt-5">
		<div class="card-body">
			<?= $this->Form->create($user, [ 'class' => 'form' ]) ?>
			<?php if(!empty($user['photo'])) { ?>
				<img src="<?php echo $this->request->webroot; ?>img/<?php echo $user['photo']; ?>" style="    margin-left: 40%; height: 155px;width: 163px;" alt="..." class="img-circle profile_img">
			<?php } else { ?>
				<img src="<?php echo $this->request->webroot; ?>img/default.png" style="    margin-left: 40%; height: 155px;width: 163px;" alt="..." class="img-circle profile_img">
			<?php } ?>
			<div class="form-group">
				<div class="form-row">
					<div class="col-md-12">
						<?= $this->Form->control('userid', [ 'placeholder' => 'Enter Faculty ID', 'class' => 'form-control', 'label' => 'Faculty ID', 'disabled' => true ]); ?>
					</div>
				</div>
			</div>


			<div class="form-group">
				<div class="form-row">
					<div class="col-md-12">
						<?= $this->Form->control('firstname', [ 'class' => 'form-control', 'placeholder' => 'Enter Firstname', 'disabled' => true ]) ?>
					</div>
				</div>
			</div>

			<div class="form-group">
				<div class="form-row">
					<div class="col-md-12">
						<?= $this->Form->control('middlename', [ 'class' => 'form-control', 'placeholder' => 'Enter Middlename', 'disabled' => true ]) ?>
					</div>
				</div>
			</div>

			<div class="form-group">
				<div class="form-row">
					<div class="col-md-12">
						<?= $this->Form->control('lastname', [ 'class' => 'form-control', 'placeholder' => 'Enter Lastname', 'disabled' => true ]) ?>
					</div>
				</div>
			</div>

			<div class="form-group">
				<div class="form-row">
					<div class="col-md-12">
						<?= $this->Form->control('contact_no', [ 'class' => 'form-control', 'placeholder' => 'Enter Contact No', 'label' => 'Contact No', 'disabled' => true ]) ?>
					</div>
				</div>
			</div>

			<div class="form-group">
				<div class="form-row">
					<div class="col-md-12">
						<?= $this->Form->control('email', [ 'class' => 'form-control', 'placeholder' => 'Enter email', 'disabled' => true ]) ?>
					</div>
				</div>
			</div>

			<div class="form-group">
				<div class="form-row">
					<div class="col-md-6">
						<?= $this->Form->input('designation', [
							'type' => 'select',
							'class' => 'form-control',
							'disabled' => true,
							'options' => [ 
								'' => 'Select Designation',
								'Regular' => 'Regular',
								'Contractual' => 'Contractual'
							]
						]);
						?>
					</div>
					<div class="col-md-6">
						<?= $this->Form->input('department', [
							'type' => 'select',
							'class' => 'form-control',
							'disabled' => true,
							'options' => [ 
								'' => 'Select Department',
								'Junior High' => 'Junior High',
								'Senior High (GAS)' => 'Senior High (GAS) ',
								'Senior High (ABM)' => 'Senior High (ABM)',
								'Senior High (HUMSS)' => 'Senior High (HUMSS)',
								'Senior High (STEM)' => 'Senior High (STEM)',
								'Senior High (TLE)' => 'Senior High (TLE)'
							]
						]);
						?>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="modal-footer">
	<?php 
      echo $this->Html->link(
        '<i class="fa fa-close" aria-hidden="true"></i> Close',
        [
          'controller'=>'users',
          'action'=>'adminmasterlist'
        ],
        ['escape' => false, 'class' => 'btn btn-danger' ]
      );
    ?>
	<?= $this->Form->end() ?>
</div>
