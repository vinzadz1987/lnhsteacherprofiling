<style type="text/css">
.notice {
    padding: 15px;
    background-color: #fafafa;
    border-left: 6px solid #7f7f84;
    margin-bottom: 10px;
    -webkit-box-shadow: 0 5px 8px -6px rgba(0,0,0,.2);
       -moz-box-shadow: 0 5px 8px -6px rgba(0,0,0,.2);
            box-shadow: 0 5px 8px -6px rgba(0,0,0,.2);
}
.notice-sm {
    padding: 10px;
    font-size: 80%;
}
.notice-lg {
    padding: 35px;
    font-size: large;
}
.notice-success {
    border-color: #80D651;
}
.notice-success>strong {
    color: #80D651;
}
.notice-info {
    border-color: #45ABCD;
}
.notice-info>strong {
    color: #45ABCD;
}
.notice-warning {
    border-color: #FEAF20;
}
.notice-warning>strong {
    color: #FEAF20;
}
.notice-danger {
    border-color: #d73814;
}
.notice-danger>strong {
    color: #d73814;
}
</style>
<div class="page-title">
    <div class="title_left">
      <h3> Teacher Board </h3>
    </div>
  </div>
<div class="clearfix"></div>

<div class="container">
	<?php foreach ($logs as $l) { ?>
		<?php
			$session = $this->request->session();
			$user_data = $session->read('Auth.User');
		?>
		<?php $notice = array('notice-success','notice-danger','notice-info','notice-warning'); ?>
		<div class="notice <?php echo $notice[array_rand($notice)]; ?>">
			<strong><?php echo ($l['userid'] == $user_data['userid'])? 'You': $l['userid']; ?></strong> <?php echo $l['message'] ?>
		</div>
	<?php } ?>
</div>