<div class="">
	<div class="page-title">
		<div class="title_left">
			<h3> <i class="fa fa fa-tachometer blue"></i> Admin Board </h3>
		</div>
	</div>
	<div class="clearfix"></div>
	<div class="row">
		<div class="col-md-12 col-sm-12 col-xs-12">
			<div class="x_panel">
				<div class="x_title">
					<h2>Set Deadline</h2>
					<div class="clearfix"></div>
				</div>
				<div class="x_content">
					<table id="deadlineTable" class="table table-striped table-bordered">
						<thead>
							<tr>
								<th>ID Number</th>
								<th>Name</th>
								<th>Department</th>
								<th>Date submitted</th>
								<th>Deadline</th>
								<th style="width: 10px;">Action</th>
							</tr>
						</thead>
						<tbody>
							<?php foreach($users as $user): ?>
							<tr>
								<td><?=h($user->userid)?></td>
								<td><?=h($user->lastname)?> <?=h($user->firstname)?></td>
								<td><?=h($user->department)?></td>
								<td><?=h($user->submitdate)?></td>
								<td><?=h($user->deadline)?></td>
								<td>
									<?php 
										echo $this->Html->link(
										'<i class="fa fa-calendar" aria-hidden="true"></i>',
										[
											'controller'=>'users',
											'action'=>'admineditdeadline',
											h($user->id)
										],
										['escape' => false, 'class' => 'btn btn-success btn-xs ' ]
										);
									?>
								</td>
							</tr>
							<?php endforeach; ?> 
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
	$(function () {
		$('#deadlineTable').dataTable( {
			'language': {
				'searchPlaceholder': 'ID Number, Name, Department ....'
			}
		});
	});
</script>
