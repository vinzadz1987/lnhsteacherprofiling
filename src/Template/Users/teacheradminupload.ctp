<div class="modal-body">
	<div class="col-md-12 col-sm-3 col-xs-12">
		<section class="x_panel ">
			<div class="x_title">
				<h2>UPLOAD PHOTO </h2>
				<div class="clearfix"></div>
			</div>
			<div class="panel-body">

				<form action='teacheradminuploadimages' method='post' enctype="multipart/form-data">
					<center>
						<table>
							<tr>
								<td></td>
								<td>
									<?php foreach($users as $usr) { ?>
										<img class="img-circle" style="height: 300px; margin-bottom: 10px;" src="<?php echo $this->request->webroot; ?>img/<?php echo $usr['photo'];; ?>" alt="">
									<?php } ?>
									<input name="ufile" type="file" id="ufile" class="form-control btn-default"/></td>
								</td>
							</tr>
						</table>
						<br>
						<p><input type='submit' name='submit' class="btn btn-primary" value='upload photo'></p>
					</center>
				</form>
			</div>
		</section>
	</div>
</div>
