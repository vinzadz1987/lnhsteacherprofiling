<div class="">
<div class="page-title">
  <div class="title_left">
    <h3> <i class="fa fa fa-tachometer green"></i> Admin Board </h3>
  </div>
</div>

<div class="clearfix"></div>

<div class="row">
  <div class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">
      <div class="x_title">
        <h2>Department</h2>
        <div class="clearfix"></div>
      </div>
       <div class="x_content">

        <?php $arrayDept = array(
            1=>'Junior High',
            2=>'Senior High (GAS) ',
            3=>'Senior High (ABM)',
            4=>'Senior High (HUMSS)',
            5=>'Senior High (STEM)',
            6=>'Senior High (TLE)'
         ); ?>
		<?php
			$dept_count = array();
			foreach ($department as $key => $value) {
				$dept_count[] = $value->department.'-'.$value->count;
			} 
		?>
		<?php foreach ($dept_count as $dept) { ?>
			<div class="col-md-4">
				<div class="x_panel">
					<div class="x_title">
						<h2><?= explode('-', $dept)[0]; ?><small>No of Teachers: <?= explode('-', $dept)[1]; ?> </small></h2>
						<div class="clearfix"></div>
					</div>
					<div class="x_content">
						<?php 
							$departmentParams = [' ','(',')']; 
							$departmentReplace = ['_','__','___'];
							echo $this->Html->link(
								'<i class="fa fa-eye" aria-hidden="true"></i> View List',
								[
									'controller'=>'users',
									'action'=>'admindepartmentlist',
									explode('-', $dept)[0]
								],
								['escape' => false, 'class' => 'btn btn-primary' ]
							);
						?>
					</div>
				</div>
			</div>
		<?php } ?>

      </div>
    </div>
  </div>
</div>
</div>
<script type="text/javascript">
	
</script>
