<div class="">
<div class="page-title">
  <div class="title_left">
    <h3> <i class="fa fa fa-tachometer blue"></i> Admin Board </h3>
  </div>
</div>

<div class="clearfix"></div>

<div class="row">
  <div class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">
      <div class="x_title">
        <h2>Masterlist</h2>
         <div class="pull-right">
			<div class="input-group">
				<?php
					echo $this->Html->link(
						'<i class="fa fa-plus" aria-hidden="true"></i> Add Teacher',
						[
							'controller'=>'users',
							'action'=>'adminmasterlistadd',
							'teacher'
						],
						['escape' => false, 'class' => 'btn btn-primary' ]
					);
					?>
					<?php
					echo $this->Html->link(
						'<i class="fa fa-plus" aria-hidden="true"></i> Add Admin User',
						[
							'controller'=>'users',
							'action'=>'adminmasterlistadd',
							'admin'
						],
						['escape' => false, 'class' => 'btn btn-success' ]
					);
				?>
			</div>
    	</div>
        <div class="clearfix"></div>
      </div>
       <div class="x_content">
        <table id="masterlistTable" class="table table-striped table-bordered">
          <thead>
            <tr>
              <th>ID Number</th>
              <th>Name</th>
              <th>Email</th>
              <th>Contact No.</th>
              <th>Department</th>
              <th>Designation</th>
              <th>Papers</th>
              <th>User Type</th>
              <th>Created</th>
              <th style="width: 101px;">Action</th>
            </tr>
          </thead>
          <tbody>
             <?php foreach($users as $user): ?>
              <tr>
                <td><?=h($user->userid)?></td>
                <td><?=h($user->lastname)?> <?=h($user->firstname)?></td>
                <td><?=h($user->email)?></td>
                <td><?=h($user->contact_no)?></td>
                <td><?=h($user->department)?></td>
                <td><?=h($user->designation)?></td>
                <td>
                	<center>
	                	<?php 
	                    	$array_replace = array('[',']','"');
	                		$papers = explode(',', str_replace($array_replace, "", $user->papers));
	                		$pPapers = '';
	                		foreach ($papers as $value) {
	                			if($value == "PDS") {
	                				echo "<label class='label label-default'><a href='teacherfacultypds/".h($user->userid)."' style='color: white'>".$value."</a></label><br>";
	                			} else if($value == "SERVICE RECORDS") {
	                				echo "<label class='label label-primary'><a href='teacherservicerecords/".h($user->userid)."' style='color: white'>".$value."</a></label><br>";
	                			} else if($value == "SALN"){
	                				echo "<label class='label label-info'><a href='teachersaln/".h($user->userid)."' style='color: white'>".$value."</a></label><br>";
	                			}
	                		}
	                		// echo (empty($papers[1])? '':"<label class='label label-default'><a href='' style='color: white'>".$papers[1].'</a></label>').'<br>';
	                		// echo (empty($papers[0])? '':"<label class='label label-primary'><a href='' style='color: white'>".$papers[0].'</a></label>').'&nbsp;&nbsp;';
	                		// echo (empty($papers[2])? '':"<label class='label label-info'><a href='' style='color: white'>".$papers[2].'</a></label>');
	                	?>
	                </center>
                </td>
                <td>
                  <?php echo ($user->usertype == 1)? '<span class="label label-info">Teacher</span>' : '<span class="label label-primary">Admin<span>'; ?>
                </td>
                <td><?=h($user->created)?></td>
                <td>
                  <?php
                      echo $this->Html->link(
                        '<i class="fa fa-pencil" aria-hidden="true"></i>',
                        [
                          'controller'=>'users',
                          'action'=>'adminmasterlistedit',
                          h($user->id),
                          h($user->usertype)
                        ],
                        ['escape' => false, 'class' => 'btn btn-success btn-xs ' ]
                      );
                    ?>
                    <?php
                      echo $this->Html->link(
                        '<i class="fa fa-eye" aria-hidden="true"></i>',
                        [
                          'controller'=>'users',
                          'action'=>'adminmasterlistview',
                          h($user->id),
                          h($user->usertype)
                        ],
                        [ 'escape' => false, 'class' => 'btn btn-info btn-xs ' ]
                      );
                    ?>
                    <i class="fa fa-check btn btn-primary btn-xs" aria-hidden="true" data-toggle="modal" data-target="#myModal" onclick="getPapers('<?php echo h($user->id) ?>','<?php echo h($user->papers) ?>')"></i>
					<?php
						echo $this->Html->link(
								'<i class="fa fa-trash" aria-hidden="true"></i>',
							[
								'controller'=>'users',
								'action'=>'adminmasterlistdelete',
								h($user->id)
							],
							['confirm' => 'Are you sure you want to delete this user?', 'escape' => false, 'class' => 'btn btn-danger btn-xs ' ]
						);
					?>
                </td>
              </tr>
              <?php endforeach; ?>
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>
</div>
<!-- Modal -->
<div class="modal fade" id="myModal" role="dialog">
	<div class="modal-dialog" style="width: 330px;">

		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Pertinent Papers</h4>
			</div>
			<div class="modal-body">
				<p>
					<div id="checkboxes">
						<input type="hidden" id="userid">
						<input class="papers" type="checkbox" name="papers[]" id="pds" value="PDS"> PDS<br>
						<input class="papers" type="checkbox" name="papers[]" id="servicerrecords" value="SERVICE RECORDS"> SERVICE RECORDS<br>
						<input class="papers" type="checkbox" name="papers[]" id="saln" value="SALN"> SALN
					</div>
          
				</p>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-primary" id="submitCheck">Check</button>
				<button type="button" class="btn btn-default" data-dismiss="modal" onclick="location.reload()">Close</button>
			</div>
		</div>

	</div>
</div>
<script type="text/javascript">
	$(function() {
		var selected = '';
		$("#submitCheck").on('click', function() {
			var selected = new Array();
			var ctr = 0;
			$('#checkboxes input:checked').each(function() {
				selected.push($(this).val());
			});
			if(selected.length == 3) {
				var status = 1;
			} else {
				status = 0;
			}
			$.ajax({
				type: 'POST',
				url:'adminsavepapers',
				data: { papers: JSON.stringify(selected), status: status, id:  $("#userid").val() },
				success : function(data) {
					alert("Pertinent papers successfully save.");
					if(selected.length == 3) {
						window.location.href = "adminclearedfaculty";
					} else {
						location.reload();
					}				
				}
			});
					
		});
	});
	function getPapers(id,check) {
		$("#userid").val(id);
		var checkit = (check.split(','));
		var checkme = '';
		for(i=0;i<=checkit.length;i++) {
			if(typeof checkit[i] == 'undefined') {
				return false;
			} else {

				checkme = ( checkit[i].replace(/[^a-z0-9\s]/gi, '') );
				
			}
			if(checkme == "PDS") {
				$('#pds').prop('checked', true);
			} else if (checkme == "SERVICE RECORDS") {
				$('#servicerrecords').prop('checked', true);
			} else if(checkme == "SALN") {
				$("#saln").prop('checked', true);
			} 
		}
		// console.log(checkme);

	}
	$('#masterlistTable').dataTable( {
		'language': {
			'searchPlaceholder': 'ID Number, Name, Department ....'
		}
	});
</script>
