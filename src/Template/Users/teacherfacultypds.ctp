<div class="">
	<div class="page-title">
		<div class="title_left">
			<h3> Teacher Profile </h3>
		</div>
	</div>
	<div class="clearfix"></div>
	<div class="row">
		<div class="col-md-12 col-sm-12 col-xs-12">
			<div class="x_panel">
				<div class="x_title" style="padding: 16px 8px 1px; border-bottom: 0px">
					<h2>PERSONAL DATA SHEETS (PDS) <small>CS FORM NO. 212 <i>Revised 2017</i></small></h2>
					<div class="clearfix"></div>
				</div>
				<div class="x_content">
					<div class="" role="tabpanel" data-example-id="togglable-tabs">
						<ul id="myTab" class="nav nav-tabs bar_tabs" role="tablist">
							<li role="presentation" class="active"><a href="#tab_content1" id="c1-tab" role="tab" data-toggle="tab" aria-expanded="true">C1</a>
							</li>
							<li role="presentation" class=""><a href="#tab_content2" role="tab" id="c2-tab" data-toggle="tab" aria-expanded="false">C2</a>
							</li>
							<li role="presentation" class=""><a href="#tab_content3" role="tab" id="c3-tab2" data-toggle="tab" aria-expanded="false">C3</a>
							</li>
							<li role="presentation" class=""><a href="#tab_content4" role="tab" id="c4-tab2" data-toggle="tab" aria-expanded="false">C4</a>
							</li>
						</ul>
						<div id="myTabContent" class="tab-content">
							<div role="tabpanel" class="tab-pane fade active in" id="tab_content1" aria-labelledby="c1-tab">
								<?php echo $this->element('teacher/entirec1pds') ?>
							</div>
							<div role="tabpanel" class="tab-pane fade" id="tab_content2" aria-labelledby="c2-tab">
								<?php echo $this->element('teacher/entirec2pds') ?>
							</div>
							<div role="tabpanel" class="tab-pane fade" id="tab_content3" aria-labelledby="c3-tab">
								<?php echo $this->element('teacher/entirec3pds') ?>
							</div>
							<div role="tabpanel" class="tab-pane fade" id="tab_content4" aria-labelledby="c4-tab">
								<?php echo $this->element('teacher/entirec4pds') ?>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<?= $this->Html->script(['teacherpds']);?>
