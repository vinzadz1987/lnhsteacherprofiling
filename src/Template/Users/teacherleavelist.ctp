<div class="">
	<div class="page-title">
		<div class="title_left">
			<h3> Teacher Board </h3>
		</div>
	</div>
	<div class="clearfix"></div>
	<div class="row">
		<div class="col-md-12 col-sm-12 col-xs-12">
			<div class="x_panel">
				<div class="x_title">
					<h2>Leave List</h2>
					<a href="teacherleaveapplication"><button class="btn btn-primary pull-right"><i class="fa fa-plus"></i> Apply Leave </button></a>
					<div class="clearfix"></div>
				</div>
				<?php $suser = $this->request->session()->read('Auth.User'); ?>
				<div class="x_content">
					<table id="datatable-buttons" class="table table-striped table-bordered">
						<thead>
							<tr>
								<th>ID Number</th>
								<th>Name</th>
								<th>Reason</th>
								<th>Leave Date</th>
								<th>Status</th>
								<th style="width: 68px;">Action</th>
							</tr>
						</thead>
						<tbody>
							<?php foreach($leaves as $leave): ?>
							<tr>
								<td><?php echo $suser['userid']; ?></td>
								<td><?php echo $suser['firstname']; ?><?php echo $suser['lastname']; ?></td>
								<td><?=h($leave->leave_reason)?></td>
								<td><?=h($leave->leave_date)?></td>
								<td>
									<?php 
										if ($leave->leave_approval == 0) {
											echo '<span class="label label-default">pending</span>';
										} else if ($leave->leave_approval == 1) {
											echo '<span class="label label-success">approved</span>';
										} else {
											echo '<span class="label label-danger">rejected</span>';
										}
									?>
								</td>
								<td>
									<?php 
										echo $this->Html->link(
										'<i class="fa fa-pencil" aria-hidden="true"></i>',
										[
											'controller'=>'users',
											'action'=>'teachereditleave',
											h($leave->id)
										],
										['escape' => false, 'class' => 'btn btn-success btn-xs ' ]
										);
									?>
									<?php 
										echo $this->Html->link(
										'<i class="fa fa-trash" aria-hidden="true"></i>',
										[
											'controller'=>'users',
											'action'=>'teacherdeleteleave',
											h($leave->id)
										],
										['confirm' => 'Are you sure you want to delete this leave?', 'escape' => false, 'class' => 'btn btn-danger btn-xs ' ]
										);
									?>
								</td>
							</tr>
							<?php endforeach; ?> 
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>
