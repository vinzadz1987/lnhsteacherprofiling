 <?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @since         0.10.0
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */
use Cake\Cache\Cache;
use Cake\Core\Configure;
use Cake\Core\Plugin;
use Cake\Datasource\ConnectionManager;
use Cake\Error\Debugger;
use Cake\Network\Exception\NotFoundException;

$this->layout = false;

if (!Configure::read('debug')):
    throw new NotFoundException('Please replace src/Template/Pages/home.ctp with your own version.');
endif;

$cakeDescription = 'LNHS Teacher Profiling';
?>
<!DOCTYPE html>
<html>
<head>
    <?= $this->Html->charset() ?>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>
        <?= $cakeDescription ?>
    </title>

    <?= $this->Html->meta('icon') ?>
    <?= $this->Html->css('cake.css') ?>
    <?= $this->Html->css('bootstrap2.min.css') ?>
    <link href="https://fonts.googleapis.com/css?family=Raleway:500i|Roboto:300,400,700|Roboto+Mono" rel="stylesheet">
</head>
 <style type="text/css">
 	.img-circle {
	    border-radius: 50%;
	}
 </style>
 <body class="bg-dark">
	<div class="container" style="max-width: 431px">
		<br><br>
		<center style="font-size: 27px; font-weight: bold;" class="text-info">
			<img src="<?php echo $this->request->webroot; ?>img/lucsoonlogo.jpg" alt="..." class="img-circle profile_img" style="width: 45px; margin-left: 0; margin-top: 0;">
			LNHS TEACHER PROFILING
		</center>
		<div class="card card-login mx-auto mt-2">
			<div class="card-header">
				<?= __('Login') ?>
			</div>
			<?= $this->Flash->render() ?>
			<div class="card-body">
				<div class="users form">
					<?= $this->Form->create() ?>
						<div class="form-group">
							<?= $this->Form->control('email', [ 'placeholder' => 'Enter email', 'class' => 'form-control', 'label' => false ]) ?>
						</div>
						<div class="form-group">
							<?= $this->Form->control('password', [ 'placeholder' => 'Enter password', 'class' => 'form-control', 'label' => false ]) ?>
						</div>
					<?= $this->Form->button(__('Login'),['class'=>'btn btn-primary btn-block']); ?>
					<?= $this->Form->end() ?>
				</div>
			</div>
		</div>
	</div>
</body>