<body class="bg-dark">
    <div class="container">
        <br><br>
        <center style="font-size: 30px; font-weight: bold;" class="text-info">LNHS TEACHER PROFILING</center>
        <div class="card card-login mx-auto mt-2">
            <div class="card-header">
                <?= __('Login') ?>
            </div>
            <div class="card-body">
                <div class="users form">
                    <?= $this->Form->create() ?>
                        <div class="form-group">
                            <?= $this->Form->control('email', [ 'placeholder' => 'Enter email', 'class' => 'form-control', 'label' => false ]) ?>
                        </div>
                        <div class="form-group">
                            <?= $this->Form->control('password', [ 'placeholder' => 'Enter password', 'class' => 'form-control', 'label' => false ]) ?>
                        </div>
                    <?= $this->Form->button(__('Login'),['class'=>'btn btn-primary btn-block']); ?>
                    <?= $this->Form->end() ?>
                </div>
            </div>
        </div>
    </div>
</body>