<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link      http://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   http://www.opensource.org/licenses/mit-license.php MIT License
 */
namespace App\Controller;

use App\Controller\AppController;
use Cake\Event\Event;
use Cake\ORM\TableRegistry;

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @link http://book.cakephp.org/3.0/en/controllers.html#the-app-controller
 */
class UsersController extends AppController
{
	public function beforeFilter(Event $event)
	{
		parent::beforeFilter($event);
		$this->Auth->allow([
			'logout',
			'adminmasterlist',
			'adminmasterlistadd',
			'adminmasterlistedit',
			'adminmasterlistview',
			'adminmasterlistdelete',
			'admindepartment',
			'admindepartmentlist',
			'adminsetdeadline',
			'admineditdeadline',
			'adminleavelist',
			'adminclearedfaculty',
			'admincheckpapers',
			'adminsavepapers',
			'adminleaveactions',
			'admintrainingseminars',
			'admintrainingseminarsadd',
			'admintrainingseminarsedit',
			'admintrainingseminarsdelete',
			'teacherfacultypds',
			'teacherservicerecords',
			'teachertrainingseminar',
			'teachertrainingjoin',
			'teacherleaveapplication',
			'teachersaln',
			'teacherleavelist',
			'teacherAddRequestLeave',
			'teachereditleave',
			'teacherdeleteleave',
			'teachersavepds',
			'teacheradminupload',
			'teacheradminuploadimages',
			'teachermessaging',
			'teachertimeline',
			'sendMessage'
		]);
	}

	public function index()
	{
		if(!($this->request->session()->read('Auth.User'))) {
			return $this->redirect(
				['action' => 'login']
			);
		}
		$this->set('users', $this->Users->find('all'));
	}

	public function login()
	{
		$this->viewBuilder()->autoLayout(false);
		if($this->request->is('post')){
			$user = $this->Auth->identify();
			if($user){
				$this->Auth->setUser($user);
				return $this->redirect($this->Auth->redirectUrl());
			}
			$this->Flash->error(__('Invalid email or password, try again'));
		}
	}

	public function logout(){
		return $this->redirect($this->Auth->logout());
	}

	function accessLevel() {
		$sessUser = $this->request->session()->read('Auth.User');
		if($sessUser['usertype'] == 1) {
			return $this->redirect('/users/facultyPds');
		}
	}

	/************************* ADMIN ACCESS *********************************/

	public function adminmasterlist() {
		if(!($this->request->session()->read('Auth.User'))) {
			return $this->redirect(
				['action' => 'login']
			);
		}
		$this->accessLevel;
		$this->viewBuilder()->setLayout('default');
		$user = null;
		$this->set('users', $this->Users->find()
			->select([
				'id',
				'userid',
				'lastname',
				'firstname',
				'email',
				'contact_no',
				'department',
				'designation',
				'usertype',
				'papers',
				'created',
				'status'
			])
		);
	}

	public function adminmasterlistedit($id = null, $type = null){
		if(empty($id)){
			throw new NotFoundException;
		}
		$user = $this->Users->get($id);
		if ($this->request->is(['post', 'put']) ){
			$user = $this->Users->patchEntity($user, $this->request->data);
			if ($this->Users->save($user)) {
				$this->Flash->success(__('The User has been updated successfully.'));
				return $this->redirect('/users/adminmasterlist');
			}
		}
		$user_type = null;
		if($type == 1) {
			$type = "Teacher";
			$user_type = 1;
		} else {
			$type = "Admin User";
			$user_type = 2;
		}
		$this->set('usertype',$user_type);
		$this->set('type',$type);
		$this->set('user',$user);
	}

	public function adminmasterlistadd($type = null){
		if(!($this->request->session()->read('Auth.User'))) {
			return $this->redirect(
				['action' => 'login']
			);
		}
		$articlesTable = TableRegistry::get('Users');
		$user = null;
		$article = $articlesTable->newEntity();
		$data = $this->request->getData();
		if(isset($data)) {
			$user = $articlesTable->patchEntity($article, $data);
			if($articlesTable->save($user)){
				$this->Flash->success(__('The user has been saved.'));
				return $this->redirect('/users/adminmasterlist');
			}
		}
		$user_type = null;
		if($type == 'teacher') {
			$type = "Teacher";
			$user_type = 1;
		} else {
			$type = "Admin User";
			$user_type = 2;
		}
		$this->set('usertype',$user_type);
		$this->set('type',$type);
		$this->set('adminmasterlistadd',$user);
	}

	public function adminmasterlistview($id = null, $type = null){
		if(!($this->request->session()->read('Auth.User'))) {
			return $this->redirect(
				['action' => 'login']
			);
		}
		if(empty($id)){
			throw new NotFoundException;
		}
		$user = $this->Users->get($id);
		$user_type = null;
		if($type == 1) {
			$type = "Teacher";
			$user_type = 1;
		} else {
			$type = "Admin User";
			$user_type = 2;
		}
		$this->set('usertype',$user_type);
		$this->set('type',$type);
		$this->set('user',$user);
	}

	public function adminmasterlistdelete($id = null)
	{
		if(empty($id)){
			throw new NotFoundException;
		}
		$user = $this->Users->get($id);
		$result = $this->Users->delete($user);
		if ( $result ){
			$this->Flash->success(__('The User has been successfully removed.'));
			return $this->redirect('/users/adminmasterlist');
		}
	}

	public function teacheradminupload() {
		if(!($this->request->session()->read('Auth.User'))) {
			return $this->redirect(
				['action' => 'login']
			);
		}
		$this->viewBuilder()->setLayout('default');
		$suser = $this->request->session()->read('Auth.User');
		$this->set('users', $this->Users->find()->select(['photo'])->where(['userid'=>$suser['userid']]));
	}

	public function teacheradminuploadimages()
    {
		$this->viewBuilder()->setLayout('default');
		$suser = $this->request->session()->read('Auth.User');
        if (isset($_POST['submit'])) {
            $target_path = WWW_ROOT . 'img/';
            $file_name = $_FILES['ufile']['name'];
            $parts = explode(".", $file_name);
            $fname = $parts[0];
            $new_file_name = $fname . $suser['userid'] . "." .$parts[1];
            $to_path = $target_path . $fname . $suser['userid'] . "." .$parts[1]; //set the target path with a new name of image
            if ($file_name != '') {

                if (move_uploaded_file($_FILES['ufile']['tmp_name'], $to_path)) {
					$users = TableRegistry::get('Users');
					$query = $users->query();
					$query->update()
						->set(['photo' => $new_file_name])
						->where(['userid' => $suser['userid']])
						->execute();
                    $this->Flash->success(__('photo uploaded.'));
					return $this->redirect('/users/teacheradminupload');
                } else {
                    echo "Error";
                }
            }
        }
    }

	public function view($id)
	{
		$user = $this->Users->get($id);
		$this->set(compact('user'));
	}

	public function admincheckpapers($id=null){
		if(!($this->request->session()->read('Auth.User'))) {
			return $this->redirect(
				['action' => 'login']
			);
		}
		$setdeadline = $this->Users->get($id);
		if ($this->request->is(['post', 'put']) ){
			$setdeadline = $this->Users->patchEntity($setdeadline, $this->request->data);
			if ($this->Users->save($setdeadline)) {
				$this->Flash->success(__('The deadline has been set successfully.'));
				return $this->redirect('/users/adminsetdeadline');
			}
		}
		$this->set('users', $setdeadline);
	}

	public function adminsavepapers() {
		if(!($this->request->session()->read('Auth.User'))) {
			return $this->redirect(
				['action' => 'login']
			);
		}
		if($this->request->is('ajax')) {
			$id = $this->request->data['id'];
			$user = $this->Users->get($id);
			$user->papers = $this->request->data['papers'];
			$user->status = $this->request->data['status'];
			if ($this->Users->save($user)) {
				return $this->redirect('/users/adminmasterlist');
			}
		}
	}

	public function admindepartmentlist($department = null) {
		$bydepartment = $this->Users->find('all')->where(['department' => $department]);
		$this->set('users', $bydepartment);
		$this->set('department', $department);
		if(!($this->request->session()->read('Auth.User'))) {
			return $this->redirect(
				['action' => 'login']
			);
		}
	}

	public function adminclearedfaculty() {
		$this->viewBuilder()->setLayout('default');
		$user = null;
		$clearedfaculty = $this->Users->find('all')->where(['status' => 1 ]);
		$this->set('users', $clearedfaculty);
		if(!($this->request->session()->read('Auth.User'))) {
			return $this->redirect(
				['action' => 'login']
			);
		}
	}

	public function admindepartment() {
		if(!($this->request->session()->read('Auth.User'))) {
			return $this->redirect(
				['action' => 'login']
			);
		}
		$this->viewBuilder()->setLayout('default');
		$user = null;
		$query = $this->Users->find();
		$queryCount = $query->select([
			'department',
			'count' => "COUNT(DISTINCT `id`)"
		])->group(['department']);
		$this->set('department', $queryCount);
	}

	public function adminsetdeadline() {
		if(!($this->request->session()->read('Auth.User'))) {
			return $this->redirect(
				['action' => 'login']
			);
		}
		$user = null;
		$setdeadline = $this->Users->find()->select(['id','userid','lastname','department','firstname','submitdate','deadline'])->where(['OR' => [ ['status' => 1], ['status' => 0] ] ]);
		$this->set('users', $setdeadline);
	}

	public function admineditdeadline($id = null) {
		if(!($this->request->session()->read('Auth.User'))) {
			return $this->redirect(
				['action' => 'login']
			);
		}
		$setdeadline = $this->Users->get($id);
		if ($this->request->is(['post', 'put']) ){
			$setdeadline = $this->Users->patchEntity($setdeadline, $this->request->data);
			if ($this->Users->save($setdeadline)) {
				$this->Flash->success(__('The deadline has been set successfully.'));
				return $this->redirect('/users/adminsetdeadline');
			}
		}
		$this->set('users', $setdeadline);
	}

	public function adminleavelist() {
		if(!($this->request->session()->read('Auth.User'))) {
			return $this->redirect(
				['action' => 'login']
			);
		}
		$this->loadModel('Leaves');
		$this->set('leaves', $this->Leaves->find('all'));
	}

	public function adminleaveactions($id = null) {
		if(!($this->request->session()->read('Auth.User'))) {
			return $this->redirect(
				['action' => 'login']
			);
		}
		$this->loadModel('Leaves');
		if($this->request->is('ajax')) {
			$user = $this->Leaves->get($id);
			$user->leave_approval = $this->request->data['leave_approval'];
			if ($this->Leaves->save($user)) {
				return $this->redirect('/users/adminleavelist');
			}
		}
	}

	public function admintrainingseminars() {
		if(!($this->request->session()->read('Auth.User'))) {
			return $this->redirect(
				['action' => 'login']
			);
		}
		$this->loadModel('Training_seminars');
		$this->set('training_seminar', $this->Training_seminars->find('all'));
	}
	public function admintrainingseminarsadd() {
		if(!($this->request->session()->read('Auth.User'))) {
			return $this->redirect(
				['action' => 'login']
			);
		}
		$training_seminar = TableRegistry::get('training_seminars');
		$user = null;
		$article = $training_seminar->newEntity();
		$data = $this->request->getData();
		if(isset($data)) {
			$user = $training_seminar->patchEntity($article, $data);
			if($training_seminar->save($user)){
				$this->Flash->success(__('save successfully.'));
				return $this->redirect('/users/admintrainingseminars');
			}
		}
		$this->set('training_seminar',$user);
	}

	public function admintrainingseminarsedit($id = null){
		if(empty($id)){
			throw new NotFoundException;
		}
		$training_seminar = TableRegistry::get('training_seminars');
		$dtraining_seminar = $training_seminar->get($id);
		if ($this->request->is(['post', 'put']) ){
			$dtraining_seminar = $training_seminar->patchEntity($dtraining_seminar, $this->request->data);
			if ($training_seminar->save($dtraining_seminar)) {
				$this->Flash->success(__('updated successfully'));
				return $this->redirect('/users/admintrainingseminars');
			}
		}
		$this->set('adminedittrainingseminars',$dtraining_seminar);
	}
	public function admintrainingseminarsdelete($id = null){
		$training_seminar = TableRegistry::get('training_seminars');
		if(empty($id)){
			throw new NotFoundException;
		}
		$ts = $training_seminar->get($id);
		$result = $training_seminar->delete($ts);
		if ( $result ){
			$this->Flash->success(__('successfully removed.'));
			return $this->redirect('/users/admintrainingseminars');
		}
	}

	/************************* END ADMIN ACCESS *******************************/

	/************************* FACULTY ACCESS *********************************/
	public function teacherfacultypds($id = "") {

		$this->viewBuilder()->setLayout('default');

		if(!($this->request->session()->read('Auth.User'))) {
			return $this->redirect(
				['action' => 'login']
			);
		}

		$sessUser = $this->request->session()->read('Auth.User');
		$usersTable = TableRegistry::get('Users');
		$sessUser = $this->request->session()->read('Auth.User');
		$this->viewBuilder()->setLayout('default');
		$users = $usersTable->get($sessUser['id']);
		$this->set('users', $users);

	}

	public function teachersavepds($id = null){

		$logs = null;

		if(!($this->request->session()->read('Auth.User'))) {
			return $this->redirect(
				['action' => 'login']
			);
		}

		$usersTable = TableRegistry::get('Users');
		$sessUser = $this->request->session()->read('Auth.User');
		if($sessUser['usertype'] == 2) {
			return $this->redirect('/users/adminmasterlist');
		}

		$this->viewBuilder()->setLayout('default');
		$usergetData = $usersTable->get($id);

		if ($this->request->is(['post', 'put']) ){
			$usergetData = $usersTable->patchEntity($usergetData, $this->request->data);
			if ($usersTable->save($usergetData)) {
				$this->Flash->success(__('The PDS save successfully.'));

				$logsTable = TableRegistry::get('Logs');
				$logs = $logsTable->newEntity();
				$logs->userid = $sessUser['userid'];
				$logs->message = "Updated PDS";
				if($logsTable->save($logs)) {

				}

				return $this->redirect('/users/teacherfacultypds/'.$id);
			}
			$this->Flash->error(__('There something wrong saving data.'));
		}

		$this->set('users', $usergetData);
	}

	public function teacherservicerecords($id = null) {
		if(!($this->request->session()->read('Auth.User'))) {
			return $this->redirect(
				['action' => 'login']
			);
		}
		$usersTable = TableRegistry::get('Users');
		$sessUser = $this->request->session()->read('Auth.User');
		$users = $usersTable->get($sessUser['id']);
		if ($this->request->is(['post', 'put']) ){
			$users = $usersTable->patchEntity($users, $this->request->data);
			if ($usersTable->save($users)) {
				$this->Flash->success(__('The service records save successfully.'));
				$logsTable = TableRegistry::get('Logs');
				$logs = $logsTable->newEntity();
				$logs->userid = $sessUser['userid'];
				$logs->message = "Updated Service Records";
				if($logsTable->save($logs)) {

				}
				return $this->redirect('/users/teacherservicerecords/'.$sessUser['id']);
			}
			$this->Flash->error(__('There something wrong saving data.'));
		}
		$this->set('users', $users);
	}

	public function teachertrainingseminar() {
		if(!($this->request->session()->read('Auth.User'))) {
			return $this->redirect(
				['action' => 'login']
			);
		}
		$usersTable = TableRegistry::get('Users');
		$sessUser = $this->request->session()->read('Auth.User');
		if($sessUser['usertype'] == 2) {
			return $this->redirect('/users/adminmasterlist');
		}
		$this->loadModel('Training_seminars');
		$this->set('training_seminar', $this->Training_seminars->find('all'));
	}

	public function teachertrainingjoin($id=null) {
		if(!($this->request->session()->read('Auth.User'))) {
			return $this->redirect(
				['action' => 'login']
			);
		}
		$Training_seminars = $this->loadModel('Training_seminars');
		if($this->request->is('ajax')) {
			$seminars = $Training_seminars->get($id);
			$suser = $this->request->session()->read('Auth.User');
			$seminars->joiner = $this->request->data['joiner'].','.$suser['firstname'].' '.$suser['lastname'];
			if ($Training_seminars->save($seminars)) {
				return $this->redirect('/users/teachertrainingseminar');
			}
		}
	}

	public function teacherleavelist() {
		if(!($this->request->session()->read('Auth.User'))) {
			return $this->redirect(
				['action' => 'login']
			);
		}
		$sessUser = $this->request->session()->read('Auth.User');
		$this->loadModel('Leaves');
		$this->set('leaves', $this->Leaves->find('all')->where(['userid'=>$sessUser['userid']]));
	}

	public function teacherleaveapplication() {
		if(!($this->request->session()->read('Auth.User'))) {
			return $this->redirect(
				['action' => 'login']
			);
		}
		$usersTable = TableRegistry::get('Users');
		$sessUser = $this->request->session()->read('Auth.User');
		if($sessUser['usertype'] == 2) {
			return $this->redirect('/users/adminmasterlist');
		}
		$this->viewBuilder()->setLayout('default');
		$users = $usersTable->get($sessUser['id']);
		$this->set('users', $users);
	}

	public function teachereditleave($id = null) {
		if(!($this->request->session()->read('Auth.User'))) {
			return $this->redirect(
				['action' => 'login']
			);
		}
		$leavesTable = $this->loadModel('Leaves');
		$leaves = $leavesTable->get($id);
		if ($this->request->is(['post', 'put']) ){
			$leaves = $leavesTable->patchEntity($leaves, $this->request->data);
			if ($leavesTable->save($leaves)) {
				$this->Flash->success(__('Updated leave successfully.'));
				return $this->redirect('/users/teacherleavelist');
			}
		}
		$this->set('leaves', $leaves);
	}

	public function teacherAddRequestLeave() {
		if(!($this->request->session()->read('Auth.User'))) {
			return $this->redirect(
				['action' => 'login']
			);
		}
		$this->loadModel('Leaves');
		$leaves = null;
		$data = $this->request->getData();
		$leaves = $this->Leaves->newEntity();
		if(isset($data)) {
           $leaves = $this->Leaves->patchEntity($leaves, $data);
           if($this->Leaves->save($leaves)){
           	$this->Flash->success(__('Requested leave successfully.'));
           	return $this->redirect('/users/teacherleavelist');
           }
		}
	}

	public function teacherdeleteleave($id = null)
	{
		$leavesTable = $this->loadModel('Leaves');
		if(empty($id)){
			throw new NotFoundException;
		}
		$user = $leavesTable->get($id);
		$result = $leavesTable->delete($user);
		if ( $result ){
			$this->Flash->success(__('The Leave has been successfully removed.'));
			return $this->redirect('/users/teacherleavelist');
		}
	}

	public function teachersaln($id=null) {
		$sessUser = $this->request->session()->read('Auth.User');
		if(!($sessUser)) {
			return $this->redirect(
				['action' => 'login']
			);
		}
		$usersTable = TableRegistry::get('Users');
		$usergetData = $usersTable->get($sessUser['id']);
		if ($this->request->is(['post', 'put']) ){
			$usergetData = $usersTable->patchEntity($usergetData, $this->request->data);
			if ($usersTable->save($usergetData)) {
				$this->Flash->success(__('The SALN save successfully.'));
				$logsTable = TableRegistry::get('Logs');
				$logs = $logsTable->newEntity();
				$logs->userid = $sessUser['userid'];
				$logs->message = "Updated SALN";
				if($logsTable->save($logs)) {

				}
				return $this->redirect('/users/teachersaln/'.$id);
			}
			$this->Flash->error(__('There something wrong saving data.'));
		}
		$this->set('teachersaln', $usergetData);
	}

	public function teachermessaging() {
		$sessUser = $this->request->session()->read('Auth.User');
		if(!($sessUser)) {
			return $this->redirect(
				['action' => 'login']
			);
		}
		$Messaging = TableRegistry::get('Messaging');
		$Users = TableRegistry::get('Users');
		$Users = $Users->find('all')->where(['userid !=' => $sessUser['userid']]);
		$MessageUsers = $Users->find('all');
		$Messaging = $Messaging->find('all')->group('recipient')->order(['datesent' => 'DESC']);
		$this->set('Messaging',$Messaging->toArray());
		$this->set('forSelectUsers',$Users->toArray());
		$this->set('forMessageUsers',$MessageUsers->toArray());
	}

	public function sendMessage() {
		$this->loadModel('Messaging');
		$message = null;
		$data = $this->request->getData();
		$message = $this->Messaging->newEntity();
		if(isset($data)) {
           $message = $this->Messaging->patchEntity($message, $data);
           if($this->Messaging->save($message)){
           	$this->Flash->success(__('Message Sent.'));
           	return $this->redirect('/users/teachermessaging');
           }
		}
	}

	public function teachertimeline() {
		$sessUser = $this->request->session()->read('Auth.User');
		if(!($sessUser)) {
			return $this->redirect(
				['action' => 'login']
			);
		}
		$logsTable = TableRegistry::get('Logs');
		$logs = $logsTable->find('all');
		$this->set('logs', $logs->toArray());
	}
}
