<?php
	namespace App\Model\Table;

	use Cake\ORM\Table;
	use Cake\Validation\Validator;
	use Cake\ORM\RulesChecker;

	class UsersTable extends Table
	{
		public function validationDefault(Validator $validator)
		{
			$validator
				->notEmpty('designation', 'A designation is required')
				->notEmpty('department', 'A department is required')
				->notEmpty('userid', 'A user id is required')
				->notEmpty('username', 'A username is required')
				->notEmpty('lastname', 'A lastname is required')
				->notEmpty('firstname', 'A firstname is required')
				->notEmpty('middlename', 'A middlename is required')
				->notEmpty('contact_no', 'A contact no is required')
				->notEmpty('email', 'A email is required')
				->notEmpty('dateofbirth', 'A date of birth is required')
				->notEmpty('placeofbirth', 'A place of birth is required')
				->notEmpty('civilstatus', 'A civil status is required')
				->notEmpty('height', 'A height is required')
				->notEmpty('weight', 'A weight is required')
				->notEmpty('photo', 'A photo is required')
				->notEmpty('bloodtype', 'A blood type is required')
				->notEmpty('gsisidno', 'A GSIS ID no is required')
				->notEmpty('pagibigidno', 'A PAG IBIG ID no is required')
				->notEmpty('philhealthno', 'A PHILHEALTH no is required')
				->notEmpty('sssno', 'A SSS no is required')
				->notEmpty('sex', 'A Sex no is required')
				->notEmpty('tinno', 'A TIN no is required')
				->notEmpty('agencyemployeeno', 'A Agency employee no is required')
				->notEmpty('citizenship', 'A Citizenship is required')
				->notEmpty('residence_address_brgy', 'A Barangay is required')
				->notEmpty('residence_address_city', 'A City/Municipality is required')
				->notEmpty('residence_address_zip', 'A Zip Code is required')
				->notEmpty('permanent_address_brgy', 'A Barangay is required')
				->notEmpty('permanent_address_city', 'A City/Municipality is required')
				->notEmpty('permanent_address_zip', 'A Zip Code is required')
				->notEmpty('telephoneno', 'A Telephone no is required')
				->notEmpty('mobileno', 'A Mobile no is required')
				->notEmpty('spousesurname', 'A spouse surname  is required')
				->notEmpty('spousefirstname', 'A spouse firstname  is required')
				->notEmpty('spousemiddlename', 'A spouse middlename  is required')
				->notEmpty('spousenameextension', 'A spouse name extension  is required')
				->notEmpty('occupation', 'A occupation  is required')
				->notEmpty('employerbusinessname', 'A employer businessname extension  is required')
				->notEmpty('spouse_telno', 'A spouse telno extension  is required')
				->notEmpty('father_surname', 'A father surname is required')
				->notEmpty('fatherfirstname', 'A father firstname is required')
				->notEmpty('fathermiddlename', 'A father middlename is required')
				->notEmpty('fathernameextension', 'A father name extension is required')
				->notEmpty('mother_maidenname', 'A mother_maidenname is required')
				->notEmpty('mothersurname', 'A mothersurname  is required')
				->notEmpty('motherfirstname', 'A motherfirstname is required')
				->notEmpty('mothermiddlename', 'A mothermiddlename is required')
				->notEmpty('password', 'A password is required');

			$validator
			// 	->add('password', [
			// 	'length' => [
			// 		'rule' => ['minLength', 6],
			// 		'message' => 'The password have to be at least 6 characters!',
			// 	]
			// ])
			->add('password',[
				'match'=>[
					'rule'=> ['compareWith','password2'],
					'message'=>'The passwords does not match!',
				]
			])
			->notEmpty('password');
			$validator
			// ->add('password2', [
			// 	'length' => [
			// 		'rule' => ['minLength', 6],
			// 		'message' => 'The password have to be at least 6 characters!',
			// 	]
			// ])
			->add('password2',[
				'match'=>[
					'rule'=> ['compareWith','password'],
					'message'=>'The passwords does not match!',
				]
			])
			->notEmpty('password2');

			return $validator;
		}

		public function buildRules(RulesChecker $rules)
		{
			$rules->add($rules->isUnique(['email']));
			$rules->add($rules->isUnique(['userid']));
			return $rules;
		}

	}
