$(function () {

	$('.saln1,#assetsdatesign,#assetsdateissued1,#assetsdateissued2,.businessinterest3 ').mask("99/99/9999");
	$('.assetslialnet5').mask('9999');
	$('.saln2').attr('type','number');
	$('.assetslialnet7').attr('type','number');
	$('.saln2').keyup(function(){
		if ($(this).val() > 17){
			alert("age limit is 17");
			$(this).val('17');
		}
	});
	var arrayTable = ['3_saln','8_assetslialnet']; 
	for(x=0;x<arrayTable.length;x++) {
		for(i=0;i<arrayTable[x].split('_')[0];i++) {
			$("."+arrayTable[x].split('_')[1]+i).keyup(function(){
				getdata($(this).data('id'));
			});
		}
	}
	distribute_data("declarantHouseholds");
	distribute_data("assetslialnet");
});
	
function distribute_data(table) {
	if(table == "declarantHouseholds") {
		var eData = ['salnchildrensname','salnchildrensdob','salnchildrensage'];
		var column = "salncolumn_";
	} else if(table == "assetslialnet") {
		eData = ['salnassetsdescription','salnassetskind','salnassetslocation','salnassetsassvalue','salnassetsmarketvalue',
								'salnassetsyear','salnassetsmode','salnassetscost'];
		column = "assetslialnetcolumn_";
	} 
	for(y=0; y<eData.length;y++) {
		var pEData = $("#"+eData[y]).val().split(',');
		$.each(pEData, function(i) {
			$("#"+table).find('td input.'+column+y+'_'+i).each(function() {
				$(this).val(pEData[i]);
			});
		});
	}
}

function getdata(rowname) {
	var iData = new Array();
	$("."+rowname).each(function() {
		iData.push($(this).val());
	});
	var input = "";
	switch (rowname) {
		case 'saln0':
			input = "salnchildrensname";
		break;
		case 'saln1':
			input = "salnchildrensdob";
		break;
		case 'saln2':
			input = "salnchildrensage";
		break;
		case 'assetslialnet0':
			input = "salnassetsdescription";
		break;
		case 'assetslialnet1':
			input = "salnassetskind";
		break;
		case 'assetslialnet2':
			input = "salnassetslocation";
		break;
		case 'assetslialnet3':
			input = "salnassetsassvalue";
		break;
		case 'assetslialnet4':
			input = "salnassetsmarketvalue";
		break;
		case 'assetslialnet5':
			input = "salnassetsyear";
		break;
		case 'assetslialnet6':
			input = "salnassetsmode";
		break;
		case 'assetslialnet7':
			input = "salnassetscost";
		break;
	}
	$("#"+input).val(iData);
}