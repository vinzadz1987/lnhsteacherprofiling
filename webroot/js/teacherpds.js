$(function () {

	$('.stepContainer').attr('style','height: 1500px;');

	$("#dateofbirth").datepicker({
		autoclose: true,
		todayHighlight: false
	});
	$("input[name=childdob]").datepicker({
		autoclose: true,
		todayHighlight: false
	});

	$(".educ3,.educ4,.csecolumn_6,.civil5,.work0,.work1,.voluntary1,.voluntary2,.learning1,.learning2").mask("99/99/9999");
	$("#telephoneno").mask("9999999");
	$("#sssno").mask("9999-9999999-9");
	$("#pagibigidno").mask("9999-9999-9999");
	$("#tinno").mask("999-999-999-999");
	$("#philhealthno").mask("99-999999999-9");

	$("input[name=childname],input[name=childdob]").on('keyup', function() {
		getChildName();
		$("#nameofchildrens").val(getChildName);
	});

	jQuery('input[name=childdob]').on('change', function () {
		 getChildBod();
			$("#childrendateofbirth").val(getChildBod);
	 });
	var dataChildName = $("#nameofchildrens").val().split(',');
	$.each(dataChildName, function(i) {
		$("#c1").find('td input.childname'+i).each(function() {
			$(this).val(dataChildName[i]);
		});
	});
	var dataChildBod = $("#childrendateofbirth").val().split(',');
	$.each(dataChildBod, function(i) {
		$("#c1").find('td input.childdob'+i).each(function() {
			$(this).val(dataChildBod[i]);
		});
	});

	var educSchoolName = $("#education-school-name").val().split(',');
	$.each(educSchoolName, function(i) {
		$("#educationTable").find('td input.educcolumn_1_'+i).each(function() {
			$(this).val(educSchoolName[i]);
		});
	});
	var educSchoolName1 = $("#education-degree-course").val().split(',');
	$.each(educSchoolName1, function(i) {
		$("#educationTable").find('td input.educcolumn_2_'+i).each(function() {
			$(this).val(educSchoolName1[i]);
		});
	});
	var educSchoolName2 = $("#education-attendance-from").val().split(',');
	$.each(educSchoolName2, function(i) {
		$("#educationTable").find('td input.educcolumn_3_'+i).each(function() {
			$(this).val(educSchoolName2[i]);
		});
	});
	var educSchoolName3 = $("#education-attendance-to").val().split(',');
	$.each(educSchoolName3, function(i) {
		$("#educationTable").find('td input.educcolumn_4_'+i).each(function() {
			$(this).val(educSchoolName3[i]);
		});
	});
	var educSchoolName4 = $("#education-highest-level").val().split(',');
	$.each(educSchoolName4, function(i) {
		$("#educationTable").find('td input.educcolumn_5_'+i).each(function() {
			$(this).val(educSchoolName4[i]);
		});
	});
	var educSchoolName5 = $("#education-year-graduated").val().split(',');
	$.each(educSchoolName5, function(i) {
		$("#educationTable").find('td input.educcolumn_6_'+i).each(function() {
			$(this).val(educSchoolName5[i]);
		});
	});
	var educSchoolName6 = $("#education-scholars").val().split(',');
	$.each(educSchoolName6, function(i) {
		$("#educationTable").find('td input.educcolumn_7_'+i).each(function() {
			$(this).val(educSchoolName6[i]);
		});
	});
	
	distribute_data1('civilserviceTable');
	distribute_data1('workexperienceTable');
	distribute_data1('voluntaryTable');
	distribute_data1('learningTable');

	for(i=1;i<8;i++) { 
		$(".educ"+i).keyup(function(i){
			getdata($(this).data('id'));
		});
	}
	var arrayTable = ['7_civil','8_work','5_voluntary','6_learning'];
	for(x=0;x<arrayTable.length;x++) {
		for(i=0;i<arrayTable[x].split('_')[0];i++) {
			$("."+arrayTable[x].split('_')[1]+i).keyup(function(){
				getdata($(this).data('id'));
			});
		}
	}
});
	
function distribute_data1(table) {
	if(table == "workexperienceTable") {
		var eData = ['we-inclusive-from','we-inclusive-to','we-position','we-department-agency','we-monthly-salary',
		'we-salary-job','we-status-of-appointment','we-govt-service'];
		var column = "workcolumn_";
	} else if(table == "civilserviceTable") {
		eData = ['cse-career','cse-rating','cse-exam-date','cse-exam-place','cse-license-no','cse-license-expiry'];
		column = "csecolumn_";
	} else if(table == "voluntaryTable") {
		eData = [	
				'voluntary-name-org',
				'voluntary-inclusive-from',
				'voluntary-inclusive-to',
				'voluntary-hrs-no',
				'voluntary-position'
				];
		column = "voluntarycolumn_";
	} else {
		eData = [
			'learning-title',
			'learning-inclusive-from',
			'learning-inclusive-to',
			'learning-hrs-no',
			'learning-id-type',
			'learning-conducted'
			];
		column = "learningcolumn_";
	}
	for(y=0; y<eData.length;y++) {
		var pEData = $("#"+eData[y]).val().split(',');
		$.each(pEData, function(i) {
			$("#"+table).find('td input.'+column+y+'_'+i).each(function() {
				$(this).val(pEData[i]);
			});
		});
	}
}

function getdata(rowname) {
	var iData = new Array();
	$("."+rowname).each(function() {
		iData.push($(this).val());
	});
	var input = "";
	switch (rowname) {
		case 'educ1':
			input = "education-school-name";
		break;
		case 'educ2':
			input = "education-degree-course";
		break;
		case 'educ3':
			input = "education-attendance-from";
		break;
		case 'educ4':
			input = "education-attendance-to";
		break;
		case 'educ5':
			input = "education-highest-level";
		break;
		case 'educ6':
			input = "education-year-graduated";
		break;
		case 'educ7':
			input = "education-scholars";
		break;
		case 'civil0':
			input = "cse-career";
		break;
		case 'civil1':
			input = "cse-rating";
		break;
		case 'civil2':
			input = "cse-exam-date";
		break;
		case 'civil3':
			input = "cse-exam-place";
		break;
		case 'civil4':
			input = "cse-license-no";
		break;
		case 'civil5':
			input = "cse-license-expiry";
		break;
		case 'work0':
			input = "we-inclusive-from";
		break;
		case 'work1':
			input = "we-inclusive-to";
		break;
		case 'work2':
			input = "we-position";
		break;
		case 'work3':
			input = "we-department-agency";
		break;
		case 'work4':
			input = "we-monthly-salary";
		break;
		case 'work5':
			input = "we-salary-job";
		break;
		case 'work6':
			input = "we-status-of-appointment";
		break;
		case 'work7':
			input = "we-govt-service";
		break;
		case 'voluntary0':
			input = "voluntary-name-org";
		break;
		case 'voluntary1':
			input = "voluntary-inclusive-from";
		break;
		case 'voluntary2':
			input = "voluntary-inclusive-to";
		break;
		case 'voluntary3':
			input = "voluntary-hrs-no";
		break;
		case 'voluntary4':
			input = "voluntary-position";
		break;
		case 'learning0':
			input = "learning-title";
		break;
		case 'learning1':
			input = "learning-inclusive-from";
		break;
		case 'learning2':
			input = "learning-inclusive-to";
		break;
		case 'learning3':
			input = "learning-hrs-no";
		break;
		case 'learning4':
			input = "learning-id-type";
		break;
		case 'learning5':
			input = "learning-conducted";
		break;
	}
	$("#"+input).val(iData);
}

function getChildName() {
	var arrayChildName = new Array();
	$('input[name=childname]').each(function() {
		arrayChildName.push($(this).val());
	});
	return arrayChildName;
}

function getChildBod() {
	var arraychilddob =  new Array();
	$('input[name=childdob]').each(function() {
		arraychilddob.push($(this).val());
	});
	return arraychilddob;
}