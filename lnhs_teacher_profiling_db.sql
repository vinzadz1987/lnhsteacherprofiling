-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jan 13, 2018 at 12:12 PM
-- Server version: 10.1.28-MariaDB
-- PHP Version: 7.1.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `lnhs_teacher_profiling_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `department`
--

CREATE TABLE `department` (
  `id` int(11) NOT NULL,
  `department` varchar(225) NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `department`
--

INSERT INTO `department` (`id`, `department`, `created`) VALUES
(1, 'Junior High', '2017-11-17 07:29:40'),
(2, 'Senior High (GAS)', '2017-11-17 07:29:40'),
(3, 'Senior High (ABM)', '2017-11-17 07:29:40'),
(4, 'Senior High (HUMSS)', '2017-11-17 07:29:40'),
(5, 'Senior High (STEM)', '2017-11-17 07:29:40'),
(6, 'Senior High (TLE)', '2017-11-17 07:29:40');

-- --------------------------------------------------------

--
-- Table structure for table `leaves`
--

CREATE TABLE `leaves` (
  `id` int(11) NOT NULL,
  `name` varchar(225) NOT NULL,
  `userid` int(11) DEFAULT NULL,
  `leave_reason` varchar(100) NOT NULL,
  `leave_date` varchar(100) NOT NULL,
  `leave_date_from` varchar(100) NOT NULL,
  `leave_date_to` varchar(100) NOT NULL,
  `leave_approval` int(11) NOT NULL COMMENT '0: pending, 1: approve: 2 rejected',
  `important_comments` text NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `leaves`
--

INSERT INTO `leaves` (`id`, `name`, `userid`, `leave_reason`, `leave_date`, `leave_date_from`, `leave_date_to`, `leave_approval`, `important_comments`, `created`) VALUES
(1, 'SiegfredLove me', 12222, 'Sick', '12/13/2017', '12/05/2017', '12/18/2017', 1, 'teachers', '2017-12-19 16:13:17'),
(2, 'Lemuel1Lemuel1', 12345, 'Unpaid Leave', '12/19/2017', '12/22/2017', '12/26/2017', 2, 'dtest', '2017-12-30 12:04:08'),
(3, 'SiegfredLove me', 12222, 'Maternity/Paternity', '12/18/2017', '12/09/2017', '12/17/2017', 1, 'fd', '2017-12-19 18:27:08');

-- --------------------------------------------------------

--
-- Table structure for table `messaging`
--

CREATE TABLE `messaging` (
  `id` int(11) NOT NULL,
  `sender` varchar(50) NOT NULL,
  `recipient` text NOT NULL,
  `title` varchar(225) NOT NULL,
  `message` text NOT NULL,
  `status` int(11) NOT NULL,
  `datesent` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `datereply` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `messaging`
--

INSERT INTO `messaging` (`id`, `sender`, `recipient`, `title`, `message`, `status`, `datesent`, `datereply`) VALUES
(1, '12222', '12222,32434', 'test', 'test', 0, '2018-01-12 08:34:29', ''),
(2, '12222', '12222,32434,asaassasa', 'testt', 'test', 0, '2018-01-12 08:34:27', ''),
(3, '12222', '12222', '', '', 0, '2018-01-12 08:34:26', ''),
(4, '12222', '32434', 'test', 'test', 0, '2018-01-12 08:34:25', ''),
(5, '12222', '12222,32434', '', '', 0, '2018-01-12 08:34:24', ''),
(6, '12222', '12222,32434', 'tsts', '', 0, '2018-01-12 08:34:23', ''),
(7, '12222', '12222,32434', 'tsts', 'test', 0, '2018-01-12 08:34:21', ''),
(8, '12222', '12222,32434', 'tsts', 'test', 0, '2018-01-12 08:34:19', ''),
(9, '12222', '32434,124653', 'ydytest', 'test sdef', 0, '2018-01-12 08:34:18', ''),
(10, '12222', '23434', 'tst', 'test', 0, '2018-01-12 08:34:11', ''),
(11, '12222', '32434', 'test', 'test', 0, '2018-01-12 08:34:13', ''),
(12, '12222', '23434,124653', 'ydy', 'yrda', 0, '2018-01-12 08:34:14', ''),
(13, '12222', '23434,124653', 'ydy', 'yrda', 0, '2018-01-12 08:34:16', ''),
(14, '12222', 'saasd,7777', 'testt', 'tasdfsf', 0, '2018-01-12 08:34:08', ''),
(15, '12222', '12222,saasd', 'test', 'test', 0, '2018-01-12 08:31:15', ''),
(16, '12222', '12222,23434,1234,343434', 'sdfsdf', 'sdfsddsf', 0, '2018-01-12 08:35:21', ''),
(17, '124653', '12222,asaassasa,saasd,124653,1234,123,7777,12345,4564465,343434', 'test', 'test', 0, '2018-01-13 03:54:20', ''),
(18, '124653', '12222,32434', 'hello', 'hi', 0, '2018-01-13 06:17:47', ''),
(19, '12345', '12323', 'hello', 'how are you', 0, '2018-01-13 07:03:58', ''),
(21, '12345', '12222', 'hi', 'hello', 0, '2018-01-13 07:06:13', ''),
(22, '12222', '12323', 'test', 'test', 0, '2018-01-13 08:14:08', ''),
(23, '12222', '12222,32434,12345', 'Kumusta na kayo ma pare koy', 'kumusta na', 0, '2018-01-13 10:03:45', '');

-- --------------------------------------------------------

--
-- Table structure for table `training_seminars`
--

CREATE TABLE `training_seminars` (
  `id` int(11) NOT NULL,
  `name` varchar(225) NOT NULL,
  `description` text NOT NULL,
  `type` int(11) NOT NULL COMMENT '1:seminars, 2: traings',
  `joiner` text NOT NULL,
  `venue` varchar(225) NOT NULL,
  `date` varchar(100) NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `training_seminars`
--

INSERT INTO `training_seminars` (`id`, `name`, `description`, `type`, `joiner`, `venue`, `date`, `created`) VALUES
(1, 'Test name', 'test', 1, ',Siegfred Balidbid', 'test', '2017-12-17', '2017-12-31 14:27:50'),
(2, 'Authorized Agent', 'Authorized agent', 1, ',Siegfred Balidbid', 'test', '12/08/1900', '2017-12-31 14:34:01'),
(5, 'cxxvvtest', 'tesxcvvcx', 2, ',Siegfred Balidbid', 'testes', '12/22/2017', '2017-12-31 14:34:04'),
(6, 'test', 'test', 2, '', 'sfdfsdff dfsdfsdfds test sfsdffsdfsffsf', '12/20/2017', '2017-12-31 13:54:44'),
(7, 'test', 'test', 1, '', 'test', '12/30/2017', '2017-12-31 13:54:49');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `userid` varchar(255) NOT NULL,
  `username` varchar(50) NOT NULL,
  `password` varchar(255) NOT NULL,
  `designation` varchar(225) NOT NULL,
  `lastname` varchar(255) NOT NULL,
  `firstname` varchar(255) NOT NULL,
  `middlename` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `name_extension` varchar(20) NOT NULL,
  `contact_no` varchar(11) NOT NULL,
  `department` varchar(100) NOT NULL,
  `photo` varchar(125) NOT NULL,
  `dateofbirth` varchar(225) NOT NULL,
  `citizenship` varchar(225) NOT NULL,
  `other_citizenship_detail` varchar(225) NOT NULL,
  `placeofbirth` varchar(225) NOT NULL,
  `sex` varchar(225) NOT NULL,
  `address` varchar(225) NOT NULL,
  `zipcode` int(11) DEFAULT NULL,
  `civilstatus` varchar(225) NOT NULL,
  `height` int(11) DEFAULT NULL,
  `weight` int(11) DEFAULT NULL,
  `bloodtype` varchar(225) NOT NULL,
  `gsisidno` varchar(225) NOT NULL,
  `pagibigidno` varchar(225) NOT NULL,
  `philhealthno` varchar(225) NOT NULL,
  `sssno` varchar(225) NOT NULL,
  `tinno` varchar(225) NOT NULL,
  `agencyemployeeno` varchar(225) NOT NULL,
  `spousesurname` varchar(225) NOT NULL,
  `spousefirstname` varchar(225) NOT NULL,
  `spousemiddlename` varchar(225) NOT NULL,
  `spousenameextension` varchar(225) NOT NULL,
  `occupation` varchar(225) NOT NULL,
  `employerbusinessname` varchar(225) NOT NULL,
  `businessaddress` varchar(225) NOT NULL,
  `telephoneno` varchar(225) NOT NULL,
  `spouse_telno` bigint(20) NOT NULL,
  `nameofchildrens` text NOT NULL,
  `childrendateofbirth` varchar(225) NOT NULL,
  `father_surname` varchar(225) NOT NULL,
  `fatherfirstname` varchar(225) NOT NULL,
  `fathermiddlename` varchar(225) NOT NULL,
  `fathernameextension` varchar(225) NOT NULL,
  `mother_maidenname` varchar(225) NOT NULL,
  `mothersurname` varchar(225) NOT NULL,
  `motherfirstname` varchar(225) NOT NULL,
  `mothermiddlename` varchar(225) NOT NULL,
  `childsname_and_birthdate` text NOT NULL,
  `residence_address_block` varchar(100) NOT NULL,
  `residence_address_street` varchar(225) NOT NULL,
  `residence_address_subdivision` varchar(225) NOT NULL,
  `residence_address_brgy` varchar(225) NOT NULL,
  `residence_address_city` varchar(225) NOT NULL,
  `residence_address_province` varchar(225) NOT NULL,
  `residence_address_zip` int(11) DEFAULT NULL,
  `permanent_address_block` varchar(100) NOT NULL,
  `permanent_address_street` varchar(225) NOT NULL,
  `permanent_address_subdivision` varchar(225) NOT NULL,
  `permanent_address_brgy` varchar(225) NOT NULL,
  `permanent_address_city` varchar(225) NOT NULL,
  `permanent_address_province` varchar(225) NOT NULL,
  `permanent_address_zip` int(11) DEFAULT NULL,
  `education_school_name` text NOT NULL,
  `education_degree_course` text NOT NULL,
  `education_attendance_from` text NOT NULL,
  `education_attendance_to` text NOT NULL,
  `education_highest_level` text NOT NULL,
  `education_year_graduated` text NOT NULL,
  `education_scholars` text NOT NULL,
  `cse_career` text NOT NULL,
  `cse_rating` text NOT NULL,
  `cse_exam_date` text NOT NULL,
  `cse_exam_place` text NOT NULL,
  `cse_license_no` text NOT NULL,
  `cse_license_expiry` text NOT NULL,
  `we_inclusive_from` text NOT NULL,
  `we_inclusive_to` text NOT NULL,
  `we_position` text NOT NULL,
  `we_department_agency` text NOT NULL,
  `we_monthly_salary` text NOT NULL,
  `we_salary_job` text NOT NULL,
  `we_status_of_appointment` text NOT NULL,
  `we_govt_service` text NOT NULL,
  `voluntary_name_org` text NOT NULL,
  `voluntary_inclusive_from` text NOT NULL,
  `voluntary_inclusive_to` text NOT NULL,
  `voluntary_hrs_no` text NOT NULL,
  `voluntary_position` text NOT NULL,
  `learning_title` text NOT NULL,
  `learning_inclusive_from` text NOT NULL,
  `learning_inclusive_to` text NOT NULL,
  `learning_hrs_no` text NOT NULL,
  `learning_id_type` text NOT NULL,
  `learning_conducted` text NOT NULL,
  `mobileno` bigint(20) NOT NULL,
  `servicerecordsfrom` text NOT NULL,
  `servicerecordsto` text NOT NULL,
  `servicerecordsdesignation` text NOT NULL,
  `servicerecordsstatus` text NOT NULL,
  `servicerecordssalary` text NOT NULL,
  `servicerecordsbranch` text NOT NULL,
  `servicerecordslvpay` text NOT NULL,
  `servicerecordsdate` varchar(50) NOT NULL,
  `servicerecordsseparation` text NOT NULL,
  `salnchildrensname` text NOT NULL,
  `salnchildrensdob` text NOT NULL,
  `salnchildrensage` text NOT NULL,
  `salnassetsdescription` text NOT NULL,
  `salnassetskind` text NOT NULL,
  `salnassetslocation` text NOT NULL,
  `salnassetsassvalue` text NOT NULL,
  `salnassetsmarketvalue` text NOT NULL,
  `salnassetsyear` text NOT NULL,
  `salnassetsmode` text NOT NULL,
  `salnassetscost` text NOT NULL,
  `deadline` varchar(100) NOT NULL,
  `submitdate` varchar(50) NOT NULL,
  `papers` varchar(225) NOT NULL,
  `usertype` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  `created` datetime NOT NULL,
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `userid`, `username`, `password`, `designation`, `lastname`, `firstname`, `middlename`, `email`, `name_extension`, `contact_no`, `department`, `photo`, `dateofbirth`, `citizenship`, `other_citizenship_detail`, `placeofbirth`, `sex`, `address`, `zipcode`, `civilstatus`, `height`, `weight`, `bloodtype`, `gsisidno`, `pagibigidno`, `philhealthno`, `sssno`, `tinno`, `agencyemployeeno`, `spousesurname`, `spousefirstname`, `spousemiddlename`, `spousenameextension`, `occupation`, `employerbusinessname`, `businessaddress`, `telephoneno`, `spouse_telno`, `nameofchildrens`, `childrendateofbirth`, `father_surname`, `fatherfirstname`, `fathermiddlename`, `fathernameextension`, `mother_maidenname`, `mothersurname`, `motherfirstname`, `mothermiddlename`, `childsname_and_birthdate`, `residence_address_block`, `residence_address_street`, `residence_address_subdivision`, `residence_address_brgy`, `residence_address_city`, `residence_address_province`, `residence_address_zip`, `permanent_address_block`, `permanent_address_street`, `permanent_address_subdivision`, `permanent_address_brgy`, `permanent_address_city`, `permanent_address_province`, `permanent_address_zip`, `education_school_name`, `education_degree_course`, `education_attendance_from`, `education_attendance_to`, `education_highest_level`, `education_year_graduated`, `education_scholars`, `cse_career`, `cse_rating`, `cse_exam_date`, `cse_exam_place`, `cse_license_no`, `cse_license_expiry`, `we_inclusive_from`, `we_inclusive_to`, `we_position`, `we_department_agency`, `we_monthly_salary`, `we_salary_job`, `we_status_of_appointment`, `we_govt_service`, `voluntary_name_org`, `voluntary_inclusive_from`, `voluntary_inclusive_to`, `voluntary_hrs_no`, `voluntary_position`, `learning_title`, `learning_inclusive_from`, `learning_inclusive_to`, `learning_hrs_no`, `learning_id_type`, `learning_conducted`, `mobileno`, `servicerecordsfrom`, `servicerecordsto`, `servicerecordsdesignation`, `servicerecordsstatus`, `servicerecordssalary`, `servicerecordsbranch`, `servicerecordslvpay`, `servicerecordsdate`, `servicerecordsseparation`, `salnchildrensname`, `salnchildrensdob`, `salnchildrensage`, `salnassetsdescription`, `salnassetskind`, `salnassetslocation`, `salnassetsassvalue`, `salnassetsmarketvalue`, `salnassetsyear`, `salnassetsmode`, `salnassetscost`, `deadline`, `submitdate`, `papers`, `usertype`, `status`, `created`, `modified`) VALUES
(22, '12222', '', '$2y$10$n8lJ/rfCLYHo75cHSUzqg.CDzF7b/JVv/hfDq9bFnCfC12iJXD1BG', 'Regular', 'Balidbid', 'Siegfred', 'test3', 'teacher@gmail.com', 'JR', '423', 'Junior High', 'location12222.png', '02/02/1999', 'FILIPINO', '24234', 'Cebu city', 'Female', '', NULL, 'Single', 20, 30, 'O-', 'gsid12', '4545-4545-4545', '45-454545454-5', '4545-4545454-5', '234-324-243-243', '23tyrrty', 'Domica Rosalinda', 'SFN', 'SMN', '', 'SOCU', 'SEMP', 'busi add', '3242234', 45454, 'Adolfo monico,Monico ni Monica,sdffsdf,sdff,dddd,dd,d,d,dfdf,dfd,df,df,sdf', '12/13/2017,12/20/2017,12/05/2017,01/31/2017,03/16/2000,10/21/1970,12/11/2017,12/11/2017,12/22/2017,12/06/2017,12/06/2017,12/06/2017,12/26/2017', 'FatSURE', 'FN1dfgfg', 'FMNdfgdfg', 'name', 'fddf', 'dfg', 'dfg', 'dg', '', 'sdfdsf', 'sfs', 'sdf', 'sdfdf', 'sdfsdf', 'Province', 234, 'sdfdfs', 'sdfdsf', 'sfdsf', 'sdf', 'sdfsd', 'test', 423, 'sdfdssdf24/33/2342,dsfssdf34/54/5453,7sdfsdfdsf,7sdfrsdfsdetret,7sdfsfds', '7sf234324,34/54/5453,45/34/5453sdfsdf,34/54/5345,34/53/4534', '23/44/2434,34/54/5453,45/34/5453,34/54/5345,34/53/4534', '23/44/2342,34/54/5453,23/43/4324,32/42/4234,23/43/2434', '7sfsdff,7sdfsdfsd,7sdfsdfdf,7sdfsdfsdfsdf,7sdfdsfsdfsdf', '7sf,7sdf,7sdfsdf,7sdfsdf,7sdf', 'ddddddddddd,sdfdsf,sdfsdf,sdfsf,sdfsdf', 'ddsf,sdfsdf,wr,,,,', 'sdfs,sdf,wrwe,,,,', 'sdf,sdf,werwr,,,,', 'sdf,sdf,wrwer,,,,', 'sdf,sdf,wer,,,,', 'sdf,sdf,werew,,,,', 'sdf,sdf,sdfdsf,d,wrer,,,,,,,,,,,,,,,,,,,,,,,,', 'sdf,sdf,sdf,d,wrwer,,,,,,,,,,,,,,,,,,,,,,,,', 'sdf,sdf,sdf,d,wrwer,,,,,,,,,,,,,,,,,,,,,,,,', 'sdf,sd,sdfsdf,d,rwee,,,,,,,,,,,,,,,,,,,,,,,,', 'sdf,sdf,dfsd,d,ewrwe,,,,,,,,,,,,,,,,,,,,,,,,', 'sdf,sdf,dsf,d,werew,,,,,,,,,,,,,,,,,,,,,,,,', 'sdf,sdf,sdf,d,werwer,,,,,,,,,,,,,,,,,,,,,,,,', 'd,d,d,d,were,,,,,,,,,,,,,,,,,,,,,,,,', 'fdf,sdfsd,sf,,,,,,,,,,,', 'sdf,sdfsdf,sdf,sdf,,,,,,,,,,', 'dsf,sf,sf,sdf,,,,,,,,,,', '0sdf,sdf,sdf,sdfds,sfs,,,,,,,,,', 'sdf,sdf,sfs,sf,sdf,,,,,,,,,', 'sdf,xcvxc,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,', 'sdf,xcvcx,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,', 'sdf,xcvxc,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,', 'sdf,xcvxcv,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,', 'sdf,cxvx,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,', 'cvb,cvb,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,', 2147483647, '23/43/2423,23/42/3432,32/43/4234,,,,,,,,,,,,,,,,,,,,,,,,,,,,', '23/43/2423,23/42/3423,32/43/2432,,,,,,,,,,,,,,,,,,,,,,,,,,,,', 'test,werr,werwerrw,,,,,,,,,,,,,,,,,,,,,,,,,,,,', 'test,werer,werrwr,,,,,,,,,,,,,,,,,,,,,,,,,,,,', '3434,3434,werwer,,,,,,,,,,,,,,,,,,,,,,,,,,,,', 'teswt,test,werewr,,,,,,,,,,,,,,,,,,,,,,,,,,,,', 'test,wr,werwer,,,,,,,,,,,,,,,,,,,,,,,,,,,,', '01/02/2018', 'test,wre,werew,,,,,,,,,,,,,,,,,,,,,,,,,,,,', 'TEST,TEST,TEST,,', '34/34/3434,34/34/3434,34/34/3434,,', '12,17,11,,', 'test,,,,', 'test,,,,', 'test,,,,', 'test,,,,', 'test,,,,', '1545,,,,', 't,,,,', '22,,,,', '11/18/2017', '01/31/2018', '\"PDS,SALN,SERVICE RECORDS\"', 1, 1, '2017-11-23 10:15:14', '2018-01-11 09:12:53'),
(24, '32434', '', '$2y$10$KtG4WSA6ooT.HpL5x3/L4ejAqkMWJx0QsCnvg0pEPXZJ7UgEmexpW', 'Regular', 'test', 'admin', 'admin', 'testadmin@gmail.com', '', '4234', 'Senior High (GAS)', '', '0000-00-00', '', '', '', '', '', 0, '', 0, 0, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '', '', '', '', '', '', 0, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0', '', '', '', '', '', '', '', 0, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '01/16/2018', '01/19/2018', '[\"PDS\",\"SERVICE RECORDS\",\"SALN\"]', 2, 1, '2017-11-23 10:15:14', '2018-01-11 03:29:06'),
(25, '12323', '', '$2y$10$xyWU/TKpWBqheMYLWBY.euHT7IH/eknM7YZmaSEBwHol04pt9egU6', 'Regular', 'J', 'Jo', 'Antipala', 'jotest@gmail.com', '', '2234', 'Senior High (STEM)', '', '0000-00-00', '', '', '', '', '', 0, '', 0, 0, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '', '', '', '', '', '', 0, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0', '', '', '', '', '', '', '', 0, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '11/05/2017', '', '[\"PDS\",\"SERVICE RECORDS\",\"SALN\"]', 2, 1, '2017-11-23 10:15:14', '2017-12-13 15:25:36'),
(26, '23434', '', '$2y$10$u1wNQWStcgbvXUb6toeT0OVoPbePKsOP0hBj1Ns3uYKABQFLLoXAu', 'Regular', 'sdf', 'dfdsf', 'sdf', 'vinzadz1987@gmail.com', '', '3242', 'Senior High (ABM)', '', '0000-00-00', '', '', '', '', '', 0, '', 0, 0, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '', '', '', '', '', '', 0, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0', '', '', '', '', '', '', '', 0, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '01/10/2018', '01/10/2018', '[\"PDS\",\"SERVICE RECORDS\",\"SALN\"]', 2, 1, '2017-11-23 10:15:14', '2018-01-11 03:28:56'),
(27, 'asaassasa', '', '$2y$10$LPpSQsWupijcsC/i7qxpcOgSgAa3XmgSAYVLYpxefOtBxCn/5YAtm', 'Regular', 'assa', 'sasa', 'assa', 'sasaas@gmail.com', '', 'asadsa', 'Senior High (HUMSS)', '', '0000-00-00', '', '', '', '', '', 0, '', 0, 0, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '', '', '', '', '', '', 0, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0', '', '', '', '', '', '', '', 0, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0000-00-00 00:00:00', '', '', 2, 0, '2017-11-23 10:15:14', '0000-00-00 00:00:00'),
(28, 'saasd', '', '$2y$10$y1jIILoMvst.41rx6Eg4sOKkMMqTIcbzo5rCWK5t5YdlSnS4RS1Xm', 'Contractual', 'asd', 'asdsda', 'asdasd', 'c@gmail.com', '', '213213', 'Senior High (TLE)', '', '0000-00-00', '', '', '', '', '', 0, '', 0, 0, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '', '', '', '', '', '', 0, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0', '', '', '', '', '', '', '', 0, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0000-00-00 00:00:00', '', '', 2, 1, '2017-11-23 10:15:14', '2017-12-09 10:20:16'),
(29, '124653', '', '$2y$10$FChyfjiE53vML84HEgAxhObqlCytOc1mT207WuvqK4tOHkd5Kpvcq', 'Regular', 'SureName', 'testname', 'Middlename', 'admin@gmail.com', '', '955445', 'Senior High (GAS)', 'anMebG0_700b124653.jpg', '0000-00-00', '', '', '', '', '', 0, '', 0, 0, 'B-', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '', '', '', '', '', '', 0, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0', '', '', '', '', '', '', '', 0, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '12/12/2017', '', '[\"PDS\",\"SERVICE RECORDS\",\"SALN\"]', 2, 1, '2017-11-23 10:15:14', '2018-01-11 03:22:37'),
(30, '1234', '', '$2y$10$prOY6ecPFzx6gsnlVlNSP.0R7wAM3qYZ9V8SSm9/jdBbwkH33eDgu', 'Regular', 'Manatad', 'Manatad', 'Dotollo', 'janine_manatad@ymail.com', '', '09466667883', 'Senior High (ABM)', '', '0000-00-00', '', '', '', '', '', 0, '', 0, 0, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '', '', '', '', '', '', 0, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0', '', '', '', '', '', '', '', 0, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '11/23/2017', '', '[\"PDS\",\"SERVICE RECORDS\",\"SALN\"]', 1, 1, '2017-11-23 10:15:14', '2017-12-13 15:21:54'),
(31, '123', '', '$2y$10$iyHHYKmZACs7OOtz4dxPqO2uG.caCez0dqUwza8GT7GeFN1aMiEYC', 'Regular', 'done', 'nsu', 'admin', 'test123@gmail.com', '', '123', 'Senior High (ABM)', '', '0000-00-00', '', '', '', '', '', 0, '', 0, 0, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '', '', '', '', '', '', 0, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0', '', '', '', '', '', '', '', 0, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '11/02/2017', '', '[\"PDS\",\"SERVICE RECORDS\",\"SALN\"]', 2, 1, '2017-11-23 10:15:14', '2017-12-13 15:17:52'),
(32, '7777', '', '$2y$10$3VmET5z5mk7qvnsEOvD0c.H5qLDnsJW0MilnSa9xd9NlxMsL864Bi', 'Regular', 'Antipala', 'Jo', 'sabonsolin', 'vinzadz1987@test.com', '', '343', 'Senior High (HUMSS)', '', '0000-00-00', 'Filipino', '', 'Larrazabal Naval Biliran', 'Female', '', NULL, 'Single', 1212, 145, 'A+', '3433234324', '3423434324', '12313123123', '23423424', '2344324423', '343243324', 'dsfsd', 'df', 'sdf', 'sdfdsf', 'mannaro', 'walay trabaho', '', 'na', 234234, '', '', 'father_surname', 'ddfdd', 'dfdfd', 'ddd', 'dsfd', 'dsfsdf sdfsdfdsds', 'sdffdsfsdf', 'sdfdfsfdsdf', '', '', '', '', 'Apas', 'Cebu', '', 60000, '', '', '', 'Naval', 'Naval', '', 564, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0', '', '', '', '', '', '', '', 21465456, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0000-00-00 00:00:00', '', '[\"PDS\"]', 1, 0, '2017-11-24 18:06:00', '2018-01-10 10:05:14'),
(33, '12345', '', '$2y$10$dCIQmYVpaH5zFr8KUJ/amesPdzwNBgj5Aas5s1XFbZ6j4YrD7XKma', 'Regular', 'lemuel1', 'lemuel1', 'my middle', 'karllim@gmail.com', 'JR', '342', 'Junior High', '153271222212345.jpg', '0000-00-00', '45', '435', 'fdgdfg', 'Male', '', NULL, 'Divorced', 432, 43, 'O-', '44444444', '45', '435', '45', '4543', '45', '', '', '', '', '', '', '', '343', 0, '', '', '', '', '', '', '', '', '', '', '', 'house block lot no', 'NORMA', 'residential subdivision', '4354', 'Municipality', 'Province', 100, 'dsf', 'Street', 'dsf', 'sdf', 'sdf', '', 46, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0', '', '', '', '', '', '', '', 3453, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '11/10/2017', '', '[\"PDS\",\"SERVICE RECORDS\",\"SALN\"]', 1, 1, '2017-11-24 18:17:21', '2018-01-13 09:55:13'),
(34, '4564465', '', '$2y$10$p6LBIEkrVcDG.KtH5Nx3ZumF0ujc.JoJpNwS14FTU39uz03rVzh1C', 'Contractual', 'padul', 'lester', 'lester', 'lesterpadul@test.com', '', '342432', 'Senior High (TLE)', '', '0000-00-00', '', '', '', '', '', NULL, '', NULL, NULL, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', NULL, '', '', '', '', '', '', NULL, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0', '', '', '', '', '', '', '', 0, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '11/04/2017', '', '[\"PDS\",\"SERVICE RECORDS\",\"SALN\"]', 2, 1, '2017-11-24 18:18:22', '2018-01-10 09:41:30'),
(36, '343434', '', '$2y$10$OJ/DUdMRzaRk89.FoZJ4lOqx9VasYEWDO5ws6j5IJGqbFRQtafoSu', 'Regular', 'asdfsf', 'edsfsdf', 'sdfsfjf', 'admin123@test.com', '', 'sdfdsf', 'Senior High (GAS)', '', '0000-00-00', '', '', '', '', '', NULL, '', NULL, NULL, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', NULL, '', '', '', '', '', '', NULL, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0', '', '', '', '', '', '', '', 0, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '01/10/2018', '01/11/2018', '[\"PDS\",\"SERVICE RECORDS\"]', 1, 0, '2017-11-24 10:40:58', '2018-01-11 03:29:14');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `department`
--
ALTER TABLE `department`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `leaves`
--
ALTER TABLE `leaves`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `messaging`
--
ALTER TABLE `messaging`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `training_seminars`
--
ALTER TABLE `training_seminars`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `department`
--
ALTER TABLE `department`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `leaves`
--
ALTER TABLE `leaves`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `messaging`
--
ALTER TABLE `messaging`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT for table `training_seminars`
--
ALTER TABLE `training_seminars`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=37;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
